import math
import numpy as np
import algorithms
import matplotlib.pyplot as plt

from sklearn import datasets, cluster
from mpl_toolkits.mplot3d import Axes3D
from scipy.linalg import eigh
#from cvxopt import normal, matrix
#from cvxopt.modeling import variable, op, max, sum
#from l1regls import l1regls

#m, n = 3, 2
#P, q = normal(m,n), normal(m,1)
#P[0, :] = [5, 2]
#P[1, :] = [4, 5]
#P[2, :] = [3, 1]
##P = matrix([[5.0, 4.0, 3.0], [2.0, 5.0, 1.0]])
#q[0] = 19
#q[1] = 22
#q[2] = 11
## q = matrix([[19], [22], [11]])
#print P
#print '----------'
#print q
#print '----------'
#x = l1regls(P,q)
#print '----------'
#print x
#print '----------'
##print y

# Generates the S-Curve dataset.
# X, color = datasets.samples_generator.make_s_curve(n_samples=1500)
# n_samples, n_features = X.shape
# n_neighbors = 30

# Creates a dataset with 2 plans.
plan_size = 10
plan_step = 1
#plan_dataset = [[50+i-plan_size/2, 50+plan_size/2-0.5, 50+j] for j in range(0, plan_size, plan_step) for i in range(0, plan_size, plan_step)]
#plan_dataset.extend([[50-0.5, 50+i, 50+j] for j in range(0, plan_size, plan_step) for i in range(0, plan_size, plan_step)])

plan_dataset = [[i+1, j+1, j+i] for j in range(0, plan_size, plan_step) for i in range(0, plan_size, plan_step)]
plan_dataset.extend([[5, j+1, i+1]for j in range(0, plan_size, plan_step) for i in range(0, plan_size, plan_step)])

plan_dataset = np.array(plan_dataset)

# Creates affinity matrix.
A = np.zeros((len(plan_dataset), len(plan_dataset)))
for i in range(len(plan_dataset)):
	q = plan_dataset[i, :]
	P = np.delete(plan_dataset, (i), axis=0).T
	tau = algorithms.l1_bound(P, q)
	#print tau
	x = algorithms.l1l2_regularization(P, q, 1, 1)
	x = np.insert(x, i, 0)
	#print x
	#print '---'
	#print q
	#print np.dot(plan_dataset.T,x)
	#print '---'
	A[i, :] = x
	##########################################################
	#########q = matrix(plan_dataset[i, :])
	#########P = matrix(np.delete(plan_dataset, (i), axis=0).T)
	#########x = variable(P.size[1], 'x')
	##########u = variable(P.size[0], 'u')
	#########op(sum(x), [P*x <= q, x >= 0]).solve()
	#########print x
	###########################################################
	#q = matrix(plan_dataset[i, :])
	##print q
	#P = matrix(np.delete(plan_dataset, (i), axis=0).T)
	##print P
	#x = np.array(l1regls(P,q))
	#x = np.insert(x, i, 0)
	##print x
	#A[i, :] = x
#print A
A = abs(A) + abs(A.T)
#A = np.array([[A[i, j] + A[j, i] for i in range(len(A))] for j in range(len(A))])
#print A.shape
#A = np.array([[math.exp(-(np.linalg.norm(x1-x2)**2)/2) for x2 in plan_dataset] for x1 in plan_dataset]) - np.identity(len(plan_dataset))

# Construct the Laplacian Graph.
D = np.identity(len(plan_dataset))
for i in range(len(plan_dataset)):
	D[i, i] = np.sum(A[i, :])
L = D - A

# Calculate eigenvalues and eigenvectors
w, v = np.linalg.eig(L)
w, v = eigh(L, eigvals=(0, 1))
#print w
#print v.shape

# Apply KMeans to the eigenvectors.
labels = cluster.KMeans(n_clusters=2).fit_predict(v)
print labels

# Apply spectral clustering.
# labels = cluster.SpectralClustering(n_clusters=4, gamma=0.5).fit_predict(plan_dataset)
#print labels

# Plot result.
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.scatter(plan_dataset[:, 0], plan_dataset[:, 1], plan_dataset[:, 2], c=labels, cmap=plt.cm.Spectral)
plt.axis('tight')
plt.xticks([]), plt.yticks([])
plt.title('S-Curve Data')
plt.show()