import numpy as np

p_size = 10
p_step = 1
p_dataset = [[50+i-p_size/2, 50+p_size/2-0.5, 50+j] for j in range(0, p_size, p_step) for i in range(0, p_size, p_step)]
p_dataset.extend([[50-0.5, 50+i, 50+j] for j in range(0, p_size, p_step) for i in range(0, p_size, p_step)])
X = np.array(p_dataset)

Z = np.zeros(X.shape)
J = np.zeros(X.shape)
E = np.zeros(X.shape)
Y1 = np.zeros(X.shape)
Y2 = np.zeros(X.shape)
m_min = 0.000001
m_max = 1000000
p = 1.1
e = 0.00000001

