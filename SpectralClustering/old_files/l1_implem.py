import numpy as np

# L1 norm minimization
def l1(A, b, x0, pdtol=0.001, pdmaxiter=50, cgtol=0.00000001):
	N = x0.shape[0]

	alpha = 0.01
	beta = 0.5
	mu = 10

	gradf0 = np.append([0 for i in range(N)], [1 for i in range(N)])

	if np.linalg.norm(np.dot(A, x0) - b)/np.linalg.norm(b) > cgtol:
		print "Starting point infeasible; Using x0 = At*inv(AAt)*y."
		w = np.linalg.lstsq(np.dot(A, A.T), b)[0]
		x0 = np.dot(A.T, w)

	x = x0
	v_abs = np.vectorize(abs)
	u = 0.95*v_abs(x0) + 0.1*max(v_abs(x0))

	fu1 = x - u
	fu2 = -x - u
	lamu1 = -1/fu1
	lamu2 = -1/fu2

	v = np.dot(-A, (lamu1 - lamu2))
	Atv = np.dot(A.T, v)
  	rpri = np.dot(A, x) - b

  	sdg = -(np.dot(fu1.T, lamu1) + np.dot(fu2.T, lamu2))
	tau = 2*N*mu/sdg

	rcent = np.append(-lamu1*fu1, lamu2*fu2) - 1/tau
	rdual = gradf0 + np.append(lamu1-lamu2, -lamu1-lamu2) + np.append(Atv, [0 for i in range(N)])
	resnorm = np.linalg.norm(np.append(np.append(rdual, rcent), rpri))

	pditer = 0
	done = (sdg < pdtol) or (pditer >= pdmaxiter)

	while not done:
		pditer = pditer + 1

		w1 = -1/tau*(-1/fu1 + 1/fu2) - Atv
		w2 = -1 - 1/tau*(1/fu1 + 1/fu2)
		w3 = -rpri

		sig1 = -lamu1/fu1 - lamu2/fu2
  		sig2 = lamu1/fu1 - lamu2/fu2
  		sigx = sig1 - (sig2**2)/sig1

  		w1p = -(w3 - np.dot(A, w1*sigx - w2*sig2/(sigx*sig1)))

  		H11p = np.dot(A, np.dot(np.diag(1/sigx), A.T)) # H11p = A*(sparse(diag(1./sigx))*A');

  		dv = np.linalg.lstsq(H11p, w1p)[0]

  		dx = (w1 - w2*sig2/sig1 - np.dot(A.T, dv))/sigx
		Adx = np.dot(A, dx)
		Atdv = np.dot(A.T, dv)

		du = (w2 - sig2*dx)/sig1
		dlamu1 = (lamu1/fu1)*(-dx+du) - lamu1 - (1/tau)*1/fu1
		dlamu2 = (lamu2/fu2)*(dx+du) - lamu2 - (1/tau)*1/fu2

		indp = np.where(dlamu1 < 0)[0]
		indn = np.where(dlamu2 < 0)[0]
		s = min(np.append(1, np.append(-lamu1[indp]/dlamu1[indp], -lamu2[indn]/dlamu2[indn])))

		indp = np.where(dx-du > 0)[0]
		indn = np.where(-dx-du > 0)[0]
		s = 0.99*min(np.append(s, np.append(-fu1[indp]/(dx[indp]-du[indp]), -fu2[indn]/(-dx[indn]-du[indn]))))

		suffdec = 0
		backiter = 0
		while not suffdec:
			xp = x + np.dot(s, dx)
			up = u + np.dot(s, du)

			vp = v + np.dot(s, dv)
			Atvp = Atv + np.dot(s, Atdv)

			lamu1p = lamu1 + np.dot(s, dlamu1)
			lamu2p = lamu2 + np.dot(s, dlamu2)

			fu1p = xp - up
			fu2p = -xp - up

			rdp = gradf0 + np.append(lamu1p-lamu2p, -lamu1p-lamu2p) + np.append(Atvp, [0 for i in range(N)])
			rcp = np.append(-lamu1p*fu1p, -lamu2p*fu2p) - 1/tau

			rpp = rpri + np.dot(s, Adx)

			suffdec = np.linalg.norm(np.append(np.append(rdp, rcp), rpp)) <= resnorm*(1 - alpha*s)

			s = beta*s
			backiter += 1
			if backiter > 32:
				print "Stuck backtracking, returning last iterate.  (See Section 4 of notes for more information.)"
				return x

			# suffdec = True

		x = xp
		u = up
		v = vp
		Atv = Atvp
		lamu1 = lamu1p
		lamu2 = lamu2p
		fu1 = fu1p
		fu2 = fu2p

		sdg = -(np.dot(fu1.T, lamu1) + np.dot(fu2.T, lamu2))
		tau = 2*N*mu/sdg
		rpri = rpp

		rcent = np.append(-lamu1*fu1, -lamu2*fu2) - 1/tau
		rdual = gradf0 + np.append(lamu1-lamu2, -lamu1-lamu2) + np.append(Atv, [0 for i in range(N)])
		resnorm = np.linalg.norm(np.append(np.append(rdual, rcent), rpri))

		done = (sdg < pdtol) or (pditer >= pdmaxiter)

		# print "Iterating..."
		# done = True
	return xp

# Test matrix A.
A = np.zeros((2, 20))
A[0, 0:10] = A[1, 0:10] = A[1, 10:20] = [i for i in range(10)]
A[0, 10:20] = 5

# Test array b.
b = np.array([2.5, 2.5])

# Calculates initial value x0 (parameter)
x0 = np.dot(A.T, b) 

xp_r = l1(A, b, x0)
print xp_r