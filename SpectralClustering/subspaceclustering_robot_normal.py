# Importing libraries. The same will be used throughout the article.
import sys
import random

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from scipy.linalg import eigh
from sklearn import linear_model, cluster

# Reading input file
P = []
x = []
for line in sys.stdin:
    line = line.replace(",", "").replace(";", "")
    p = line.split()
    P.append([float(p[i]) if i > 0 or float(p[0]) >= 0 else 360 + float(p[0]) for i in range(len(p))])

P.sort(key=lambda column: column[:1], reverse=True)
P = np.array(P)

x = np.array(P[:, 0])
P = P[:, 3:P.shape[1]]

alpha = [1e-2, 1e-2, 1e-2, 1e-2, 1e-2, 1e-2]

fig = plt.figure()

for d in range(P.shape[1]): # P.shape[1]
	A = np.c_[np.radians(x), P[:, d]].T
	
	W = np.zeros((A.shape[1], A.shape[1]))
	for i in range(A.shape[1]): # A.shape[1]
		b = A[:, i]
		A_ = np.delete(A, (i), axis=1)
		
		lassoreg = linear_model.Lasso(alpha=alpha[d], normalize=False, max_iter=1e5)
		lassoreg.fit(A_, b)
		
		w = lassoreg.coef_
		w = np.insert(w, i, 0)

		W[i, :] = abs(w)/max(abs(w))

	W = W + W.T

	K = 5 # Number of no-zero coeff to form adjacency matrix
	W_ = np.zeros(W.shape)
	for i in range(W.shape[0]):
	 	ind_order = np.argsort(W[i, :])[::-1]
	 	for j in range(K):
	 		W_[i, ind_order[j]] = W[i, ind_order[j]]/W[i, ind_order[0]]
	W = W_ + W_.T

	# Construct the Laplacian Graph.
	D = np.identity(W.shape[0])
	for i in range(W.shape[0]):
		D[i, i] = np.sum(W[i, :])
	L = D - W

	# Calculate eigenvalues and eigenvectors
	w, v = np.linalg.eig(L)
	w, v = eigh(L, eigvals=(0, 1))

	# Apply KMeans to the eigenvectors.
	labels = cluster.KMeans(n_clusters=20).fit_predict(v)
	print labels

	ax = fig.add_subplot(231 + d)
	ax.scatter(A[0, :], A[1, :], c=labels, cmap=plt.cm.Spectral)
	plt.axis('tight')
	plt.xticks([]), plt.yticks([])
	plt.title('Attribute: %.3g'%d)

plt.show()