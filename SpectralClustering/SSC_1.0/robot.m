% Clear Workspace
clear all; clc;

% Read data
RobotData = csvread('data.csv');

% Sort the data
[~, order] = sort(RobotData(:, 1));
RobotData = RobotData(order, :);

% Change angles to radians
RobotData(:, 1) = degtorad(RobotData(:, 1));

% % Create a matrix with angles and first dimension values
% X = ones(3, size(RobotData));
% X(1, :) = RobotData(:, 1);
% X(2, :) = RobotData(:, 4);

% Create a matrix using the 6D data.
X = ones(7, size(RobotData));
X(1:6, :) = RobotData(:, 4:9)';

% Projection Dimension
r = 0;

% Affine Constraint
Cst = 1;

% Optimization Algorithm
OptM = 'Lasso';

% Regularization Parameter
lambda = 0.001;

% Number of components to be used in the affinity matrix
K = 360;
% if Cst == 1
%     K = K + 1;
% end;

% Projecting Data
Xp = DataProjection(X, r, 'NormalProj');

% Create sparce coefficient matrix
CMat = SparseCoefRecovery(Xp, Cst, OptM, lambda);
CKSym = BuildAdjacency(CMat, K);

% Apply spectral clustering
Grps = SpectralClustering(CKSym, 10);

% Shows the scatterplot
figure;
scatter(RobotData(:, 1), RobotData(:, 4), [], Grps, 'filled');
figure;
scatter(RobotData(:, 1), RobotData(:, 5), [], Grps, 'filled');
figure;
scatter(RobotData(:, 1), RobotData(:, 6), [], Grps, 'filled');
figure;
scatter(RobotData(:, 1), RobotData(:, 7), [], Grps, 'filled');
figure;
scatter(RobotData(:, 1), RobotData(:, 8), [], Grps, 'filled');
figure;
scatter(RobotData(:, 1), RobotData(:, 9), [], Grps, 'filled');