# Importing libraries. The same will be used throughout the article.
import sys
import random

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from sklearn import linear_model

# Reading input file
P = []
x = []
for line in sys.stdin:
    line = line.replace(",", "").replace(";", "")
    p = line.split()
    P.append([float(p[i]) if i > 0 or float(p[0]) >= 0 else 360 + float(p[0]) for i in range(len(p))])

P.sort(key=lambda column: column[:1], reverse=True)
P = np.array(P)

x = np.array(P[:, 0])
P = P[:, 3:P.shape[1]]

alpha = [1e-2, 1e-2, 1e-2, 1e-2, 1e-2, 1e-2]
n_exp = [10, 10, 10, 10, 10, 10]

print "x x^2 ... x^9 sin(x) cos(x) log(x) exp(x) x=1...360"

for d in range(P.shape[1]):
	X = np.array([x**j for j in range(1, n_exp[d])]).T
	X = np.c_[X, np.sin(np.radians(x)), np.cos(np.radians(x)), np.log(x), np.exp(x)]

	y = P[:, d]
	
	lassoreg = linear_model.Lasso(alpha=alpha[d], normalize=True, max_iter=1e5)
	lassoreg.fit(X, y)
	y_pred = lassoreg.predict(X)

	print "Dimension %.3g Coefficients: "%d
	print lassoreg.coef_

	plt.subplot(231 + d)
	plt.tight_layout()
	plt.plot(x, y_pred)
	plt.plot(x, y, '.')
	plt.title('Attribute: %.3g'%d)

plt.show()