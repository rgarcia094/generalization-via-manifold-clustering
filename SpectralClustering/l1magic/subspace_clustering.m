clear all; clc;

path(path, './Optimization');
path(path, './Data');

A = zeros(3, 200);
for i = 1:10
    for j = 1:10
        A(1, 10*(i-1) + j) = 50 + i - 10/2;
        A(2, 10*(i-1) + j) = 50 + 10/2 - 0.5;
        A(3, 10*(i-1) + j) = 50 + j;
    end;
end;
for i = 1:10
    for j = 1:10
        A(1, 100 + 10*(i-1) + j) = 50 - 0.5;
        A(2, 100 + 10*(i-1) + j) = 50 + i;
        A(3, 100 + 10*(i-1) + j) = 50 + j;
    end;
end;
        

%A = zeros(2, 20);
%A(1, 1:10) = 1:10;
%A(2, 1:10) = 1:10;
%A(1, 11:20) = 5;
%A(2, 11:20) = 1:10;

N = size(A, 2);
d = size(A, 1);
W = zeros(N);
for i = 1:N
    b = A(:, i);
    
    A_ = A;
    A_(:, i) = [];
    
    x0 = A_'*b;
    
    xp = l1eq_pd(x0, A_, [], b, 1e-3);
    xp_ = [xp(1:(i-1)); 0.0; xp(i:end)];
    
    W(:, i) = xp_;
end;

W = abs(W) + abs(W');

[C, L, U] = SpectralClustering(W, 2, 1);



