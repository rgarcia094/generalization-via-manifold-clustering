import sys
import random

import numpy as np
import numpy.matlib
import matplotlib.pyplot as plt

# from sklearn import linear_model, cluster
from math import floor
from scipy.linalg import eigh
from sklearn.cluster import KMeans
from sklearn.decomposition import PCA
from mpl_toolkits.mplot3d import Axes3D

def DataProjection(X, r=0):
	if r == 0:
		return X
	else:
		X = X.T
		X = PCA(n_components=r).fit_transform(X)
		return X.T

def MatrixNormalize(Y):
	n = np.zeros((Y.shape[1], 1))
	Yn = np.zeros((Y.shape[0], Y.shape[1]))
	for i in range(Y.shape[1]):
		n[i] = np.linalg.norm(Y[:, i])
		Yn[:, i] = Y[:, i]/n[i, 0] # May be a problem
	return [Yn, n]

def ErrorLinearSystem(P, Z):
	[R, N] = Z.shape

	if R > N:
		E = np.dot(P[:, N:], Z[N:, :])
		Y = P[:, :N]
		Y0 = Y - E
		C = Z[:N, :]
	else:
		Y = P
		Y0 = P
		C = Z

	[Yn, n] = MatrixNormalize(Y0)
	M = np.matlib.repmat(n.T, Y.shape[0], 1)
	print n.shape
	S = Yn - np.dot(Y, C)/M
	return np.sqrt(np.max(np.sum(S**2, axis=0)))

def LambdaMatrix(X, P):
	N = X.shape[1]

	T = np.dot(P.T, X)
	T[:N, :] = T[:N, :] - np.diag(np.diag(T[:N, :]))

	T = abs(T)
	return np.min(np.max(T, axis=0))

def SimilarityGraph(X, affine=False, alpha=800, thr=2e-4, max_iter=200):
	# Number of elements in the dataset
	N = X.shape[1]

	# Setting penalty parameters for the ADMM
	mu1 = alpha/LambdaMatrix(X, X)
	mu2 = alpha

	if not affine:
		# Initialization
		A = np.linalg.inv(mu1*np.dot(X.T, X) + mu2*np.identity(N))
		C1 = np.zeros((N, N))
		lambda2 = np.zeros((N, N))
		err = 10*thr

		i = 0
		while err > thr and i < max_iter:
			# Updating Z
			Z = np.dot(A, mu1*np.dot(X.T, X) + mu2*(C1 - lambda2/mu2))
			Z = Z - np.diag(np.diag(Z))

			# Updating C
			C_ = abs(Z + lambda2/mu2) - 1/mu2*np.ones((N, N))
			C_[C_ < 0] = 0
			C2 = C_ * np.sign(Z + lambda2/mu2)
			C2 = C2 - np.diag(np.diag(C2))

			# Updating Lagrande Multipliers
			lambda2 = lambda2 + mu2*(Z - C2)

			# Computing errors
			err = np.max(abs(Z - C2))

			C1 = C2
			i += 1
	else:
		# Initialization
		A = np.linalg.inv(mu1*np.dot(X.T, X) + mu2*np.identity(N) + mu2*np.ones((N, N)))
		C1 = np.zeros((N, N))
		lambda2 = np.zeros((N, N))
		lambda3 = np.zeros((1, N))
		err1, err2 = 10*thr, 10*thr

		i = 0
		while (err1 > thr or err2 > thr) and i < max_iter:
			# Updating Z
			Z = np.dot(A, mu1*np.dot(X.T, X) + mu2*(C1 - lambda2/mu2) + np.dot(mu2*np.ones((N, 1)), np.ones((1, N)) - lambda3/mu2))
			Z = Z - np.diag(np.diag(Z))

			# Updating C
			C_ = abs(Z + lambda2/mu2) - 1/mu2*np.ones((N, N))
			C_[C_ < 0] = 0
			C2 = C_ * np.sign(Z + lambda2/mu2)
			C2 = C2 - np.diag(np.diag(C2))

			# Updating Lagrande Multipliers
			lambda2 = lambda2 + mu2*(Z - C2)
			lambda3 = lambda3 + mu2*(np.dot(np.ones((1, N)), Z) - np.ones((1, N)))

			# Computing errors
			err1 = np.max(abs(Z - C2))
			err2 = np.max(abs(np.dot(np.ones((1, N)), Z) - np.ones((1, N))))

			C1 = C2
			i += 1

	return C2

# TODO: Version of Similarity Graph with outliers
def SimilarityGraphOutliers(X, affine=False, alpha=800, thr=2e-4, max_iter=200):
	[D, N] = X.shape

	gamma = alpha/np.linalg.norm(X, 1)
	P = np.c_[X, np.identity(D)/gamma] # This could be inverted

	# Setting penalty parameters for the ADMM
	mu1 = alpha/LambdaMatrix(X, P)
	mu2 = alpha

	if not affine:
		# Initialization
		A = np.linalg.inv(mu1*np.dot(P.T, P) + mu2*np.identity(N + D))
		C1 = np.zeros((N + D, N))
		lambda1 = np.zeros((D, N))
		lambda2 = np.zeros((N + D, N))
		err1, err2 = 10*thr, 10*thr

		i = 0
		while (err1 > thr or err2 > thr) and i < max_iter:
			# Updating Z
			Z = np.dot(A, np.dot(mu1*P.T, X + lambda1/mu1) + mu2*(C1 - lambda2/mu2))
			Z[:N, :] = Z[:N, :] - np.diag(np.diag(Z[:N, :]))

			# Updating C
			C_ = abs(Z + lambda2/mu2) - 1/mu2*np.ones((N+D, N))
			C_[C_ < 0] = 0
			C2 = C_ * np.sign(Z + lambda2/mu2)
			C2[:N, :] = C2[:N, :] - np.diag(np.diag(C2[:N, :]))

			# Updating Lagrande Multipliers
			lambda1 = lambda1 + mu1*(X - np.dot(P, Z))
			lambda2 = lambda2 + mu2*(Z - C2)

			# Computing Errors
			err1 = np.max(abs(Z - C2))
			err2 = ErrorLinearSystem(P, Z)

			C1 = C2
			i += 1
	else:
		# Initialization
		delta = np.r_[np.ones((N, 1)), np.zeros((D, 1))]
		A = np.linalg.inv(mu1*np.dot(P.T, P) + mu2*np.identity(N + D) + mu2*np.dot(delta, delta.T))
		C1 = np.zeros((N + D, N))
		lambda1 = np.zeros((D, N))
		lambda2 = np.zeros((N + D, N))
		lambda3 = np.zeros((1, N))
		err1, err2, err3 = 10*thr, 10*thr, 10*thr

		i = 0
		while (err1 > thr or err2 > thr or err3 > thr) and i < max_iter:
			# Updating Z
			Z = np.dot(A, mu1*np.dot(P.T, X + lambda1/mu1) + mu2*(C1 - lambda2/mu2) + mu2*np.dot(delta, np.ones((1, N)) - lambda3/mu2))
			Z[:N, :] = Z[:N, :] - np.diag(np.diag(Z[:N, :]))

			# Updating C
			C_ = abs(Z + lambda2/mu2) - 1/mu2*np.ones((N+D, N))	
			C_[C_ < 0] = 0
			C2 = C_ * np.sign(Z + lambda2/mu2)
			C2[:N, :] = C2[:N, :] - np.diag(np.diag(C2[:N, :]))

			# Updating Lagrande Multipliers
			lambda1 = lambda1 + mu1*(X - np.dot(P, Z))
			lambda2 = lambda2 + mu2*(Z - C2)
			lambda3 = lambda3 + mu2*(np.dot(delta.T, Z) - np.ones((1, N)))

			# Computing Errors
			err1 = np.max(abs(Z - C2))
			err2 = ErrorLinearSystem(P, Z)
			err3 = np.max(abs(np.dot(delta.T, Z) - np.ones((1, N))))

			C1 = C2
			i += 1

	return C2[:N, :]

# Creates a sparse representation of the matrix W removing the smaller components
def SparseRepresentation(W, rho=1):
	if rho >= 1:
		return W

	N = W.shape[1]
	W_ = np.zeros((N, N))

	for i in range(N):
		order = np.argsort(abs(W[:, i]))[::-1]
		sorted_w = abs(W[order, i])

		sum_w = np.sum(sorted_w[:])
		stop, cSum, t = False, 0, 0
		while not stop:
			cSum += sorted_w[t]
			if cSum >= rho*sum_w:
				stop = True
				W_[order[:t], i] = W[order[:t], i]
			t += 1
	return W_

def AdjacencyMatrix(W, k=0):
	N = W.shape[0]

	W = abs(W)

	if k == 0:
		for i in range(N):
			W[:, i] = W[:, i]/(np.max(W[:, i]) + np.finfo(float).eps)
	else:
		for i in range(N):
			order = np.argsort(W[:, i])[::-1]
			for j in range(k):
				W[order[j], i] = W[order[j], i]/(W[order[0], i] + np.finfo(float).eps)

	return W + W.T

def SpectralClustering(W, n):
	N = W.shape[0]

	max_iter = 1000
	n_init = 20

	D = np.diag(1.0/np.sqrt(np.sum(W, axis=0) + np.finfo(float).eps))
	L = np.identity(N) - np.dot(D, np.dot(W, D))

	[_,_,V] = np.linalg.svd(L)
	V = V.T

	K = V[:, N-n:]
	M = np.sum(K**2, axis=1)**0.5
	NS = K/(M[:, None] + np.finfo(float).eps) # TODO: This might cause problems

	groups = KMeans(n_clusters=n, max_iter=max_iter, n_init=n_init).fit_predict(NS)

	return groups

def SSC(X, n, r=0, affine=False, alpha=20, outlier=False, rho=1):
	# Project the data to r dimension
	X = DataProjection(X, r)

	# Creates a similarity graph where elements in the same subspace will have greater weights
	if not outlier:
		W = SimilarityGraph(X, affine, alpha)
	else:
		W = SimilarityGraphOutliers(X, affine, alpha)

	# Remove smallers elements of the matrix to make it sparse
	W = SparseRepresentation(W, rho)

	# Build adjacency matrix from the sparse similarity graph
	W = AdjacencyMatrix(W)

	# i = 113
	# order = np.argsort(W[:, i])[::-1]
	# print order

	# Apply Spectral Clustering in the adjacency matrix
	groups = SpectralClustering(W, n)

	return [groups, W]

# Creates a random dataset.
D = 3
d1 = 1
d2 = 1
d3 = 1
d4 = 1
d5 = 1
N1 = 100
N2 = 100
N3 = 100
N4 = 100
N5 = 100
N = N1 + N2 + N3 + N4 + N5
np.random.seed(60)
X1 = np.dot(np.random.randn(D,d1), np.random.randn(d1,N1))
X2 = np.dot(np.random.randn(D,d2), np.random.randn(d2,N2))
X3 = np.dot(np.random.randn(D,d3), np.random.randn(d3,N3))
X4 = np.dot(np.random.randn(D,d4), np.random.randn(d4,N4))
X5 = np.dot(np.random.randn(D,d5), np.random.randn(d5,N5))
X = np.c_[X1, X2, X3, X4, X5]
# X = np.insert(X, X.shape[0], 1, axis=0)

# Set SSC parameters
n = 5 # Number of subspaces to be found
r = 0 # Low-Dimensionality of the initial projection
affine = False # Set to true when data is in affine space rather than linear 
alpha = 20 # Regularization parameter of LASSO algorithm
outlier = False # Set to true if data has outliers
rho = 1 # Top elements used to form the Similarity Matrix

[groups, W] = SSC(X, n, r, affine, alpha, outlier, rho)
print groups
# print groups[:N1]
# print groups[N1:N1+N2]
# print groups[N1+N2:N1+N2+N3]
# print groups[N4:N5]
# print groups[N5:]

fig = plt.figure()

plots_per_line = 3
plot_lines = floor((n + 1)/plots_per_line)

ax = fig.add_subplot(plot_lines*100 + plots_per_line*10 + 1, projection='3d')
ax.scatter(X[0, :], X[1, :], X[2, :], c=groups, cmap=plt.cm.Spectral)
plt.axis('tight')
plt.xticks([]), plt.yticks([])

for j in range(n):
	Sj = np.where(groups == j)[0]
	Xj = X[:, Sj]
	Wj = W[Sj, :][:, Sj]

	M = np.dot(np.dot(np.dot(Xj, (np.identity(Wj.shape[0]) - Wj).T), np.identity(Wj.shape[0]) - Wj), Xj.T)
	[_, U] = eigh(M, eigvals=(D-2, D-1))

	Xp = np.dot(U.T, X)
	# Xpj = Xp[:, Sj]

	ax = fig.add_subplot(plot_lines*100 + plots_per_line*10 + 2 + j)
	# ax.scatter(Xpj[0, :], Xpj[1, :], c=groups[Sj], cmap=plt.cm.Spectral)
	ax.scatter(Xp[0, :], Xp[1, :], c=groups, cmap=plt.cm.Spectral)
	plt.axis('tight')
	plt.xticks([]), plt.yticks([])

	# Sj_out = np.where(groups != j)[0]
	# Xj_out = X[:, Sj_out]

	# for i in range(2, D):
	#  	[_, U] = eigh(M, eigvals=(D-i, D-1))

	#  	Xp = np.dot(U.T, X)

	#  	# print X.T

	#  	Xpj = Xp[:, Sj]
	#  	# print Xpj.T

	#  	L2in = np.zeros((Sj.shape[0], Sj.shape[0]))
	#  	for k in range(Sj.shape[0]):
	#  		for l in range(Sj.shape[0]):
	#  			# print np.linalg.norm(Xpj[:, k] - Xpj[:, l]), np.linalg.norm(Xj[:, k] - Xj[:, l])
	#  			L2in[k, l] = abs(np.linalg.norm(Xpj[:, k] - Xpj[:, l]) - np.linalg.norm(Xj[:, k] - Xj[:, l]))
	#  			# L2in[k, l] = np.linalg.norm(Xpj[:, k] - Xpj[:, l])
	#  	Ein = np.median(L2in)
	#  	# print L2in
	#  	print Ein

	#  	Xpj_out = Xp[:, Sj_out]
	#  	# print Xpj_out.T
	#  	L2out = np.zeros((Sj_out.shape[0], Sj_out.shape[0]))
	#  	for k in range(Sj_out.shape[0]):
	#  		for l in range(Sj_out.shape[0]):
	#  			L2out[k, l] = abs(np.linalg.norm(Xpj_out[:, k] - Xpj_out[:, l]) - np.linalg.norm(Xj_out[:, k] - Xj_out[:, l]))
	#  			# L2out[k, l] = np.linalg.norm(Xpj_out[:, k] - Xpj_out[:, l])
	#  	Eout = np.median(L2out)
	#  	# print L2out
	#  	print Eout

	#  	print j, i, Ein < Eout

	#Xp = DataProjection(X, 2)

plt.show()


# test_subspace = np.where(groups == 1)[0]
# Xj = X[:, test_subspace]
# Wj = W[test_subspace, :][:, test_subspace]

# S = np.dot(np.dot(np.dot(Xj, (np.identity(Wj.shape[0]) - Wj).T), np.identity(Wj.shape[0]) - Wj), Xj.T)
# [_, U] = eigh(S, eigvals=(0, 1))
# Xp = np.dot(U.T, X)
# print Xp.shape


##################################################################
##################################################################################################
# Calculates the regularization of the L1-Norm
#
#	Parameters:
#	  - X, Matrix DxN, dataset of N elements in D dimensions
#
#	Returns:
#	  - Lambda: Regularization Parameter for lambda*||C||_1 + 0.5 ||Y-YC||_F^2
##################################################################################################
##################################################################################################
# Recover a similarity graph from the dataset using ADMM algorithm.
#
#	Parameters:
# 	  - X: Matrix DxN, dataset of N elements in D dimensions
# 	  - affine: Bool, whether if the data lays on affine or linear space
# 	  - alpha: Float, coefficient of the ADMM
# 	  - thr: Float, threshold to stop ADMM
# 	  - max_iter: int, maximum of iterations that the ADMM will run
##################################################################################################
##################################################################################################
# Subspace Clustering algorithm
#
#   Parameters:
# 	  - X: Matrix DxN, dataset of N elements in D dimensions
# 	  - n: Int, number of subspaces to be found
#	  - r: Int, Low-Dimensionality to where the data will be reductioned before the SSC algorithm
# 	  - affine: Bool, whether if the data lays on affine or linear space
# 	  - alpha: Float, Coefficient of the ADMM
# 	  - outlier: Bool, whether the data has outliers or not
# 	  - rho: Int, Top elements used to create affinity matrix
#
#   Return
#	  - labels: Array N, the cluster label of each data element
##################################################################################################
# 

# # Creates a random dataset.
# D = 3
# d = 2
# d1 = 1
# d2 = 1
# N1 = 20
# N2 = 20
# A1 = np.dot(np.random.randn(D,d1), np.random.randn(d1,N1))
# A2 = np.dot(np.random.randn(D,d2), np.random.randn(d2,N2))
# A = np.c_[A1, A2]

# # Projection dimension: TODO

# # Sets for affine subspaces.
# Cst = 0

# # Regularization Parameter of Lasso Algorithm
# alpha = 0.001

# # Number of top coefficients to build affinity graph
# K = max(d1, d2) + Cst

# # Apply projection: TODO

# # Recovery Sparse Coeficient Matrix
# D = A.shape[0]
# N = A.shape[1]

# W = np.zeros((N, N))
# for i in range(N):
# 	y = A[:, i]
# 	X = np.delete(A, (i), axis=1)
	
# 	#TODO: Check if this solution is equal to Matlab's
# 	lassoreg = linear_model.Lasso(alpha=alpha, normalize=False, max_iter=1e5)
# 	lassoreg.fit(X, y)

# 	w = lassoreg.coef_
# 	w = np.insert(w, i, 0)

# 	W[:, i] = w

# # TODO: Detect and remove outliers

# # Build the adjacency matrix
# N = W.shape[0]
# W = abs(W)

# for i in range(N):
# 	W[:, i] = W[:, i]/max(W[:, i])

# W = W + W.T

# if K != 0:
# 	WK = np.zeros((N, N))
# 	for i in range(N):
# 		order = np.argsort(W[:, i])[::-1]
# 		for j in range(K):
# 			WK[order[j], i] = W[order[j], i]/W[order[0], i]
# 	W = WK + WK.T

# # for i in range(N):
# #	print i, np.sum(W[:N1, i]), np.sum(W[N1:, i])

# # Apply spectral clustering
# N = W.shape[0]

# DN = np.diag(1.0/np.sqrt(np.sum(W, axis=0) + np.finfo(float).eps))

# LapN = np.identity(N) - np.dot(DN, np.dot(W, DN))

# try:
# 	[_,_,vN] = np.linalg.svd(LapN)
# 	vN = vN.T

# 	kerN = vN[:, N-d:]

# 	normN = np.sum(kerN**2, axis=1)**0.5

# 	kerNS = kerN/(normN[:, None] + np.finfo(float).eps) # TODO: This might cause problems

# 	# print kerNS
# 	labels = cluster.KMeans(n_clusters=d, max_iter=1000, n_init=20).fit_predict(kerNS)
# 	print labels[:N1]
# 	print labels[N1:]

# 	fig = plt.figure()
# 	ax = fig.add_subplot(111, projection='3d')
# 	ax.scatter(A[0, :], A[1, :], A[2, :], c=labels, cmap=plt.cm.Spectral)
# 	plt.axis('tight')
# 	plt.xticks([]), plt.yticks([])
# 	plt.title('Attribute: %.3g'%d)

# 	plt.show()


# except ValueError:
# 	print "ERRO!!!"
# 	print LapN

###################################################################

# # Construct the Laplacian Graph.
# DN = np.identity(W.shape[0])
# for i in range(W.shape[0]):
# 	DN[i, i] = np.sum(W[i, :])
# L = DN - W

# # Calculate eigenvalues and eigenvectors
# # w, v = np.linalg.eig(L)
# _, v = eigh(L, eigvals=(0, 1))

# # Apply KMeans to the eigenvectors.
# labels = cluster.KMeans(n_clusters=d).fit_predict(v)
# print labels

#####################################################################

# W = np.zeros((A.shape[1], A.shape[1]))
# for i in range(A.shape[1]): # A.shape[1]
# 	if i % 100 == 0:
# 		print i
# 	b = A[:, i]
# 	A_ = np.delete(A, (i), axis=1)
	
# 	lassoreg = linear_model.Lasso(alpha=1e-2, normalize=False)
# 	lassoreg.fit(A_, b)
	
# 	w = lassoreg.coef_
# 	w = np.insert(w, i, 0)

# 	W[i, :] = abs(w)/max(abs(w))

# W = W + W.T

# # K = 5 # Number of no-zero coeff to form adjacency matrix
# # W_ = np.zeros(W.shape)
# # for i in range(W.shape[0]):
# #  	ind_order = np.argsort(W[i, :])[::-1]
# #  	for j in range(K):
# #  		W_[i, ind_order[j]] = W[i, ind_order[j]]/W[i, ind_order[0]]
# # W = W_ + W_.T

# # D = np.identity(W.shape[0]) * 1/np.sqrt(np.sum(W, axis=1))
# # L = np.identity(W.shape[0]) - np.dot(np.dot(D, W), D)
# # [u, s, v] = np.linalg.svd(L)

# # Kern = v[:, W.shape[0] - n + 1: W.shape[0]]
# # normN = np.sum(kerN**2, axis=1)**0.5

# # Construct the Laplacian Graph.
# D = np.identity(W.shape[0])
# for i in range(W.shape[0]):
# 	D[i, i] = np.sum(W[i, :])
# L = D - W

# # Calculate eigenvalues and eigenvectors
# # w, v = np.linalg.eig(L)
# w, v = eigh(L, eigvals=(0, 1))

# # Apply KMeans to the eigenvectors.
# labels = cluster.KMeans(n_clusters=2).fit_predict(v)
# print labels

# # plt.subplot(231 + d)
# # plt.tight_layout()
# # plt.plot(A[0, :], A[1, :], ".", c=labels)
# # plt.title('Attribute: %.3g'%d)

# # ax = fig.add_subplot(111, projection='3d')
# # ax.scatter(A[0, :], A[1, :], A[2, :], c=labels, cmap=plt.cm.Spectral)
# # plt.axis('tight')
# # plt.xticks([]), plt.yticks([])
# # plt.title('Attribute: %.3g'%d)

# # plt.show()