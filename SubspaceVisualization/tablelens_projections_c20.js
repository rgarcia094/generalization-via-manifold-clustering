d3.csv("Robot_20clusters_k=5/data.csv", function(error, data) {
d3.csv("Robot_20clusters_k=5/graph_ss.csv", function(error, graph) {
d3.json("Robot_20clusters_k=5/basis_he.json", function(error, basis) {
d3.json("Robot_20clusters_k=5/groups.json", function(error, subspaces) {

    // Shift angle values to positive range and sort the data
    data.forEach(function(d) {
        Object.ApllyOnProperties(d, parseFloat);
        d["Angle"] = Math.radians(d["Angle"]);
        d["Angle"] = (d["Angle"] < 0)*2*Math.PI + d["Angle"];

        d["Attributes"] = [];
        for (var i = 1; i <= 6; i++) {
            d["Attributes"][i-1] = d["Attribute"+i];
        }
        
        d["Attributes"][6] = 1;
    });
    data.sort(function(a, b) { return a["Angle"] > b["Angle"]; });

    console.log(data)

    // Map graph values to Integer
    graph.forEach(function(g) {
       Object.ApllyOnProperties(g, parseInt); 
    });

    // Create 2-D arrays to represent the basis
    basis.forEach(function(b) {
        var k = 0;
        
        b["BasisMatrix"] = new Array(b["Lines"]);
        
        for (var i = 0; i < b["Lines"]; i++){
            b["BasisMatrix"][i] = new Array(b["Columns"]);
        }

        for (var i = 0; i < b["Lines"]; i++) for (var j = 0; j < b["Columns"]; j++) {
            b["BasisMatrix"][i][j] = b["Basis"][k++];
        }
    });

    console.log(basis)

    var width = 1300*2, 
        height = 600;

    var svg = d3.select("body").append("svg")
        .attr("width", width)
        .attr("height", height);

   var order = [12, 2, 5, 10, 14, 6, 16, 15, 3, 4, 8, 9, 11, 13, 17, 1, 18, 7, 0, 19]

    for (var s1 = 0; s1 < basis.length; s1++) {
        var s = order[s1];

        var points = new Array(data.length);
        for (var i = 0; i < data.length; i++) {
            points[i] = new Array(basis[s]["Lines"] + 1);
        }

        for (var i = 0; i < data.length; i++) {
            points[i][0] = data[i]["Angle"]
            points[i][1] = Math.dotProduct(basis[s]["BasisMatrix"][0], data[i]["Attributes"]);
            points[i][2] = Math.dotProduct(basis[s]["BasisMatrix"][1], data[i]["Attributes"]);
        }

        console.log(points)

        var g = svg.append("g").attr("class", "tablelens_subspace" + s);

        var dimensions = 3;

        var barHeight = (height/2-60)/data.length;
        var offset = 30;
        var barWidth = width/(dimensions*basis.length/2) - offset;

        var colorScale = d3.scaleOrdinal(d3.schemeCategory10).domain([false, true]);

        g.selectAll("text")
            .data(["Angle", "PosX", "PosY"])
          .enter().append("text")
            .attr("x", function(_, i) { return (s1 % (basis.length/2))*width/(basis.length/2) + i*(barWidth + offset) + barWidth/2; })
            .attr("y", (s1 >= basis.length/2)? height : (height)/2)
            .style("font-family", "sans-serif")
            .style("text-anchor", "middle")
            .style("font-size", "14px")
            .text(function(d) { return d; })

        charts = []
        for (var attrN = 0; attrN < dimensions; attrN++) {
            var maxData = d3.max(points, function(d) { return d[attrN]; });
            var minData = d3.min(points, function(d) { return d[attrN]; });

            var wScale = d3.scaleLinear()
                .domain([minData, maxData])
                .range([5, barWidth]);

            var g1 = g.append("g");
            charts.push(g1);

            charts[attrN].selectAll("rect")
                .data(points)
              .enter().append("rect")
                .attr("x", (s1 % (basis.length/2))*width/(basis.length/2) + attrN*(barWidth + offset))
                .attr("y", function(_, i) { return ((s1 >= basis.length/2)? (height-15)/2 : 0) + barHeight*i + offset; })
                .attr("width", function(d) { return wScale(d[attrN]); })
                .attr("height", barHeight)
                .style("fill", function(_, i) {
                    return colorScale(subspaces[i] - 1 == s); 
                });

            var dAxis = d3.axisBottom(wScale)
                .ticks(2);

            charts[attrN].append("g")
                .attr("class", "axis")
                .attr("transform", "translate(" + 
                    ((s1 % (basis.length/2))*width/(basis.length/2) + (attrN*(barWidth + offset)) - 5)
                 + " , " + 
                    ((s1 >= basis.length/2)? (height-15)/2 + barHeight*points.length + offset : barHeight*points.length+offset) //(barHeight*points.length+1)
                 + ")")
                .call(dAxis);
        }
    }

    console.log(subspaces);

    svg.append("line")
        .attr("x1", 0)
        .attr("y1", height/2 + 10)
        .attr("x2", width)
        .attr("y2", height/2 + 10)
        .style("stroke-width", 2)
        .style("stroke", "black");

    for (var i = 1; i < basis.length/2; i++) {
        svg.append("line")
            .attr("x1", i*width/(basis.length/2) - 15)
            .attr("y1", 0)
            .attr("x2", i*width/(basis.length/2) - 15)
            .attr("y2", height)
            .style("stroke-width", 2)
            .style("stroke", "black");
    }

    /*Basis 2x7
    Data 7x360
    Points 2x360

    for i=1:360
        P[1, i] = B[1,:]*D[:, i]
        P[2, i] = B[2,:]*D[:, i]
    
    // Passar grps para um json
    // Colorir as bolinhas pelos grupos
    // Fazer interações das projeções

    */
});
});
});
});

// Applies the function func to all the object's properties
Object.ApllyOnProperties = function(obj, func) {
    for (var property in obj) {
        obj[property] = func(obj[property]);
    }
}


// Converts from degrees to radians.
Math.radians = function(degrees) {
    return degrees * Math.PI / 180;
};

Math.dotProduct = function(a, b) {
    var r = 0;
    for (var i = 0; i < a.length; i++) {
        r += a[i]*b[i];
    }
    return r;
}