var currentVisualization = "Projection";
var currentDataset = "Robot_2clusters";

createProjection(currentDataset);

function selectVisualization() {
    d3.select("body").select("svg").remove();

    switch (document.getElementById("projection_tablelens").value) {
        case "projection":
            currentVisualization = "Projection";
            createProjection(currentDataset);
            break;
        case "tablelens":
            currentVisualization = "TableLens";
            createTableLeans(currentDataset);
            break;
    }
}

function changeClusterization() {
    d3.select("body").select("svg").remove();

    switch (document.getElementById("number_clusters_selection").value) {
        case "c2":
            currentDataset = "Robot_2clusters";
            break;
        case "c3":
            currentDataset = "Robot_3clusters";
            break;
        case "c4":
            currentDataset = "Robot_4clusters";
            break;
        case "c5":
            currentDataset = "Robot_5clusters";
            break;
    }

    if (currentVisualization == "Projection") {
        createProjection(currentDataset);
    } else {
        createTableLeans(currentDataset);
    }
}

function createProjection (paste) {
    d3.json((paste + "/subspaces.json"), function(error, subspaces) {
    d3.json((paste + "/groups.json"), function(error, groups) {
    d3.json((paste + "/order.json"), function(error, order) {
    	var currentSubspace = 0;

    	var width = 1300, height = 600;

    	svg = d3.select("#projection_scatterplot").append("svg")
            .attr("width", width)
            .attr("height", height);

    	svg.append("text")
            .attr("class", "next_label")
            .attr("x", 800)
            .attr("y", 580)
            .text("Next")
            .on("click", function() {
                currentSubspace = (currentSubspace + 1) % subspaces.length;
                drawSubspace(order[currentSubspace]);
            });

        svg.append("text")
            .attr("class", "previous_label")
            .attr("x", 500)
            .attr("y", 580)
            .text("Previous")
            .on("click", function() {
                currentSubspace = (currentSubspace == 0)? subspaces.length - 1 : currentSubspace - 1;
                drawSubspace(order[currentSubspace]);
            });

        var hightlighted = false;
        svg.append("text")
            .attr("id", "subspace_name")
            .attr("x", 600)
            .attr("y", 580)
            .text("Highlight Subspace")
            .on("click", function () {
                if (!hightlighted) {
                    svg.selectAll("circle").attr("visibility", function(_, i) { return (groups[i] == order[currentSubspace])? "visible" : "hidden"; })
                    hightlighted = true;
                } else {
                    svg.selectAll("circle").attr("visibility", "visible");
                    hightlighted = false;
                }
            });

        drawSubspace(order[currentSubspace]);

        function drawSubspace(c) {
        	var points = subspaces[c].points;

        	var xScale = d3.scaleLinear()
                .domain([d3.min(points, function(p) { return p.x; }), d3.max(points, function(p) { return p.x; })])
                .range([0, 500]);

            var yScale = d3.scaleLinear()
                .domain([d3.min(points, function(p) { return p.y; }), d3.max(points, function(p) { return p.y; })])
                .range([500, 0]);

            var color = d3.scaleOrdinal(d3.schemeCategory20);

            var circles = svg.selectAll("circle")
            	.data(points, function(_, i) { return i; });

           	circles.enter().append("circle")
                .attr("cx", function(p) { return xScale(p.x) + 400; })
                .attr("cy", function(p) { return yScale(p.y) + 50; })
                .attr("r", 5)
                .style("fill", function(_, i) { return color(groups[i]); });

            circles.transition()
                .duration(1000)
                .attr("cx", function(p) { return xScale(p.x) + 400; })
                .attr("cy", function(p) { return yScale(p.y) + 50; })
                .attr("r", 5)
                .style("fill", function(_, i) { return color(groups[i]); });
        }
    });
    });
    });
}

function createTableLeans(paste) {
    d3.json((paste + "/subspaces.json"), function(error, subspaces) {
    d3.json((paste + "/groups.json"), function(error, groups) {
    d3.json((paste + "/order.json"), function(error, order) {
        var width = 1300, height = 600;

        var points = subspaces[0].points;

        var dimensions = 2;
        var barHeight = (height - 60)/points.length;
        var offset = 30;
        var barWidth = width/(dimensions*subspaces.length) - offset;

        svg = d3.select("#projection_scatterplot").append("svg")
            .attr("width", width)
            .attr("height", height);

        var maxX = d3.max(subspaces, function(s) { return d3.max(s.points, function(p) { return p.x; }); });
        var minX = d3.min(subspaces, function(s) { return d3.min(s.points, function(p) { return p.x; }); });

        var xScale = d3.scaleLinear()
            .domain([minX, maxX])
            .range([5, barWidth]);

        var maxY = d3.max(subspaces, function(s) { return d3.max(s.points, function(p) { return p.y; }); });
        var minY = d3.min(subspaces, function(s) { return d3.min(s.points, function(p) { return p.y; }); });

        var yScale = d3.scaleLinear()
            .domain([minY, maxY])
            .range([5, barWidth]);

        for (var ss = 0; ss < subspaces.length; ss++) {
            var s = order[ss];

            var points = subspaces[s].points;

            var g = svg.append("g").attr("class", "tablelens_subspace" + s);

            var colorScale = d3.scaleOrdinal(d3.schemeCategory10).domain([false, true]);

            g.selectAll("text")
                .data(["PosX", "PosY"])
              .enter().append("text")
                .attr("x", function(_, i) { return ss*width/(subspaces.length) +  i*(barWidth + offset) + barWidth/2; })
                .attr("y", height)
                .style("font-family", "sans-serif")
                .style("text-anchor", "middle")
                .style("font-size", "14px")
                .text(function(d) { return d; });

            var xg = g.append("g");

            xg.selectAll("rect")
                .data(points)
              .enter().append("rect")
                .attr("x", ss*width/(subspaces.length))
                .attr("y", function(_, i) { return barHeight*i + offset; })
                .attr("width", function(d) { return xScale(d.x); })
                .attr("height", barHeight)
                .style("fill", function(_, i) { return colorScale(groups[i] == s); });

            var xAxis = d3.axisBottom(xScale).ticks(10, "s");

            xg.append("g")
                .attr("class", "axis")
                .attr("transform", "translate(" + (ss*width/subspaces.length - 5) + " , " + (barHeight*points.length+offset) + ")")
                .call(xAxis);

            var yg = g.append("g");

            yg.selectAll("rect")
                .data(points)
              .enter().append("rect")
                .attr("x", ss*width/(subspaces.length) + barWidth + offset)
                .attr("y", function(_, i) { return barHeight*i + offset; })
                .attr("width", function(d) { return yScale(d.y); })
                .attr("height", barHeight)
                .style("fill", function(_, i) { return colorScale(groups[i] == s); });

            var yAxis = d3.axisBottom(yScale).ticks(10, "s");

            yg.append("g")
                .attr("class", "axis")
                .attr("transform", "translate(" + (ss*width/subspaces.length + barWidth + offset - 5) + " , " + (barHeight*points.length+offset) + ")")
                .call(yAxis);

            if (ss < subspaces.length - 1) {
                svg.append("line")
                    .attr("x1", (ss + 1)*width/(subspaces.length) - offset/2)
                    .attr("y1", 0)
                    .attr("x2", (ss + 1)*width/(subspaces.length) - offset/2)
                    .attr("y2", height)
                    .style("stroke-width", 2)
                    .style("stroke", "black");
            }
        }
    });
    });
    });
}