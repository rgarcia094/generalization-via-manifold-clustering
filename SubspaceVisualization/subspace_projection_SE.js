d3.csv("Robot_Data/data.csv", function(error, data) {
d3.csv("Robot_Data/graph_ss.csv", function(error, graph) {
d3.json("Robot_Data/basis_se.json", function(error, basis) {
d3.json("Robot_Data/groups.json", function(error, subspaces) {

    // Shift angle values to positive range and sort the data
    data.forEach(function(d) {
        Object.ApllyOnProperties(d, parseFloat);
        d["Angle"] = Math.radians(d["Angle"]);
        d["Angle"] = (d["Angle"] < 0)*2*Math.PI + d["Angle"];

        d["Attributes"] = [];
        for (var i = 1; i <= 6; i++) {
            d["Attributes"][i-1] = d["Attribute"+i];
        }
        
        d["Attributes"][6] = 1;
    });
    data.sort(function(a, b) { return a["Angle"] > b["Angle"]; });

    // Map graph values to Integer
    graph.forEach(function(g) {
       Object.ApllyOnProperties(g, parseInt); 
    });

    // Create 2-D arrays to represent the basis
    basis.forEach(function(b) {
        var k = 0;
        
        b["BasisMatrix"] = new Array(b["Lines"]);
        
        for (var i = 0; i < b["Lines"]; i++){
            b["BasisMatrix"][i] = new Array(b["Columns"]);
        }

        for (var i = 0; i < b["Lines"]; i++) for (var j = 0; j < b["Columns"]; j++) {
            b["BasisMatrix"][i][j] = b["Basis"][k++];
        }
    });

    var currentSubspace = 0;

    var width = 1300, 
        height = 600;

    svg = d3.select("#projection_scatterplot").append("svg")
        .attr("width", width)
        .attr("height", height)

    svg.append("text")
        .attr("class", "forwardtext")
        .attr("x", 800)
        .attr("y", 580)
        .text("Next")
        .on("click", function() {
            currentSubspace = (currentSubspace + 1) % 10;
            drawSubspace(order[currentSubspace]);
        });

    svg.append("text")
        .attr("class", "backtext")
        .attr("x", 500)
        .attr("y", 580)
        .text("Previous")
        .on("click", function() {
            currentSubspace = (currentSubspace == 0)? 9 : currentSubspace - 1;
            drawSubspace(order[currentSubspace]);
        });

    var order = [5, 9, 0, 8, 4, 3, 6, 7, 1, 2];
    //var nameOrder = ["Blue", "Orange", "Green", "Red", "Purple", "Brown", "Pink", "Gray", "Yellow", "Cyan"];
    var nameOrder = ["Green","Yellow","Cyan","Brown","Purple","Blue","Pink","Gray","Red","Orange"]

    svg.append("text")
        .attr("id", "subspacename")
        .attr("x", 600)
        .attr("y", 580)
        .text("Subspace: " + nameOrder[order[0]]);

    drawSubspace(order[currentSubspace])

    function drawSubspace(c) {
        var points = new Array(data.length);
        for (var i = 0; i < data.length; i++) {
            points[i] = new Array(basis[c]["Lines"]);
        }

        for (var i = 0; i < data.length; i++) {
            points[i][0] = Math.dotProduct(basis[c]["BasisMatrix"][0], data[i]["Attributes"]);
            points[i][1] = Math.dotProduct(basis[c]["BasisMatrix"][1], data[i]["Attributes"]);
        }

        console.log(data)
        console.log(basis)
        console.log(points)
        console.log(subspaces)

        var xScale = d3.scaleLinear()
            .domain([d3.min(points, function(p) { return p[0]; }), d3.max(points, function(p) { return p[0]; })])
            .range([0, 500])

        var yScale = d3.scaleLinear()
            .domain([d3.min(points, function(p) { return p[1]; }), d3.max(points, function(p) { return p[1]; })])
            .range([500, 0])

            console.log(xScale.domain())

        var color = d3.scaleOrdinal(d3.schemeCategory10);

        var circles = svg.selectAll("circle")
            .data(points, function(_, i) { return i; });

        circles.enter().append("circle")
            .attr("cx", function(d) { return xScale(d[0]) + 400; })
            .attr("cy", function(d) { return yScale(d[1]) + 50; })
            .attr("r", 5)
            .style("fill", function(_, i) { return color(subspaces[i]); });

        circles.transition()
            .duration(1000)
            .attr("cx", function(d) { return xScale(d[0]) + 400; })
            .attr("cy", function(d) { return yScale(d[1]) + 50; })
            .attr("r", 5)
            .style("fill", function(_, i) { return color(subspaces[i]); });

        svg.select("#subspacename")
            .text("Subspace: " + nameOrder[c])

    }

    /*Basis 2x7
    Data 7x360
    Points 2x360

    for i=1:360
        P[1, i] = B[1,:]*D[:, i]
        P[2, i] = B[2,:]*D[:, i]
    
    // Passar grps para um json
    // Colorir as bolinhas pelos grupos
    // Fazer interações das projeções

    */
});
});
});
});

// Applies the function func to all the object's properties
Object.ApllyOnProperties = function(obj, func) {
    for (var property in obj) {
        obj[property] = func(obj[property]);
    }
}


// Converts from degrees to radians.
Math.radians = function(degrees) {
    return degrees * Math.PI / 180;
};

Math.dotProduct = function(a, b) {
    var r = 0;
    for (var i = 0; i < a.length; i++) {
        r += a[i]*b[i];
    }
    return r;
}