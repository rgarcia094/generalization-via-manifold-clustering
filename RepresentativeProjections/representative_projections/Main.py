'''
Created on 04/08/2016

@author: Rafael
'''
import random

import numpy as np
import matplotlib.pyplot as plt

import FileReader as FR
import ElementsSelector as ES

from sklearn.cluster import KMeans
from sklearn.svm import SVR, LinearSVR
from sklearn.linear_model import Lasso
from sklearn.manifold import Isomap
from sklearn.decomposition import PCA
from copy import deepcopy

if __name__ == '__main__':
    [data, attr_names, data_names] = FR.csv_reader("../datasets/physfact_rev.csv", True, [i for i in range(2, 19)], 1)
    
    # Normalize data
    for i in range(data.shape[0]):
        e_min = np.min(data[i, :])
        e_max = np.max(data[i, :])
        data[i, :] = (data[i, :] - e_min)/(e_max - e_min)

    labels = KMeans(n_clusters=10).fit_predict(data.T)
    
    pca_projection = PCA(n_components=2).fit_transform(data.T)
    
    c_n = 0
    c_labels1 = deepcopy(labels)
    c_labels1[c_labels1 != c_n] = -1
    c_labels1[c_labels1 == c_n] = 1
     
    linear_svr1 = LinearSVR(C=1e5, epsilon=1e-5, max_iter=1e5)
    predict_data1 = linear_svr1.fit(data.T, c_labels1).predict(data.T)
#     print linear_svr1.coef_
     
    c_n = 4
    c_labels2 = deepcopy(labels)
    c_labels2[c_labels2 != c_n] = -1
    c_labels2[c_labels2 == c_n] = 1
     
    linear_svr2 = LinearSVR(C=1e5, epsilon=1e-5, max_iter=1e5)
    predict_data2 = linear_svr2.fit(data.T, c_labels2).predict(data.T)
#     print linear_svr2.coef_
    
    fig = plt.figure()
    ax = fig.add_subplot(141)
    ax.scatter(pca_projection[:, 0], pca_projection[:, 1], c=labels)
    for name, x, y in zip(data_names, pca_projection[:, 0], pca_projection[:, 1]):
        plt.annotate(name, xy=(x, y), xytext=(-20, 20), textcoords = 'offset points', ha = 'right', va = 'bottom', bbox = dict(boxstyle = 'round,pad=0.5', fc = 'yellow', alpha = 0.5), arrowprops = dict(arrowstyle = '->', connectionstyle = 'arc3,rad=0'))

    plt.axis('tight')
    plt.title('Random Data')
     
    ax = fig.add_subplot(142)
    ax.scatter(predict_data1, predict_data1, c=c_labels1)
    plt.axis('tight')
    plt.title('Regression 1')
      
    ax = fig.add_subplot(143)
    ax.scatter(predict_data2, predict_data2, c=c_labels2)
    plt.axis('tight')
    plt.title('Regression 2')
      
    ax = fig.add_subplot(144)
    ax.scatter(np.dot(data.T, linear_svr1.coef_), np.dot(data.T, linear_svr2.coef_), c=labels)
    for name, x, y in zip(data_names, np.dot(data.T, linear_svr1.coef_), np.dot(data.T, linear_svr2.coef_)):
        plt.annotate(name, xy=(x, y), xytext=(-20, 20), textcoords = 'offset points', ha = 'right', va = 'bottom', bbox = dict(boxstyle = 'round,pad=0.5', fc = 'yellow', alpha = 0.5), arrowprops = dict(arrowstyle = '->', connectionstyle = 'arc3,rad=0'))
    plt.axis('tight')
    plt.title('Comparation')
         
    plt.show()

#     pca_projection = PCA(n_components=2).fit_transform(data.T)
#     
#     fig = plt.figure()
#     ax = fig.add_subplot(111)
#     ax.scatter(pca_projection[:, 0], pca_projection[:, 1], c=labels)
#     for name, x, y in zip(data_names, pca_projection[:, 0], pca_projection[:, 1]):
#         plt.annotate(name, xy=(x, y), xytext=(-20, 20), textcoords = 'offset points', ha = 'right', va = 'bottom', bbox = dict(boxstyle = 'round,pad=0.5', fc = 'yellow', alpha = 0.5), arrowprops = dict(arrowstyle = '->', connectionstyle = 'arc3,rad=0'))
#     plt.axis('tight')
#     plt.title('Geo Data')
#     
#     plt.show()
    

#     n_samples = 10000
#     np.random.seed(60)
#     random.seed(60)
#     data = np.random.randn(2, n_samples)
#     for i in range(n_samples/10):
#         data[0, i*n_samples/10:(i+1)*n_samples/10] += (random.random() - 0.5)*20
#         data[1, i*n_samples/10:(i+1)*n_samples/10] += (random.random() - 0.5)*20
# 
#     [rep_data, rep_idx] = ES.RepresentativeElements(data, n_iteration=5)
# 
#     """ TODO: Test others clustering algorithms """ 
#     rep_labels = KMeans(n_clusters=7).fit_predict(rep_data.T)
    
#     c_n = 0
#     c_labels1 = deepcopy(rep_labels)
#     c_labels1[c_labels1 != c_n] = -1
#     c_labels1[c_labels1 == c_n] = 1
#     
#     linear_svr1 = LinearSVR(C=1e5, epsilon=1e-5, max_iter=1e5)
#     predict_data1 = linear_svr1.fit(rep_data.T, c_labels1).predict(rep_data.T)
#     print linear_svr1.coef_
#     
#     c_n = 1
#     c_labels2 = deepcopy(rep_labels)
#     c_labels2[c_labels2 != c_n] = -1
#     c_labels2[c_labels2 == c_n] = 1
#     
#     linear_svr2 = LinearSVR(C=1e5, epsilon=1e-5, max_iter=1e5)
#     predict_data2 = linear_svr2.fit(rep_data.T, c_labels2).predict(rep_data.T)
#     print linear_svr2.coef_
    
#     data_lasso = Lasso(alpha=1e-3, max_iter=1e4).fit(rep_data.T, c_labels).predict(rep_data.T)
    
#     svr_rbf = SVR(kernel='rbf', C=1e2, gamma=0.001, max_iter=1e4)
#     y_rbf = svr_rbf.fit(rep_data.T, c_labels).predict(rep_data.T)
#     print svr_rbf.support_vectors_
#     print rep_data.T[svr_rbf.support_, :]
#     w = svr_rbf.coef_ 
    
#     labels = np.zeros(n_samples)
#     labels[rep_idx] = 1
#     
#     labels = ES.AssociateToRepresentative(data, rep_idx, rep_labels)
    
#     fig = plt.figure()
#     ax = fig.add_subplot(141)
# #     ax.scatter(data[0, :], data[1, :], c=labels)
#     ax.scatter(data[0, :], data[1, :])
#     plt.axis('tight')
#     plt.title('Random Data')
#     
#     ax = fig.add_subplot(142)
#     ax.scatter(rep_data[0, :], rep_data[1, :], c=c_labels2)
#     plt.axis('tight')
#     plt.title('Representatives')
#     
#     ax = fig.add_subplot(143)
#     ax.scatter(predict_data2, predict_data2, c=c_labels2)
#     plt.axis('tight')
#     plt.title('Regression')
#     
#     ax = fig.add_subplot(144)
#     ax.scatter(np.dot(rep_data.T, linear_svr1.coef_), np.dot(rep_data.T, linear_svr2.coef_), c=rep_labels)
#     plt.axis('tight')
#     plt.title('Regression 2')
#     
#     plt.show()
    
    
    