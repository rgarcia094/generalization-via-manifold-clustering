'''
Created on 04/08/2016

@author: Rafael
'''
import numpy as np
import csv

from copy import deepcopy
   
def csv_reader(filename, has_attribute_names=False, data_indexes=[], label_index=-1):
    with open(filename) as csvfile:
        spamreader = csv.reader(csvfile, delimiter=",")

        data = np.zeros((1, len(data_indexes)))
        attr_names = None
        labels = []
            
        i = 0
        for row in spamreader:
            if i == 0 and has_attribute_names:
                indexes_names = deepcopy(data_indexes)
                if has_attribute_names:
                    indexes_names.append(label_index);
                indexes_names.sort()
                attr_names = np.array(row)[indexes_names]
                
            if label_index >= 0 and (i > 0 or not has_attribute_names):
                labels.append(row[label_index])
            
            if (i == 0 and not has_attribute_names) or i == 1:
                data = np.array(row)[data_indexes].astype(np.float)
            elif i > 0:
                data = np.vstack((data, np.array(row)[data_indexes].astype(np.float)))
                
            i += 1
            
        return [data.T, attr_names, labels]
