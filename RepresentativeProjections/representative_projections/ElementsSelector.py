'''
Created on 05/08/2016

@author: Rafael
'''
import numpy as np

def RepresentativeElements(A, n_iteration=1):
    original_indexes = np.array([i for i in range(A.shape[1])])
    
    for _ in range(n_iteration):
        N = A.shape[1]
        
        [_, _, V] = np.linalg.svd(A)
        P = np.sum(V[:, :]**2, axis=0)
        
        order = np.argsort(P)[::-1]
        idx = order[:int(N/2)]
        original_indexes = original_indexes[idx]        
        
        A = A[:, idx]
    
    return [A, original_indexes]

def AssociateToRepresentative(A, rep_idx, rep_labels):
    N = A.shape[1]
    
    labels_A = np.zeros(N)
    
    for i in range(N):
        if i in rep_idx:
            labels_A[i] = rep_labels[np.where(rep_idx == i)[0][0]]
        else:
            dist_func = lambda x: np.linalg.norm(x - A[:, i])
            dists = np.apply_along_axis(dist_func, axis=0, arr=A[:, rep_idx])
            closer_point = np.argmin(dists)
            labels_A[i] = rep_labels[closer_point]
    
    return labels_A

# from math import floor
#
# def RepresentativeElements(A, c=20, k=None):
#     if k == None:
#         k = int(floor(min(A.shape[0], A.shape[1])/2)) + 1
#     
#     [_, _, V] = np.linalg.svd(A)
#    
#     # P = np.sum(V[:k, :]**2, axis=0)
#     P = np.sum(V[:, :]**2, axis=0)
#     
#     order = np.argsort(P)[::-1]
#     idx = order[:c]
#     
#     return [A[:, idx], idx]