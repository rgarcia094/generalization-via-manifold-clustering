import matplotlib.pyplot as plt
import numpy as np

# Colormap definition
colormap = ['red', 'blue', 'gold', 'green', 'cyan', 'magenta', 'gray', 'orange', 'brown', 'violet', 'chartreuse', 'indianred',
			'skyblue', 'navy', 'black', 'mediumpurple']

def apply_colormap(c):
    return colormap[c % len(colormap)]
vec_apply_colormap = np.vectorize(apply_colormap)

dataset_name = "Obstacle_Step8"

errors_log = np.genfromtxt(dataset_name + "/training_error.csv", delimiter=",")
clusters = np.genfromtxt(dataset_name + "/clusters.csv", delimiter=",")[:, 0]

angles = errors_log[:, 0]
errors = errors_log[:, 1]

fig = plt.figure()
fig.suptitle('Training Error (' + dataset_name.replace('_', ' ') + ' Dataset)', fontsize=16, fontweight='bold')

fig.add_subplot(111)
plt.bar(angles, errors, color=vec_apply_colormap(clusters.astype(int)))
plt.xlabel('Angle')
plt.ylabel('Error')
plt.xlim([0, 360])
plt.ylim([0, 250])

plt.show()
