import numpy as np
import pickle

import matplotlib.pyplot as plt

from sklearn.gaussian_process import GaussianProcess
from scipy.stats import zscore

# Colormap definition
colormap = ['red', 'blue', 'gold', 'green', 'cyan', 'magenta', 'gray', 'orange', 'brown', 'violet', 'chartreuse', 'indianred',
			'skyblue', 'navy', 'black', 'mediumpurple']

def apply_colormap(c):
    return colormap[c % len(colormap)]
vec_apply_colormap = np.vectorize(apply_colormap)

# Read Dataset
dataset_name = 'Obstacle_Step8/FixedNugget'
csv_data = np.genfromtxt(dataset_name + '/clusters.csv', delimiter=',')
err_data = np.genfromtxt(dataset_name + '/training_error.csv', delimiter=',')

clusters = csv_data[:, 0]
angles = csv_data[:, 1]
robot = csv_data[:, 2:]
errors = err_data[:, 1]

n_clusters = np.max(clusters + 1)
x_range = np.arange(min(angles), max(angles)+1, 0.25)

fig = plt.figure()
fig.suptitle('Regression ' + dataset_name.replace('_', ' ') + ' (using Training Error)', fontsize=16, fontweight='bold')

regression_data = []
regression_error = np.zeros(robot.shape)
for i in range(robot.shape[1]):
    ax = fig.add_subplot(231 + i)
    regression_attr = []
    for j in range(int(n_clusters)):
        X = np.atleast_2d(angles[clusters == j]).T
        y = robot[clusters == j, i]

        # nugget = 1e-5*np.abs(zscore(errors[clusters == j]))
        nugget = 1e-5
        gp = GaussianProcess(corr='squared_exponential', theta0=1e-2, thetaL=1e-4, thetaU=1e-1, nugget=nugget)
        gp.fit(X, y)
        
        x = x_range[x_range >= min(angles[clusters == j])]
        x = x[x <= max(angles[clusters == j])]
        x = np.atleast_2d(x).T
        y_pred, _ = gp.predict(x, eval_MSE=True)

        if regression_attr == []:
            regression_attr = np.c_[np.atleast_2d(x), np.atleast_2d(y_pred).T]
        else:
            regression_attr = np.r_[regression_attr, np.c_[np.atleast_2d(x), np.atleast_2d(y_pred).T]]

        plt.scatter(X, y, s=1, color=apply_colormap(j))
        plt.plot(x, y_pred, color=apply_colormap(j))

        Y_pred = gp.predict(X)
        regression_error[clusters == j, i] = np.abs(y - Y_pred)

    plt.axis('tight')
    plt.title('Dimension ' + str(i+1), fontsize=12)

    if regression_data == []:
        regression_data = regression_attr
    else:
        regression_data = np.c_[regression_data, np.atleast_2d(regression_attr[:, 1]).T]

# np.savetxt(dataset_name + '/regression.csv', regression_data, fmt='%.4f', delimiter=',', newline='\n')
# np.savetxt(dataset_name + '/regression_error.csv', regression_error, fmt='%.4f', delimiter=',', newline='\n')

for i in range(regression_data.shape[0]):
    if regression_data[i, 0] == int(regression_data[i, 0]):
        filename = dataset_name + '/RegressionFiles/' + str(int(regression_data[i, 0])) + '.pkl'
    else:
        filename = dataset_name + '/RegressionFiles/' + str(regression_data[i, 0]) + '.pkl'
    param_file = open(filename, "w")
    pickle.dump([regression_data[i, 1:], [], 1, 0, 3], param_file)

###################################################

fig = plt.figure()
fig.suptitle(dataset_name.replace('_', ' ') + ' Regression Error (using Training Error)', fontsize=16, fontweight='bold')

for i in range(robot.shape[1]):
    fig.add_subplot(231 + i)
    plt.bar(angles, regression_error[:, i], color=vec_apply_colormap(clusters.astype(int)), edgecolor='none')
    plt.xlabel('Angle')
    plt.ylabel('Error')
    plt.xlim([0, 360])
    plt.ylim([0, 500])

###################################################

fig = plt.figure()
fig.suptitle(dataset_name.replace('_', ' ') + ' Regression Error (%) (using Training Error)', fontsize=16, fontweight='bold')

for i in range(robot.shape[1]):
    fig.add_subplot(231 + i)
    dim_range = np.max(robot[:, i]) - np.min(robot[:, i])
    plt.bar(angles, regression_error[:, i]/dim_range, color=vec_apply_colormap(clusters.astype(int)), edgecolor='none')
    plt.xlabel('Angle')
    plt.ylabel('Error')
    plt.xlim([0, 360])
    plt.ylim([0, 1])

###################################################

regression_normalized_error = np.sqrt(np.sum(np.power(regression_error, 2), axis=1))

fig = plt.figure()
fig.suptitle(dataset_name.replace('_', ' ') + ' Regression Normalized Error (using Training Error)', fontsize=16, fontweight='bold')

fig.add_subplot(111)
plt.bar(angles, regression_normalized_error, color=vec_apply_colormap(clusters.astype(int)))
plt.xlabel('Angle')
plt.ylabel('Error')
plt.xlim([0, 360])
plt.ylim([0, 500])

plt.show()