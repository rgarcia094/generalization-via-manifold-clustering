import pygame
import sys
sys.path.insert(0, "../")

import matplotlib.pyplot as plt

from dmp import DMP
from scipy import optimize
import numpy as np
import math
from Box2D import *
import pickle
from optparse import OptionParser
from domain import ResetWorld
from domain import SetJointsIteration
from domain import MoveJointsIteration
from domain import RunSimulation
from domain import DistanceToClosestObstacle
from domain import StartPDs
import math
import cma

# Global Constants
tau = 2.0

width = 1000
height = 1000

FPS = 60
dt = 1.0 / FPS
origin = (width / 2+150, (height / 4)*3 - 400)
dmp_dt = 0.1
fpsClock = None

target_x = 0.0
target_y = 0.0

def generate_dmps_from_parameters(params, num_basis, K, D, world, init_position):
    dmps_list = []
    num_tracking = len(init_position)
    goals = [ params[-3], params[-2], params[-1] ] if num_tracking == 3 else [world.domain_object.target_position[0],world.domain_object.target_position[1]]
    for i in range( num_tracking ):
        goal = goals[i]
        dmp = DMP(num_basis, K, D, init_position[i], goal)
        for j in range(num_basis):
            dmp.weights[j] = params[j+(num_basis*i)]
        dmps_list.append(dmp)
    return dmps_list


def generate_tool_parameters(params, num_basis, num_segments):
    tool_parameters = []
    tool_parameters.append( (100.0, 0.0) )
    tool_parameters.append( (50.0, math.pi / 2.0) )
    
    return tool_parameters


def generate_starts_and_goals_lists(goal_params, world):
    goals, starts = [], []
    for i in range(total_dmps):
        if i < num_reach_dmps-1:
            goals.append( goal_params[2*i] )
            goals.append( goal_params[2*i+1] )
        elif i == num_reach_dmps-1:
            goals.append( world.domain_object.body.position[0] )
            goals.append( world.domain_object.body.position[1] )
        elif i < total_dmps-1: #Skip the goal of reaching, that's fixed
            goals.append( goal_params[(2*(i-1))] )
            goals.append( goal_params[(2*(i-1)+1)] )
        else:
            goals.append( world.domain_object.target_position[0] )
            goals.append( world.domain_object.target_position[1] )

        if i == 0:
            starts.append( world.arm.pivot_position3[0]+world.arm.link3.length )
            starts.append( world.arm.pivot_position3[1] )
        else:
            starts.append( goals[-4] )
            starts.append( goals[-3] )

    return starts, goals

def positions_from_dmps(dmp_list):
    all_pos = [ [], [], [] ] if mode == "joints" else [ [], [] ]
    for i, dmp in enumerate(dmp_list):
        xpos, xdot, xddot, times = dmp.run_dmp(tau, dmp_dt, dmp.start, dmp.goal)
        for j in range( len(xpos) ):
            all_pos[ (i % len(all_pos)) ].append( xpos[j][0] )

    for i in range(len(all_pos)): #Append last goal position to the corresponding list
        all_pos[i].append( dmp_list[-len(all_pos)+i].goal )

    return all_pos

def get_initial_positions(world):
    global mode
    if mode == "xy":
        init_positions = [world.arm.pivot_position3[0]+world.arm.link3.length, world.arm.pivot_position3[1] ]
    else:
        init_positions = [world.arm.joint1.angle, world.arm.joint2.angle, world.arm.joint3.angle]
    return init_positions
    

def error_func(x):
    if type(x) is list:
        x = np.array(x).reshape(len(x),1)

    world = ResetWorld(origin, width, height, target_x, target_y)
    step = 0
    global mode
    global D
    global K

    init_position = get_initial_positions(world)
    dmps_list = generate_dmps_from_parameters(x, basis, K, D, world, init_position)

    all_pos = positions_from_dmps(dmps_list)

    trajectory_length = 0
    if mode == "xy":
        initial_distance = math.sqrt((world.arm.end_effector_position()[0] - all_pos[0][-1])**2 + (world.arm.end_effector_position()[1] - all_pos[1][-1])**2)
    else:
        initial_distance = math.sqrt((world.arm.joint1.angle - all_pos[0][-1])**2 + (world.arm.joint2.angle - all_pos[1][-1])**2 
            + (world.arm.joint3.angle - all_pos[2][-1])**2)

    initial_error = math.sqrt((world.domain_object.target_position[0] - world.arm.end_effector_position()[0])**2 
        + (world.domain_object.target_position[1] - world.arm.end_effector_position()[1])**2)

    thetas_reached = True
    pd_step = 0
    obstacle_penalty, max_obstacle_penalty = 0.0, 0.0

    arm_contact_cost = 10
    pd_penalty = 0.0
    max_pd_penalty = len(all_pos[0]) * 100.0

    while step < len(all_pos[0]) or thetas_reached == False:
        penalty = 0

        prev_pos = world.arm.end_effector_position()
        prev_thetas = [world.arm.joint1.angle, world.arm.joint2.angle, world.arm.joint3.angle]

        if thetas_reached == True:
            if len(all_pos) == 2: #xy coordinates
                theta1, theta2, theta3 = world.arm.inverse_kinematics_ccd(all_pos[0][step], all_pos[1][step])
            else:
                theta1, theta2, theta3 = all_pos[0][step], all_pos[1][step], all_pos[2][step]
            
            SetJointsIteration(theta1, theta2, theta3, world)
            step += 1
            #print "Step %d/%d" %(step, len(all_pos[0]))
            thetas_reached = False
            contact = 0
            pd_step = 0
        else:
            thetas_reached = MoveJointsIteration(world.arm.joint1, world.arm.joint2, world.arm.joint3)

        world.arm.set_pivot_positions()
        pd_step += 1
        world.Step(dt, 40, 40)
        world.ClearForces()
        if pd_step == 100:
            # print "Escaping..."
            penalty = 10000
            thetas_reached = True
            # break

        pd_penalty += 1.0
        new_pos = world.arm.end_effector_position()
        new_thetas = [world.arm.joint1.angle, world.arm.joint2.angle, world.arm.joint3.angle]

        if mode == "xy":
            trajectory_length += math.sqrt( (prev_pos[0] - new_pos[0])**2 + (prev_pos[1] - new_pos[1])**2 )
        else:
            trajectory_length += math.sqrt( (prev_thetas[0] - new_thetas[0])**2 + (prev_thetas[1] - new_thetas[1])**2 + (prev_thetas[2] - new_thetas[2])**2 )

        for edge in world.arm.link3.body.contacts:
            data1 = edge.contact.fixtureA.body.userData
            data2 = edge.contact.fixtureB.body.userData
            
            if data1 == "obstacle" or data2 == "obstacle":
                obstacle_penalty += arm_contact_cost

        max_obstacle_penalty += arm_contact_cost

    end_effector_position = world.arm.end_effector_position()
    error = math.sqrt( (world.domain_object.target_position[0] - end_effector_position[0])**2 + (world.domain_object.target_position[1] - end_effector_position[1])**2)

    cost = (error / initial_error) + (obstacle_penalty / max_obstacle_penalty) +  (pd_penalty / max_pd_penalty)

    global best_error
    global best_params
    global best_distance
    global best_goals

    if error <= best_distance:
        best_error = cost
        best_params = list(x)
        best_distance = error
        best_goals = list(goals_grid)

    # print "\nAvg Error: ", (total_error/total_steps)
    print "\nCost: ", cost
    print "Error: ", error
    print "Initial error: ", initial_error
    print "Params: ", x
    print "Best Error: ", best_distance
    print "Obstacle Penalty: ", obstacle_penalty
    print "Goals: ", goals_grid
    # print "Tool params: ", tool_parameters

    return cost


def diff_demonstration(demonstration, time):
    velocities = np.zeros( (len(demonstration), 1) )
    accelerations = np.zeros( (len(demonstration), 1) )

    times = np.linspace(0, time, num=len(demonstration))

    for i in range(1,len(demonstration)):
        dx = demonstration[i] - demonstration[i-1]
        dt = times[i] - times[i-1]

        velocities[i] = dx / dt
        accelerations[i] = (velocities[i] - velocities[i-1]) / dt

    velocities[0] = velocities[1] - (velocities[2] - velocities[1])
    accelerations[0] = accelerations[1] - (accelerations[2] - accelerations[1])

    return demonstration, velocities, accelerations, times


def seed_parameters(fileparams):
    params_file = open(fileparams, "r")
    data = pickle.load(params_file)
    params = data[0]
    global goals_grid
    global num_push_dmps
    global num_reach_dmps
    global basis
    global total_dmps

    goals_grid = data[1]
    num_push_dmps = data[2]
    num_reach_dmps = data[3]
    total_dmps = num_push_dmps + num_reach_dmps
    basis = data[4]

    epsilons = np.zeros( (basis*total_dmps+3) )
    epsilons[:] = 1.5

    return params, epsilons


def new_parameters():
    global mode
    if mode == "xy":
        params = np.zeros( ((basis*total_dmps*2 ), 1) )
        epsilons = np.zeros( ((basis*total_dmps*2 )) )
        epsilons[:] = 10.0
        params[:basis*total_dmps*2] = np.random.uniform(-100, 100)
    else:
        params = np.zeros( ((3*basis*total_dmps+3), 1) )
        epsilons = np.zeros( ((3*basis*total_dmps+3)) )

        for i in range(3*basis*total_dmps):
            params[i] = np.random.uniform(-100, 100)
            epsilons[i] = 10.0

        for i in range(3):
            params[3*basis*total_dmps+i] = np.random.uniform(-2.0*np.pi, 2.0*np.pi)
            epsilons[3*basis*total_dmps+i] = 0.01

    return params, epsilons


def start_training(theta=None, filesave=None, fileload=None, fileparams=None, render="false", angle='degree', xpos=None, ypos=None, new_basis=None, reach=None, push=None):
    global goals_grid
    global best_goals
    global best_params
    global num_push_dmps
    global num_reach_dmps
    global basis

    if new_basis is not None:
    	basis = new_basis
    if reach is not None:
    	num_reach_dmps = reach
    if push is not None:
    	num_push_dmps = push
    total_dmps = num_push_dmps + num_reach_dmps

    if theta is not None:
        if angle == "degree":
            theta = (theta * math.pi) / 180.0
        target_x = 200 * math.cos(theta)
        target_y = 200 * math.sin(theta)
    else:
    	target_x = 200.0 if xpos is None else xpos
        target_y = 0.0 if ypos is None else ypos
	
    global K
    global D
    K = 50.0
    D = 10.0

    if fileload is not None:
    	param_file = open(fileload, "r")
    	data = pickle.load(param_file)
    	result = data[0]
        best_goals = data[1]
        num_push_dmps = data[2]
        num_reach_dmps = data[3]
        total_dmps = num_push_dmps + num_reach_dmps
        basis = data[4]
    else:
    	params = None
        iterations = 2
        if fileparams is not None:
            params, epsilons = seed_parameters(fileparams)
            sigma = 20.0 if mode == "xy" else 1.0
            iterations = 1
            outer_iter = 1
        elif fileparams is None:
            sigma = 100.0 if mode == "xy" else 1.0
            params, epsilons = new_parameters()
            outer_iter = 3

	    max_vals = [1000.0] * len(goals_grid)
	    min_vals = [0.0] * len(goals_grid)
	    num_steps = 2

	    last_index = len(goals_grid)-1
	    for k in range(outer_iter):
	    	for i in range(iterations):
	    		sigma0 = sigma / (i+1)
	    		status_file = open("status.txt", "w")
	    		status_file.write("Theta: " + str(theta))
	    		status_file.write("Outer: " + str(k) + " Inner: " + str(i))
	    		status_file.close()

	    		global mode
                if mode == "xy":
                    params = params.reshape((params.shape[0],)).tolist()
                    result = cma.fmin(objective_function=error_func, x0=params, sigma0=sigma0, options={'maxiter':20})
                else:
	    			params = params.reshape((params.shape[0],)).tolist()
	    			result = cma.fmin(objective_function=error_func, x0=params, sigma0=sigma0, options={'maxiter':10})

				epsilons[:] = epsilons[:] / 10.0

                print 'Params', params
                print 'BestParams', best_params
                params = np.asarray(best_params)

            print "Best distance: ", best_distance

        print "Best error: ", best_error
        print "Best params: ", best_params
        print "Best goals: ", best_goals
        print "Best distances: ", best_distance, "\n\n"

        result = np.asarray(best_params)
        filename = "params.pkl" if filesave is None else filesave
        param_file = open(filename, "w")
        pickle.dump([result, best_goals, num_push_dmps, num_reach_dmps, basis], param_file)
        error_filename = filename.split(".")[0] + "_error.txt"
        error_file = open(error_filename, "w")
        error_file.write("Error: " + str(best_distance))
        error_file.write("\nCost: " + str(best_error))
        error_file.close()
        param_file.close()

    pygame.init()
    # display = pygame.display.set_mode((width+200, height))
    display = None
    fpsClock = pygame.time.Clock()

    world = ResetWorld(origin, width, height, target_x, target_y)

    init_position = get_initial_positions(world)
    dmps_list = generate_dmps_from_parameters(result, basis, K, D, world, init_position)

    all_pos = positions_from_dmps(dmps_list)
    positions_followed, positions_ccd, thetas, joint_thetas, error = RunSimulation(world, all_pos, display, height, target_x, target_y, dt, fpsClock, FPS, render)

    if fileload is None and filesave is not None:
    	pickle.dump(all_pos, open(filename.split(".")[0] + "_waypoints.pkl", "w"))
        pickle.dump(positions_ccd, open(filename.split(".")[0] + "_ccd_waypoints.pkl", "w"))
        pickle.dump(positions_followed, open(filename.split(".")[0] + "_trajectory.pkl", "w"))
        pickle.dump(thetas, open(filename.split(".")[0] + "_theta_waypoints.pkl", "w"))
        pickle.dump(joint_thetas, open(filename.split(".")[0] + "_theta_joints.pkl", "w"))

    del K
    del D

    return error

# theta = angle
# file = file to be saved or load
# save = true to save, false to load
# params = file with params (none if no params)
# render = true or false
# angle = string with degree or radians
def init_training(theta=1, filesave=None, fileload=None, fileparams=None, render="false", angle='degree', xpos=None, ypos=None, new_basis=None, reach=None, push=None):
    global basis
    global mode
    global best_error
    global best_params
    global best_distance
    global num_reach_dmps
    global num_push_dmps
    global total_dmps
    global goals_grid
    global best_goals

    StartPDs()

    basis = 3
    mode = "xy"
    best_error = float("inf")
    best_params = None
    best_distance = float("inf")
    num_reach_dmps = 0
    num_push_dmps = 1
    total_dmps = num_push_dmps + num_reach_dmps

    goals_grid = [ 0 ] * ((num_reach_dmps-1+num_push_dmps-1) * 2)
    best_goals = [ 0 ] * ((num_reach_dmps-1+num_push_dmps-1) * 2)

    result = start_training(theta, filesave, fileload, fileparams, render, angle, xpos, ypos, new_basis, reach, push)

    del basis
    del mode
    del best_error
    del best_params
    del best_distance
    del num_reach_dmps
    del num_push_dmps
    del total_dmps
    del goals_grid
    del best_goals

    return result


# final_error = init_training(theta=0, filesave='delete.pkl', render="false", angle='degree')
# print final_error
# final_error = init_training(theta=180, filesave='delete2.pkl', render="false", angle='degree')
# print final_error

import time

start = time.time()

circ_size = 360
training_set = - np.ones(circ_size)
training_errors = np.zeros(circ_size)
angle = 0
step = circ_size/2
counter = 0
while (counter < circ_size):
    if training_set[angle] == -1:
        training_set[angle] = 0
        filename = 'circular_training/' + str(angle) + '.pkl'
        training_errors[angle] = init_training(theta=angle, filesave=filename, render="false", angle='degree')
        counter += 1

    angle += step
    if angle >= circ_size:
        step /= 2
        angle = step

end = time.time()

elapsed = end - start

print "Final Time:", elapsed