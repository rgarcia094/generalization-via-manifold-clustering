import numpy as np
import matplotlib.pyplot as plt

dataset_name = 'Obstacle_Step8/FixedNugget'
err_data = np.genfromtxt(dataset_name + '/training_error_regression.csv', delimiter=',')

angles = err_data[:, 0]
errors = err_data[:, 1]

fig = plt.figure()
fig.suptitle(dataset_name.replace('_', ' ') + ' Regression Training Error', fontsize=16, fontweight='bold')

fig.add_subplot(111)
plt.bar(angles, errors, color='red', edgecolor='none')
plt.xlabel('Angle')
plt.ylabel('Error')
plt.xlim([0, 360])
plt.ylim([0, 250])

plt.show()