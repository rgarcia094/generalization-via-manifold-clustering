import numpy as np

from numpy.matlib import repmat
from numpy.linalg import inv, svd, norm, eig
from sklearn.cluster import KMeans

def error_coef(A, B):
	return np.sum(np.sum(np.abs(A - B)))/(B.shape[0]*B.shape[1])

def admm_optimization(X, v, lamb):
	mu = 10
	thr = np.array([10e-6, 10e-6, 10e-5])
	maxIter = 10e4

	N = X.shape[1]
	
	A = inv(np.dot(X.T, X) + mu*np.eye(N) + mu)
	C = np.zeros((N, 1))
	Z1 = np.zeros((N, 1))
	Lambda = np.zeros((N, 1))
	gamma = 0
	err = 10*thr
	
	i = 0
	while (err[0] > thr[0] or err[1] > thr[1] or err[2] > thr[2]) and i < maxIter:
		Z2 = np.dot(A, mu*C - Lambda + gamma)

		# print 'oi', (np.abs(mu*Z2 + Lambda) - np.atleast_2d(lamb*v).T).shape
		C = np.abs(mu*Z2 + Lambda) - np.atleast_2d(lamb*v).T
		# print 'tchau', C.shape
		C[C < 0] = 0
		C = C*np.sign(mu*Z2 + Lambda)
		C = C/mu

		Lambda = Lambda + mu*(Z2 - C)
		gamma = gamma + mu*(1 - np.sum(Z2, axis=0))

		err[0] = error_coef(Z2, C)
		err[1] = error_coef(np.sum(Z2, axis=0), np.ones((1, N)))
		err[2] = error_coef(Z1, Z2)

		Z1 = Z2
		i += 1

	return C

"""
	- A: Dataset matrix DxN.
	- k: Neighborhood size/limit
	- k_neighbors: Whether we must use the K nearest neighbors or all the neighbors at K distance.
	- lamb = Lasso coefficient
"""
def similarity_matrix(A, k=20, k_neighbors=True, lamb=10, ro=0.95):
	# Calc the distance between each data point.
	N = A.shape[1]
	A2 = np.sum(np.power(A, 2), axis=0)
	DistMat = np.sqrt(repmat(A2, N, 1) + repmat(A2, N, 1).T - 2*(np.dot(A.T, A)))

	# Build similarity matrix.
	W = np.zeros((N, N))
	for i in range(N):
		# Sort the dataset by the distance to the ith point.
		ids = np.argsort(DistMat[:, i])

		# Get the desirable neighborhood
		if k_neighbors:
			ids = ids[:k]
			k_n = k-1
		else:
			ids = ids[DistMat[ids, i] <= k]
			k_n = ids.shape[0]-1

		# Get the ith element
		y = A[:, ids[0]]

		# Excluding itself from neighborhood
		X = A[:, ids[1:]]

		# Subtracting the ith element from the neighborhood
		X = X - repmat(y, k_n, 1).T

		# Distance to each neighbor point
		v = DistMat[ids[1:], i]

		# Dividing each element in neighborhood by its distance to y
		for j in range(k_n):
			X[:, j] = X[:, j]/v[j]

		if (X.shape[1] > 0):
			# Solving Lasso optimization using ADD Framework
			c = admm_optimization(X, v/np.sum(v), lamb)

			# The ith column of W receives the normalized weights divided by v
			v = np.atleast_2d(v).T
			W[ids[1:], i] = (np.abs(c/v) / np.sum(np.abs(c/v)))[:, 0]

	# Processing the similarity matrix
	W = abs(W)
	Wp = np.zeros((N, N))
	for i in range(N):
		# Sort the column
		ind = np.argsort(W[:, i])[::-1]
		c_sorted = W[ind, i]

		# Sum all the weights
		total_sum = np.sum(c_sorted)

		stop = False
		cSum = 0
		t = 0
		while not stop:
			cSum += c_sorted[t]
			t += 1
			if cSum >= ro*total_sum:
				stop = True
				Wp[ind[:t], i] = W[ind[:t], i]

	# Symmetrize the adjacency matrix
	Wsym = np.maximum(Wp, Wp.T)

	return Wsym

def spectral_clustering(W, n=2, max_iter=10e3, replic=20):
	N = W.shape[0]

	D = np.diag(1/np.sqrt(np.sum(W, axis=0) + np.finfo(float).eps))
	L = np.eye(N) - np.dot(np.dot(D, W), D)
	[_, _, V] = svd(L)
	Y = V.T[:, V.shape[1] - n:]

	n_clusters = n
	for i in range(N):
		Y[i, :] = Y[i, :] / norm(Y[i, :] + np.finfo(float).eps)
		if norm(Y[i, :]) == 0:
			n_clusters = n + 1

	if n > 1:
		kmeans = KMeans(n_clusters=n_clusters, max_iter=int(max_iter), n_init=replic, init='k-means++').fit(Y)
		labels = kmeans.labels_
	else:
		labels = np.zeros(W.shape[0])

	return Y, labels

def elbow_method(W, max_n=30):
	sse = np.zeros(max_n)
	for i in range(1, max_n+1):
		Y, labels = spectral_clustering(W, n=i)
		for k in range(i+1):
			k_ind = labels == k
			mean = np.sum(Y[k_ind, :], axis=0)/k_ind[k_ind == True].shape[0]
			sse[i-1] += np.sum(np.sum(np.power((Y[k_ind, :] - repmat(mean, k_ind[k_ind == True].shape[0], 1)), 2)))
	return sse
	# return np.argsort(sse)[0] + 1

def eigengap_heuristic(W):
	N = W.shape[0]

	D = np.diag(1/np.sqrt(np.sum(W, axis=0) + np.finfo(float).eps))
	L = np.eye(N) - np.dot(np.dot(D, W), D)

	_, w = eig(L)
	return np.sort(np.abs(np.sum(w, axis=0)))

def iterative_subspace_clustering(A, k=20, k_neighbors=True, lamb=10, ro=0.95):
	W = similarity_matrix(A, k, k_neighbors, lamb, ro)

	elbow = np.max(np.argwhere(elbow_method(W) < 10e-20))
	print elbow

	if elbow == 0:
		return np.ones(W.shape[0])
	else:
		Y, labels = spectral_clustering(W, n=2)
		print labels
		for i in range(np.max(labels)+1):
			B = A[:, labels == i]
			labels[labels == i] = iterative_subspace_clustering(B, k=k, k_neighbors=k_neighbors, lamb=lamb, ro=ro)