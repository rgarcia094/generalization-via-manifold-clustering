import numpy as np
import manifold_clustering as mc
import matplotlib.pyplot as plt

# Colormap definition
colormap = ['red', 'blue', 'gold', 'green', 'cyan', 'magenta', 'gray', 'orange', 'brown', 'violet', 'chartreuse', 'indianred',
			'skyblue', 'navy', 'black', 'mediumpurple']

def apply_colormap(c):
    return colormap[c % len(colormap)]
vec_apply_colormap = np.vectorize(apply_colormap)

# Read Dataset
dataset_name = 'Obstacle_Step8'
csv_data = np.genfromtxt(dataset_name + '/dataset.csv', delimiter=',')
angles = csv_data[:, 0]
dataset = csv_data[:, 1:].T
print dataset.shape

# Build Similarity Matrix.
W = mc.similarity_matrix(dataset, k=10, k_neighbors=True)
_, labels = mc.spectral_clustering(W, n=7)

if 'Obstacle' in dataset_name:
	# Plot the clusterized dataset.
	fig = plt.figure()
	fig.suptitle('Subspace Clusterization (' + dataset_name.replace('_', ' ') + ' Dataset)', fontsize=16, fontweight='bold')
	for i in range(dataset.shape[0]):
		ax = fig.add_subplot(231 + i)
		plt.scatter(angles, dataset[i, :], s=1, color=vec_apply_colormap(labels))
		plt.title('Dimension ' + str(i+1), fontsize=12)
		plt.axis('tight')
else:
	# Plot the clusterized trajectory.
	fig = plt.figure()
	fig.suptitle('Subspace Clusterization (' + dataset_name.replace('_', ' ') + ' Dataset)', fontsize=16, fontweight='bold')
	ax = fig.add_subplot(111)
	for i in range(dataset.shape[1]):
		x_data = dataset[np.array(range(dataset.shape[0])) % 2 == 0, i]
		y_data = dataset[np.array(range(dataset.shape[0])) % 2 == 1, i]
		plt.plot(x_data, y_data, color=apply_colormap(labels[i]))
		plt.xlim([200, 1200])
		plt.ylim([200, 1000])

# Plot the elbow method.
elbow = mc.elbow_method(W)
fig = plt.figure()
fig.suptitle('Elbow Method (' + dataset_name.replace('_', ' ') + ' Dataset)', fontsize=16, fontweight='bold')
ax = fig.add_subplot(111)
plt.scatter(range(1, len(elbow)+1), elbow, s=5)
plt.xlabel('Number of Clusters')
plt.ylabel('Clusterization Error')
plt.axis('tight')

# Save clusterized dataset.
cluster_dataset = np.zeros((csv_data.shape[0], csv_data.shape[1] + 1))
cluster_dataset[:, 0] = labels.T
cluster_dataset[:, 1:] = csv_data
np.savetxt(dataset_name + '/clusters.csv', cluster_dataset, fmt='%.4f', delimiter=',', newline='\n')

plt.show()