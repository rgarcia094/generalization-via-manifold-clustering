function drawScatterplotMatrix(csvfile) {
	d3.select("body").select("svg").remove();


	//<link rel="stylesheet" type="text/css" href="Libraries/scatter-matrix.css">
	d3.select("head").append("link")
		.attr("class", "scatterplotstyle")
		.attr("rel", "stylesheet")
		.attr("type", "text/css")
		.attr("href", "Libraries/scatter-matrix.css");


    d3.select("#svg_container").selectAll("*").remove();
    d3.select("#svg_container")
        .style("width", "0px")
        .style("height", "0px");
    d3.select(".scatter-matrix-container").remove();

	var sm = new ScatterMatrix(csvfile);
    sm.render();
}