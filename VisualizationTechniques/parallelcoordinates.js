function drawParallelCoordinates(csvfile) {
	d3.csv(csvfile, function(error, data) {
		if (error || data.length == 0) {
            alert("Invalid Dataset!");
            return null;
        } else {
            current_dataset = csvfile;
        }

        d3.select("body").select("svg").remove();
        d3.select("#svg_container").selectAll("*").remove();
        d3.select(".scatter-matrix-container").remove();
        d3.select("head").select(".scatterplotstyle").remove();
        d3.select("#svg_container")
        	.style("width", "1300px")
    		.style("height", "620px");

		var attributes = Object.keys(data[0]).filter(function(d) { return d != "Class"; });
		var maxMatrix = d3.max(data, function(d) { return d[attributes[0]]; });
		var minMatrix = d3.min(data, function(d) { return d[attributes[0]]; });

		var classesSet = d3.set(data.map(function(d) { return d["Class"]; })).values().sort();
	    var colorScale = d3.scale.category20().domain(classesSet);

		var pc2 = d3.parcoords()("#svg_container")
			.data(data)
			.color(function(d) { return colorScale(d["Class"]); })
			.alpha(0.25)
			.margin({ top: 24, left: 0, bottom: 12, right: 0 })
	  	.render()
			.brushMode("1D-axes")
			.reorderable();
	});
}
