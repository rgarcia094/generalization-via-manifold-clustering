function drawTableLens(csvfile) {
    d3.csv(csvfile, function(error, data) {
        if (error || data.length == 0) {
            alert("Invalid Dataset!");
            return null;
        } else {
            current_dataset = csvfile;
        }

        d3.select("body").select("svg").remove();
        d3.select("#svg_container").selectAll("*").remove();
        d3.select(".scatter-matrix-container").remove();
        d3.select("head").select(".scatterplotstyle").remove();
        d3.select("#svg_container")
            .style("width", "0px")
            .style("height", "0px");
        var svg = d3.select("body").append("svg")
            .attr("width", width)
            .attr("height", height);

        var attributes = Object.keys(data[0]).filter(function(d) { return d != "Class"; });
        var dimensions = attributes.length;
        var barHeight = (height-60)/data.length;
        var offset = 30;
        var barWidth = width/dimensions - offset;

        var classesSet = d3.set(data.map(function(d) { return d["Class"]; })).values().sort();
        var colorScale = d3.scale.category20().domain(classesSet);

        data.forEach(function(d, i) {
            attributes.forEach(function(a) {
                d[a] = parseFloat(d[a]);
            });
            d["id"] = i;
        });

        svg.selectAll("text")
            .data(attributes)
          .enter().append("text")
            .attr("x", function(_, i) { return i*(barWidth + offset) + barWidth/2; })
            .attr("y", height-15)
            .style("font-family", "sans-serif")
            .style("text-anchor", "middle")
            .style("font-size", "14px")
            .text(function(d) { return d; })
            .on("click", function (d) { updateBars(d); });

        charts = []
        attributes.forEach(function(attrName, dim) {
            var maxData = d3.max(data, function(d) { return d[attrName]; });
            var minData = d3.min(data, function(d) { return d[attrName]; });

            var wScale = d3.scale.linear()
                .domain([minData, maxData])
                .range([5, barWidth]);

            var g = svg.append("g");
            charts.push(g);

            charts[dim].selectAll("rect")
                .data(data)
              .enter().append("rect")
                .attr("x", dim*(barWidth + offset))
                .attr("y", function(_, i) { return barHeight*i; })
                .attr("width", function(d) { return wScale(d[attrName]); })
                .attr("height", barHeight)
                .style("fill", function(d) { return colorScale(d["Class"]); });

            var dAxis = d3.svg.axis()
                .scale(wScale)
                .orient("bottom")
                .ticks(2);

            charts[dim].append("g")
                .attr("class", "axis")
                .attr("transform", "translate(" + (dim*(barWidth + offset)) + " , " + (barHeight*data.length+1) + ")")
                .call(dAxis);
        });

        var rollScale = d3.scale.quantize()
            .domain([0, height-60])
            .range(d3.range(0, height-60, barHeight));

        var dragger = d3.behavior.drag()
            .on("drag", function() { onDraggingBehavior(d3.mouse(this)[1]); })
            .on("dragend", onDraggingEnd);

        var hightLight = svg.append("g")
            .style("cursor", "move")
            .call(dragger);

        var hightLightBar = hightLight.append("rect")
            .attr("x", 0)
            .attr("y", 0)
            .attr("width", dimensions*(barWidth+offset))
            .attr("height", barHeight)
            .style("fill", "black");

        var hightLightLabels = hightLight.selectAll("text")
            .data(attributes)
          .enter().append("text")
            .attr("x", function(_, i) { return (barWidth + offset)*(i+1) - offset/2; })
            .attr("y", barHeight + 15)
            .style("font-family", "sans-serif")
            .style("text-anchor", "end")
            .style("font-size", "14px")
            .text(function(d) { return data[0][d]; })

        function onDraggingBehavior(mouseY) { 
            hightLightBar.attr("y", mouseY);

            hightLightLabels
                .attr("y", mouseY + barHeight + 15)
                .text(function(d) { return data[Math.round(rollScale(mouseY)/barHeight)][d]; });
        }

        function onDraggingEnd() {
            hightLightBar.attr("y", function() { return rollScale(parseInt(d3.select(this).attr("y"))); });

            hightLightLabels.attr("y", function() { return parseInt(hightLightBar.attr("y")) + barHeight + 15; });
        }

        function updateBars(key) {
            data.sort(function (a, b) { return a[key] - b[key]; });

            attributes.forEach(function(attrName, dim) {

                charts[dim].selectAll("rect")
                    .data(data, function(d) { return d["id"]; })
                  .transition()
                    .duration(1000)
                    .attr("y", function(_, i) { return barHeight*i; });
            });

            hightLightBar.transition()
                .duration(1000)
                .attr("y", 0);
        
            hightLightLabels.data(attributes).transition()
                .duration(1000)
                .attr("y", 14)
                .text(function(d) { return data[0][d]; })
        }
    });
}