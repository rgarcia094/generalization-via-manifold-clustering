var width = 1300, height = 620;

var current_dataset = "Datasets/wine.csv";
drawTableLens(current_dataset);

function loadDataset() {
    var dataset_name = document.getElementById("dataset_form").value.toLowerCase();
    dataset_name = "Datasets/" + dataset_name;
    if (!dataset_name.endsWith(".csv")) dataset_name += ".csv";

    changeVisualization(dataset_name);
}

function changeVisualization(dataset_name = current_dataset) {
    switch (document.getElementById("tool_selection").value) {
    	case "table_lens":
    		drawTableLens(dataset_name);
    		break;
    	case "radviz":
    		drawRadViz(dataset_name);
    		break;
    	case "parallel_coordinates":
    		drawParallelCoordinates(dataset_name);
    		break;
    	case "scatterplot_matrix":
    		drawScatterplotMatrix(dataset_name);
    		break;
    }
}

String.prototype.endsWith = function(suffix) {
    return this.indexOf(suffix, this.length - suffix.length) !== -1;
};