function drawRadViz(csvfile) {
    d3.csv(csvfile, function(error, data) {
        if (error || data.length == 0) {
            alert("Invalid Dataset!");
            return null;
        } else {
            current_dataset = csvfile;
        }

        d3.select("body").select("svg").remove();
        d3.select("#svg_container").selectAll("*").remove();
        d3.select(".scatter-matrix-container").remove();
        d3.select("head").select(".scatterplotstyle").remove();
        d3.select("#svg_container")
            .style("width", "0px")
            .style("height", "0px");
        var svg = d3.select("body").append("svg")
            .attr("width", width)
            .attr("height", height);

        var attributes = Object.keys(data[0]).filter(function(d) { return d != "Class"; });
        var dimensions = attributes.length;
        var dimLength = 250;

        var classesSet = d3.set(data.map(function(d) { return d["Class"]; })).values().sort();
        var colorScale = d3.scale.category20().domain(classesSet);

        data.forEach(function(d, i) {
            attributes.forEach(function(a) {
                d[a] = parseFloat(d[a]);
            });
            d["id"] = i;
        });

        var dimScales = [];
        createScales();

        var dragger = d3.behavior.drag()
            .on("drag", function() { onDraggingAxes(d3.select(this).datum(), d3.mouse(this), d3.select(this)); })
            .on("dragend", onDraggingEnd);

        var dimLabels = svg.selectAll("text")
            .data(attributes)
          .enter().append("text")
            .attr("x", function(_, i) { return width/2 + 1.1*dimLength*Math.cos(2*Math.PI/dimensions*i); })
            .attr("y", function(_, i) { return height/2 + 1.1*dimLength*Math.sin(2*Math.PI/dimensions*i); })
            .style("font-family", "sans-serif")
            .style("text-anchor", "middle")
            .style("font-size", "11px")
            .text(function(d) { return d; });

        var dimLines = svg.selectAll("line")
            .data(attributes)
          .enter().append("line")
            .attr("x1", width/2)
            .attr("y1", height/2)
            .attr("x2", function(_, i) { return width/2 + dimLength*Math.cos(2*Math.PI/dimensions*i); })
            .attr("y2", function(_, i) { return height/2 + dimLength*Math.sin(2*Math.PI/dimensions*i); })
            .style("stroke", "gray")
            .style("cursor", "move")
            .style("stroke-width", 2)
            .call(dragger);

        var points = svg.selectAll("circle")
            .data(data)
          .enter().append("circle")
            .attr("id", function(_, i) { return "circle" + i; })
            .attr("cx", function(d) { return calculateTranslation(d, "x"); })
            .attr("cy", function(d) { return calculateTranslation(d, "y"); })
            .attr("r", 5)
            .style("fill", function(d) { return colorScale(d["Class"]); })
            .on("mouseover", function() { showTooltip(this); })
            .on("mouseout", function() { d3.select("#tooltip").classed("hidden", true); });

        function showTooltip(elem) {
            var xPos = parseFloat(d3.select(elem).attr("cx"));
            var yPos = parseFloat(d3.select(elem).attr("cy"));

            var tooltip = d3.select("#tooltip")
                .style("left", xPos + "px")
                .style("top", yPos + "px")
              .select("#value").selectAll("p")
                .data(attributes)
                .text(function(attr, i) { return attr + ": " + d3.select(elem).datum()[attr]; });

            tooltip.exit().remove();

            tooltip.enter().append("p")
                .text(function(attr, i) { return attr + ": " + d3.select(elem).datum()[attr]; });

            d3.select("#tooltip").classed("hidden", false);
        }

        function calculateTranslation(d, v) {
            var len = (v == "x")? width : height;
            var fun = (v == "x")? Math.cos : Math.sin;

            var sum = 0, cv = 0;
            attributes.forEach(function(attr, i) {
                var axis = dimLength*fun(2*Math.PI/dimensions*i);
                cv += axis*dimScales[i](d[attr]);
                sum += dimScales[i](d[attr]);
            });

            return len/2 + 2*cv/sum;
        }

        function onDraggingAxes(datum, mouse, elem) {
            var angle = Math.atan2(mouse[1] - height/2, mouse[0] - width/2);

            elem.attr("x2", width/2 + dimLength*Math.cos(angle))
                .attr("y2", height/2 + dimLength*Math.sin(angle));

            dimLabels
                .attr("x", function(d) { return calcAxisRotation(d, datum, width, Math.cos, angle, d3.select(this).attr("x")); })
                .attr("y", function(d) { return calcAxisRotation(d, datum, height, Math.sin, angle, d3.select(this).attr("y")); })

            function calcAxisRotation(d, datum, size, func, angle, old) {
                if (d == datum) 
                    return size/2 + 1.1*dimLength*func(angle);
                else
                    return old;
            }
        }

        function onDraggingEnd() {
            attributes = []
            dimLines.each(function(d) {
                var angle = Math.atan2(d3.select(this).attr("y2") - height/2, d3.select(this).attr("x2") - width/2);
                attributes.push({elem: d3.select(this).datum(), angle: angle });
            });
            attributes.sort(function (a, b) { return a.angle - b.angle; });
            attributes = attributes.map(function(d) { return d.elem; });
            
            svg.selectAll("line")
                .data(attributes, function(d) { return d; })
              .transition()
                .duration(1000)
                .attr("x2", function(_, i) { return width/2 + dimLength*Math.cos(2*Math.PI/dimensions*i - Math.PI); })
                .attr("y2", function(_, i) { return height/2 + dimLength*Math.sin(2*Math.PI/dimensions*i - Math.PI); })

            svg.selectAll("text")
                .data(attributes, function(d) { return d; })
              .transition()
                .duration(1000)
                .attr("x", function(_, i) { return width/2 + 1.1*dimLength*Math.cos(2*Math.PI/dimensions*i - Math.PI); })
                .attr("y", function(_, i) { return height/2 + 1.1*dimLength*Math.sin(2*Math.PI/dimensions*i - Math.PI); })

            createScales();

            svg.selectAll("circle")
                .data(data, function(d) { return d["id"]})
              .transition()
                .duration(1000)
                .attr("cx", function(d) { return calculateTranslation(d, "x"); })
                .attr("cy", function(d) { return calculateTranslation(d, "y"); })
        }

        function createScales() {
            dimScales = [];
            attributes.forEach(function(attr, i) {
                var maxData = d3.max(data, function(d) { return d[attr]; });
                var minData = d3.min(data, function(d) { return d[attr]; });

                var dScale = d3.scale.linear()
                    .domain([minData, maxData])
                    .range([0, 1]);

                dimScales.push(dScale);
            });
        }
    });
}
