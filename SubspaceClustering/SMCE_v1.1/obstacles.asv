%------------------------------------------------------------------------
% Load robot data--------------------------------------------------------
%------------------------------------------------------------------------
data = csvread('train_obstacle.csv');
[~, order] = sort(data(:, 1));
data = data(order, :);
Y = data(:, 4:9)';

%------------------------------------------------------------------------
% Set SMCE parameters----------------------------------------------------
%------------------------------------------------------------------------
lambda = 10;        % Lasso coefficient
KMax = 15;          % Neighborhood size
dim = 2;            % Low-Dimensional embedding
n = 5;              % Number of manifolds
gtruth = [];        % Labels ground truth
verbose = false;    % Sparse optimization information

N = size(Y, 2);

%------------------------------------------------------------------------
% Solve the sparse optimization program----------------------------------
%------------------------------------------------------------------------
% Distance between each data element (????????)
Y2 = sum(Y.^2, 1);
Dist = sqrt(repmat(Y2, N, 1) + repmat(Y2', 1, N) - 2*(Y'*Y));

% Weight matrix used for clustering and dimensionality reduction
W = zeros(N, N);

% Solving the SMCE optimization program
for i = 1:N
    % KMax closest points to the ith element
    [~, ids] = sort(Dist(:, i), 'ascend');
    ids = ids(1:KMax);
	
    % ith element
    x = Y(:, ids(1));
    
    % KMax-1 closest neighbors (excluding itself)
    X = Y(:, ids);
    X(:, 1) = [];
    
    % Subtracting the ith element from the neighborhood (centring in 0?)
    X = X - repmat(x, 1, KMax - 1);
    
    % KMax-1 closest distances (excluding itself)
    v = Dist(ids, i);
    v(1) = [];

    % Dividing each elem in neighborhood by its distance to i
    for j = 1:KMax - 1
        X(:, j) = X(:, j) ./ v(j);
    end
    
%     % Printing verbose information
%     if (verbose)
%         fprintf('Point %4.0f, ', i);
%     end
    
    % Solving Lasso optimization using ADD Framework
    c = admm_vec_func(X, v./sum(v), lambda, verbose);
   
    % The ith column of W receives the normalized weights divided by v
    W(ids(2:KMax), i) = abs(c./v)/sum(abs(c./v));
end

%------------------------------------------------------------------------
% Processing the Afinity Matrix------------------------------------------
%------------------------------------------------------------------------
ro = 0.95;
C = W;

[m, N] = size(C);
Cp = zeros(m, N);

% Sort ascending the weights of each column (element)
[S, Ind] = sort(abs(C), 1, 'descend');

% For each element, take only ro percentage of all weights
for i=1:N
    % Sum of all weights for the ith element
    cL1 = sum(S(:, i));
    
    stop = false;
    cSum = 0;
    t = 0;
    
    while (~stop)
        t = t + 1;
        cSum = cSum + S(t, i); % Add a new weight
        
        % When ro is achieved, fill CP matrix
        if (cSum >= ro*cL1)
            stop = true;
            Cp(Ind(1:t, i), i) = C(Ind(1:t, i), i);
        end
    end
end

% Symmetrize the adjacency matrix
Wsym = max(abs(Cp), abs(Cp)');

%------------------------------------------------------------------------
% Clusterization Process-------------------------------------------------
%------------------------------------------------------------------------
MAXiter = 1000;
REPlic = 20;
N = size(Wsym, 1);

% Spectral Clusterization using normalized symmetric laplacian
D = diag(1./sqrt(sum(Wsym, 1) + eps));
L = eye(N) - D * Wsym * D;
[~, ~, V] = svd(L, 'econ');
Yn = V(:, end:-1:end-n+1);

for i = 1:N
	Yn(i, :) = Yn(i, 1:n) ./ norm(Yn(i, 1:n) + eps);
end

if n > 1
	clusters = kmeans(Yn(:, 1:n), n, 'maxiter', MAXiter, 'replicates', REPlic, 'EmptyAction', 'singleton');
else 
    clusters = ones(1, N);
end

Yj = V(:, end-1:-1:end-dim);
Yj = (D*Yj)';

%------------------------------------------------------------------------
% Perform Embedding------------------------------------------------------
%------------------------------------------------------------------------
for i = 1:n
    indg{i} = find(clusters == i);
    Ng(i) = length(indg{i});
    Wg{i} = Wsym(indg{i}, indg{i});
    Yg{i} = SpectralEmbedding(Wg{i}, dim);
    dim = size(Yg{i}, 2);
    Yg{i} = Yg{i}(:, 1:dim)'*sqrt(Ng(i));
    
    h = figure();
    scatter(Yg{i}(1, :), Yg{i}(2, :), [], clusters(indg{i}), 'filled');
end

%------------------------------------------------------------------------
% Separated Clustering Embedding-----------------------------------------
%------------------------------------------------------------------------
% Find Higher Eigenvalues Projections
for j=1:n
    Sj = find(clusters == j);
    
    Xj = Y(:, Sj);
    Wj = Wsym(Sj, Sj);
    
    Mj = Xj*(eye(size(Wj)) - Wj)'*(eye(size(Wj)) - Wj)*Xj';
    
    [V, D] = eigs(Mj);
    U = V(:, 1:2)';
    
    Xr = U*Y;
    
    h = figure();
    scatter(Xr(1, :), Xr(2, :), [], clusters, 'filled');
%     name = sprintf('he_cluster%d_projection.jpg', j);
%     saveas(h, name);
%     
%     name = sprintf('he_cluster%d_projection.txt', j);
%     fileID = fopen(name,'w');
%     fprintf(fileID,'%f, %f\n', Xr);
%     fprintf(fileID,'---------\n');
%     fprintf(fileID,'%f, %f\n', U); 
%     fclose(fileID);
end;

%------------------------------------------------------------------------
% Shows Scatterplots-----------------------------------------------------
%------------------------------------------------------------------------
% Shows the scatterplot
for j=4:9
    h = figure();
    scatter(data(:, 1), data(:, j), [], clusters, 'filled');
%     name = sprintf('dim%d_10clusters.jpg', j-3);
% 	  saveas(h, name);
end