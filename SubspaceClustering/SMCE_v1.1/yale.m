clear all; close all; clc; warning off;

%------------------------------------------------------------------------
% Load robot data--------------------------------------------------------
%------------------------------------------------------------------------
% data = csvread('data.csv');
% data(:, 1) = degtorad(data(:, 1));
% NegPos = find(data(:, 1) < 0);
% data(NegPos, 1) = 2*pi + data(NegPos, 1);
% [~, order] = sort(data(:, 1));
% data = data(order, :);
% Y = data(:, 4:9)';
load('yale_cropped3.mat');
Y = double(dataset);

%------------------------------------------------------------------------
% Set SMCE parameters----------------------------------------------------
%------------------------------------------------------------------------
lambda = 10;        % Lasso coefficient
useK = true;        % Whether use KMax or DMax
KMax = 20;          % Neighborhood size
DMax = 10000;       % Neighborhood distance
dim = 2;            % Low-Dimensional embedding
n = 3;              % Number of manifolds
gtruth = [];        % Labels ground truth
verbose = false;    % Sparse optimization information

N = size(Y, 2);

%------------------------------------------------------------------------
% Solve the sparse optimization program----------------------------------
%------------------------------------------------------------------------
% Distance between each data element (????????)
Y2 = sum(Y.^2, 1);
Dist = sqrt(repmat(Y2, N, 1) + repmat(Y2', 1, N) - 2*(Y'*Y));

% Weight matrix used for clustering and dimensionality reduction
W = zeros(N, N);

if (useK)
    % Solving the SMCE optimization program (using KMax)
    for i = 1:N
        % KMax closest points to the ith element
        [~, ids] = sort(Dist(:, i), 'ascend');
        ids = ids(1:KMax);

        % ith element
        x = Y(:, ids(1));

        % KMax-1 closest neighbors (excluding itself)
        X = Y(:, ids);
        X(:, 1) = [];

        % Subtracting the ith element from the neighborhood (centring in 0?)
        X = X - repmat(x, 1, KMax - 1);

        % KMax-1 closest distances (excluding itself)
        v = Dist(ids, i);
        v(1) = [];

        % Dividing each elem in neighborhood by its distance to i
        for j = 1:KMax - 1
            X(:, j) = X(:, j) ./ v(j);
        end

        % Printing verbose information
        if (verbose)
            fprintf('Point %4.0f, ', i);
        end

        % Solving Lasso optimization using ADD Framework
        c = admm_vec_func(X, v./sum(v), lambda, verbose);

        % The ith column of W receives the normalized weights divided by v
        W(ids(2:KMax), i) = abs(c./v)/sum(abs(c./v));
    end
else
    % Solving the SMCE optimization program (using DMax)
    for i = 1:N
        % Choose points with DMax max distance to the ith element
        [ord, ids] = sort(Dist(:, i), 'ascend');
        ids = ids(ord <= DMax);
        
        % Neighborhood size
        Ksize = size(ids, 1);
    	
        % ith element (the distance to itself is always the smaller [0])
        x = Y(:, ids(1));
        
        % Ksize-1 closest neighbors (excluding itself)
        X = Y(:, ids);
        X(:, 1) = [];
        
        % Subtracting the ith element from the neighborhood (centring in 0)
        X = X - repmat(x, 1, Ksize - 1);
        
        % Ksize-1 closest distances (excluding itself)
        v = Dist(ids, i);
        v(1) = [];
    
        % Dividing each elem in neighborhood by its distance to i (smaller the
        % distance, bigger the X element
        for j = 1:Ksize - 1
            X(:, j) = X(:, j) ./ v(j);
        end
        
        % Printing verbose information
        if (verbose)
            fprintf('Point %4.0f, ', i);
        end
        
        if (size(X, 2) > 0)
            % Solving Lasso optimization using ADD Framework
            c = admm_vec_func(X, v./sum(v), lambda, verbose);
       
            % The ith column of W receives the normalized weights divided by v
            W(ids(2:Ksize), i) = abs(c./v)/sum(abs(c./v));
        end
    end
end

%------------------------------------------------------------------------
% Processing the Afinity Matrix------------------------------------------
%------------------------------------------------------------------------
ro = 0.95;
C = W;

[m, N] = size(C);
Cp = zeros(m, N);

% Sort ascending the weights of each column (element)
[SortW, Ind] = sort(abs(C), 1, 'descend');

% For each element, take only ro percentage of all weights
for i=1:N
    % Sum of all weights for the ith element
    cL1 = sum(SortW(:, i));
    
    stop = false;
    cSum = 0;
    t = 0;
    
    while (~stop)
        t = t + 1;
        cSum = cSum + SortW(t, i); % Add a new weight
        
        % When ro is achieved, fill CP matrix
        if (cSum >= ro*cL1)
            stop = true;
            Cp(Ind(1:t, i), i) = C(Ind(1:t, i), i);
        end
    end
end

% Symmetrize the adjacency matrix
Wsym = max(abs(Cp), abs(Cp)');
% Wsym = max(abs(W), abs(W)');

%------------------------------------------------------------------------
% Number of Clusters-----------------------------------------------------
%------------------------------------------------------------------------
MAXiter = 1000;
REPlic = 20;
N = size(Wsym, 1);

% Spectral Clusterization using normalized symmetric laplacian
Dnc = diag(1./sqrt(sum(Wsym, 1) + eps));
Lnc = eye(N) - Dnc * Wsym * Dnc;
[Snc, Unc, Vnc] = svd(Lnc, 'econ');

% Calculate number of clusters using elbow method
maxC = 15;
sse = zeros(maxC, 1);
for k = 1:maxC
    Ync = Vnc(:, end:-1:end-k+1);
    for i = 1:N
        Ync(i, :) = Ync(i, 1:k) ./ norm(Ync(i, 1:k) + eps);
    end
    clusters = kmeans(Ync(:, 1:k), k, 'maxiter', MAXiter, 'replicates', REPlic, 'EmptyAction', 'singleton');
    sse(k) = 0;
    for ki = 1:k
        k_ind = find(clusters == ki);
        mean = sum(Ync(k_ind, 1:k), 1)/size(k_ind, 1);
        sse(k) = sse(k) + sum(sum((Ync(k_ind, 1:k) - repmat(mean, size(k_ind, 1), 1)).^2, 1));
    end
end
h = figure();
scatter(1:1:maxC, sse(:), 'filled');
if (useK)
    name = sprintf('kmax%d_elbow.jpg', KMax);
else
    name = sprintf('dmax%d_elbow.jpg', DMax);
end
saveas(h, name);

% Calculate number of clusters using eigengap heuristic 
[Vc, Dc] = eig(Lnc); % Probably we can use the S from [S U V] = svd
nc_a = sort(abs(sum(Dc, 1)), 'ascend');
nc = size(find(nc_a < 1e-3), 2);
h = figure(); 
scatter(1:1:maxC, nc_a(1, 1:maxC), 'filled');
if (useK)
    name = sprintf('kmax%d_eigengap.jpg', KMax);
else
    name = sprintf('dmax%d_eigengap.jpg', DMax);
end
saveas(h, name);

%------------------------------------------------------------------------
% Clusterization Process-------------------------------------------------
%------------------------------------------------------------------------
MAXiter = 1000;
REPlic = 20;
N = size(Wsym, 1);

% Spectral Clusterization using normalized symmetric laplacian
Dcp = diag(1./sqrt(sum(Wsym, 1) + eps));
Lcp = eye(N) - Dcp * Wsym * Dcp;
[~, ~, Vcp] = svd(Lcp, 'econ');
Yncp = Vcp(:, end:-1:end-n+1);

for i = 1:N
	Yncp(i, :) = Yncp(i, 1:n) ./ norm(Yncp(i, 1:n) + eps);
end

if n > 1
	clusters = kmeans(Yncp(:, 1:n), n, 'maxiter', MAXiter, 'replicates', REPlic, 'EmptyAction', 'singleton');
else 
    clusters = ones(1, N);
end

Yj = Vcp(:, end-1:-1:end-dim);
Yj = (Dcp*Yj)';

h = figure(); 
scatter(Yj(1, :), Yj(2, :), [], clusters, 'filled');
if (useK)
    name = sprintf('kmax%d_smce.jpg', KMax);
else
    name = sprintf('dmax%d_smce.jpg', DMax);
end
saveas(h, name);

%------------------------------------------------------------------------
% Clusters Dimensions----------------------------------------------------
%------------------------------------------------------------------------

% Calculating dimensions of each cluster
dim_clusters = zeros(n, 1);
for i = 1:n
    % Find elements in the ith cluster
    indc{i} = find(clusters == i);
    
    % Get their columns in the similarity matrix and sort them
    Ci{i} = abs(W(:, indc{i}));
    Ci_s{i} = sort(Ci{i}, 1, 'descend');
    
    % Calculates the median
    Ci_median{i} = median(Ci_s{i}, 2);
    
    % Find cluster dimension
    dim_clusters(i) = size(find(Ci_median{i} > 1e-2), 1) - 1;
    
    % Plot dimension prediction
    h = figure(); 
    scatter(1:1:10, Ci_median{i}(1:10), 'filled');
    if (useK)
        name = sprintf('kmax%d_dimcluster%i.jpg', KMax, i);
    else
        name = sprintf('dmax%d_dimcluster%i.jpg', DMax, i);
    end
    saveas(h, name);
end

% %------------------------------------------------------------------------
% % Perform Embedding------------------------------------------------------
% %------------------------------------------------------------------------
% for i = 1:n
%     indg{i} = find(clusters == i);
%     Ng(i) = length(indg{i});
%     Wg{i} = Wsym(indg{i}, indg{i});
%     Yg{i} = SpectralEmbedding(Wg{i}, dim);
%     dim = size(Yg{i}, 2);
%     Yg{i} = Yg{i}(:, 1:dim)'*sqrt(Ng(i));
%     
%     h = figure();
%     scatter(Yg{i}(1, :), Yg{i}(2, :), [], clusters(indg{i}), 'filled');
% end

%------------------------------------------------------------------------
% Separated Clustering Embedding-----------------------------------------
%------------------------------------------------------------------------
% % Find Higher Eigenvalues Projections
% for j=1:n
%     Sj = find(clusters == j);
%     
%     Xj = Y(:, Sj);
%     Wj = Wsym(Sj, Sj);
%     
%     Mj = Xj*(eye(size(Wj)) - Wj)'*(eye(size(Wj)) - Wj)*Xj';
%     
%     [Vce, Dce] = eigs(Mj);
%     Uce = Vce(:, 1:3)';
%     
%     Xr = Uce*Y;
%     
%     h = figure();
%     scatter(Xr(1, :), Xr(2, :), [], clusters, 'filled');
% %     if (useK)
% %         name = sprintf('kmax%d_projcluster%d.jpg', KMax, j);
% %     else
% %         name = sprintf('dmax%d_projcluster%d.jpg', DMax, j);
% %     end
% %     saveas(h, name);
% 
% %     name = sprintf('he_cluster%d_projection.txt', j);
% %     fileID = fopen(name,'w');
% %     fprintf(fileID,'%f, %f\n', Xr);
% %     fprintf(fileID,'---------\n');
% %     fprintf(fileID,'%f, %f\n', U); 
% %     fclose(fileID);
% end;

% % %------------------------------------------------------------------------
% % % Shows Scatterplots-----------------------------------------------------
% % %------------------------------------------------------------------------
% % % Shows the scatterplot
% % for j=4:9
% %     h = figure();
% %     scatter(data(:, 1), data(:, j), [], clusters, 'filled');
% % %     name = sprintf('dim%d_10clusters.jpg', j-3);
% % % 	  saveas(h, name);
% % end
% % 
% % % for j = 1:size(Y, 1)
% % %     Yg{i} = SpectralEmbedding(Wg{i}, j);
% % %     dim = size(Yg{i}, 2);
% % %     Yg{i} = Yg{i}(:, 1:dim)'*sqrt(Ng(i));
% % % 
% % %     Y2_g = sum(Yg{i}.^2, 1);
% % %     Dist_g = sqrt(repmat(Y2_g, size(Yg{i}, 2), 1) + repmat(Y2_g', 1, size(Yg{i}, 2)) - 2*(Yg{i}'*Yg{i}));
% % %     dim_error{i}(j) = sum(sum(abs(Dist(indg{i}, indg{i}) - Dist_g).^2, 1));
% % % end
% % % h = figure(); 
% % % scatter(1:1:size(Y, 1), dim_error{i}, 'filled');
