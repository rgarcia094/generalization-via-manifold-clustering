rng(100)

%------------------------------------------------------------------------
% Load trigo data--------------------------------------------------------
%------------------------------------------------------------------------
% Create sinoidal and cossinoidal samples
x1 = -1:.1:1;
y1 = -1:.1:1;
x1_ = zeros(size(x1, 2)*size(y1, 2), 1);
y1_ = zeros(size(x1, 2)*size(y1, 2), 1);
z1 = zeros(size(x1, 2)*size(y1, 2), 1);
for i = 1:size(x1, 2)
    for j = 1:size(y1, 2)
        x1_((i - 1)*size(y1, 2) + j) = x1(i);
        y1_((i - 1)*size(y1, 2) + j) = y1(j);
        z1((i - 1)*size(y1, 2) + j) = x1(i).^2 + y1(j).^2;
    end
end
S = [x1_ y1_ z1]';

x2 = -1:.1:1;
y2 = -1:.1:1;
x2_ = zeros(size(x2, 2)*size(y2, 2), 1);
y2_ = zeros(size(x2, 2)*size(y2, 2), 1);
z2 = zeros(size(x2, 2)*size(y2, 2), 1);
for i = 1:size(x2, 2)
    for j = 1:size(y2, 2)
        x2_((i - 1)*size(y2, 2) + j) = x2(i);
        y2_((i - 1)*size(y2, 2) + j) = y2(j);
        z2((i - 1)*size(y2, 2) + j) = x2(i).^3 + y2(j).^3;
    end
end
C = [x2_ y2_ z2]';

x3 = -1:.1:1;
y3 = -1:.1:1;
x3_ = zeros(size(x3, 2)*size(y3, 2), 1);
y3_ = zeros(size(x3, 2)*size(y3, 2), 1);
z3 = zeros(size(x3, 2)*size(y3, 2), 1);
for i = 1:size(x3, 2)
    for j = 1:size(y3, 2)
        x3_((i - 1)*size(y3, 2) + j) = x3(i);
        y3_((i - 1)*size(y3, 2) + j) = y3(j);
        z3((i - 1)*size(y3, 2) + j) = x3(i).^2 + y3(j).^3;
    end
end
Q = [x3_ y3_ z3]';

% Affine Space allowing translation
Ca = ones(size(C, 1) + 1, size(C, 2));
Ca(1:3, :) = C;
Sa = ones(size(S, 1) + 1, size(S, 2));
Sa(1:3, :) = S;
Qa = ones(size(Q, 1) + 1, size(Q, 2));
Qa(1:3, :) = Q;

% Pass data to high dimension
d = 3;
Ds = randn(d, size(Sa, 1)) * Sa;
Dc = randn(d, size(Ca, 1)) * Ca;
Dq = randn(d, size(Qa, 1)) * Qa;
Y = [Ds Dc Dq]; % Dl = [S C]

gtruth = [ones(1, size(Ds, 2)) 2*ones(1, size(Dc, 2)) 3*ones(1, size(Ds, 2))];
scatter3(Y(1, :), Y(2, :), Y(3, :), [], gtruth, 'filled');

%------------------------------------------------------------------------
% Set SMCE parameters----------------------------------------------------
%------------------------------------------------------------------------
lambda = 10;        % Lasso coefficient
KMax = 10;          % Neighborhood size
DMax = 1;           % Neighborhood distance
dim = 2;            % Low-Dimensional embedding
n = 3;              % Number of manifolds
gtruth = [];        % Labels ground truth
verbose = false;    % Sparse optimization information

N = size(Y, 2);

%------------------------------------------------------------------------
% Solve the sparse optimization program----------------------------------
%------------------------------------------------------------------------
% Distance between each data element
Y2 = sum(Y.^2, 1);
Dist = sqrt(repmat(Y2, N, 1) + repmat(Y2', 1, N) - 2*(Y'*Y));

% Weight matrix used for clustering and dimensionality reduction
W = zeros(N, N);

% % Solving the SMCE optimization program (using KMax)
% for i = 1:N
%     % KMax closest points (positions) to the ith element
%     [~, ids] = sort(Dist(:, i), 'ascend');
%     ids = ids(1:KMax);
% 	
%     % ith element (the distance to itself is always the smaller [0])
%     x = Y(:, ids(1));
%     
%     % KMax-1 closest neighbors (excluding itself)
%     X = Y(:, ids);
%     X(:, 1) = [];
%     
%     % Subtracting the ith element from the neighborhood (centring in 0)
%     X = X - repmat(x, 1, KMax - 1);
%     
%     % KMax-1 closest distances (excluding itself)
%     v = Dist(ids, i);
%     v(1) = [];
% 
%     % Dividing each elem in neighborhood by its distance to i (smaller the
%     % distance, bigger the X element
%     for j = 1:KMax - 1
%         X(:, j) = X(:, j) ./ v(j);
%     end
%     
%     % Printing verbose information
%     if (verbose)
%         fprintf('Point %4.0f, ', i);
%     end
%     
%     % Solving Lasso optimization using ADD Framework
%     c = admm_vec_func(X, v./sum(v), lambda, verbose);
%    
%     % The ith column of W receives the normalized weights divided by v
%     W(ids(2:KMax), i) = abs(c./v)/sum(abs(c./v));
% end

% Solving the SMCE optimization program (using DMax)
for i = 1:N
    % Choose points with DMax max distance to the ith element
    [ord, ids] = sort(Dist(:, i), 'ascend');
    ids = ids(ord <= DMax);
    
    % Neighborhood size
    Ksize = size(ids, 1);
	
    % ith element (the distance to itself is always the smaller [0])
    x = Y(:, ids(1));
    
    % Ksize-1 closest neighbors (excluding itself)
    X = Y(:, ids);
    X(:, 1) = [];
    
    % Subtracting the ith element from the neighborhood (centring in 0)
    X = X - repmat(x, 1, Ksize - 1);
    
    % Ksize-1 closest distances (excluding itself)
    v = Dist(ids, i);
    v(1) = [];

    % Dividing each elem in neighborhood by its distance to i (smaller the
    % distance, bigger the X element
    for j = 1:Ksize - 1
        X(:, j) = X(:, j) ./ v(j);
    end
    
    % Printing verbose information
    if (verbose)
        fprintf('Point %4.0f, ', i);
    end
    
    % Solving Lasso optimization using ADD Framework
    c = admm_vec_func(X, v./sum(v), lambda, verbose);
   
    % The ith column of W receives the normalized weights divided by v
    W(ids(2:Ksize), i) = abs(c./v)/sum(abs(c./v));
end

%------------------------------------------------------------------------
% Processing the Afinity Matrix------------------------------------------
%------------------------------------------------------------------------
ro = 0.95;
C = W;

[m, N] = size(C);
Cp = zeros(m, N);

% Sort ascending the weights of each column (element)
[S, Ind] = sort(abs(C), 1, 'descend');

% For each element, take only ro percentage of all weights
for i=1:N
    % Sum of all weights for the ith element
    cL1 = sum(S(:, i));
    
    stop = false;
    cSum = 0;
    t = 0;
    
    while (~stop)
        t = t + 1;
        cSum = cSum + S(t, i); % Add a new weight
        
        % When ro is achieved, fill CP matrix
        if (cSum >= ro*cL1)
            stop = true;
            Cp(Ind(1:t, i), i) = C(Ind(1:t, i), i);
        end
    end
end

% Symmetrize the adjacency matrix
Wsym = max(abs(Cp), abs(Cp)');

%------------------------------------------------------------------------
% Clusterization Process-------------------------------------------------
%------------------------------------------------------------------------
MAXiter = 1000;
REPlic = 20;
N = size(Wsym, 1);

% Spectral Clusterization using normalized symmetric laplacian
D = diag(1./sqrt(sum(Wsym, 1) + eps));
L = eye(N) - D * Wsym * D;
[~, ~, V] = svd(L, 'econ');
Yn = V(:, end:-1:end-n+1);

for i = 1:N
	Yn(i, :) = Yn(i, 1:n) ./ norm(Yn(i, 1:n) + eps);
end

if n > 1
	clusters = kmeans(Yn(:, 1:n), n, 'maxiter', MAXiter, 'replicates', REPlic, 'EmptyAction', 'singleton');
else 
    clusters = ones(1, N);
end

Yj = V(:, end-1:-1:end-dim);
Yj = (D*Yj)';

%------------------------------------------------------------------------
% Number of Clusters and Clusters Dimensions-----------------------------
%------------------------------------------------------------------------

% % Calculate number of clusters using gap statistic GIVING BAD RESULTS
% [opt_k, gaps] = gap_statistic(Yn(:, 1:n), [1 2 3 4 5], 20);

% Calculate number of clusters using elbow method
sse = zeros(10, 1);
for k = 1:10
    Yn = V(:, end:-1:end-k+1);
    for i = 1:N
        Yn(i, :) = Yn(i, 1:k) ./ norm(Yn(i, 1:k) + eps);
    end
    clusters = kmeans(Yn(:, 1:k), k, 'maxiter', MAXiter, 'replicates', REPlic, 'EmptyAction', 'singleton');
    sse(k) = 0;
    for ki = 1:k
        k_ind = find(clusters == ki);
        mean = sum(Yn(k_ind, 1:k), 1)/size(k_ind, 1);
        sse(k) = sse(k) + sum(sum((Yn(k_ind, 1:k) - repmat(mean, size(k_ind, 1), 1)).^2, 1));
    end
end
figure(); scatter(1:1:10, sse(:), 'filled');

% Calculate number of clusters using eigengap heuristic 
[Vc, Dc] = eig(L); % Probably we can use the S from [S U V] = svd
nc_a = sort(abs(sum(Dc, 1)), 'ascend');
figure(); scatter(1:1:size(abs(sum(Dc, 1)), 2), nc_a(1, :), 'filled');
nc = size(find(nc_a < 1e-3), 2);

% Calculating dimensions of each cluster
dim_clusters = zeros(n, 1);
for i = 1:n
    % Find elements in the ith cluster
    indc{i} = find(clusters == i);
    
    % Get their columns in the similarity matrix and sort them
    Ci{i} = abs(Wsym(:, indc{i}));
    Ci_s{i} = sort(Ci{i}, 1, 'descend');
    
    % Calculates the median
    Ci_median{i} = median(Ci_s{i}, 2);
    
    % Find cluster dimension
    dim_clusters(i) = size(find(Ci_median{i} > 1e-2), 1) - 1;
end

%------------------------------------------------------------------------
% Perform Embedding------------------------------------------------------
%------------------------------------------------------------------------
for i = 1:n
    indg{i} = find(clusters == i);
    Ng(i) = length(indg{i});
    Wg{i} = Wsym(indg{i}, indg{i});
    Yg{i} = SpectralEmbedding(Wg{i}, dim);
    dim = size(Yg{i}, 2);
    Yg{i} = Yg{i}(:, 1:dim)'*sqrt(Ng(i));
    
    h = figure();
    scatter(Yg{i}(1, :), Yg{i}(2, :), [], clusters(indg{i}), 'filled');
end

%------------------------------------------------------------------------
% Separated Clustering Embedding-----------------------------------------
%------------------------------------------------------------------------
% Find Higher Eigenvalues Projections
for j=1:n
    Sj = find(clusters == j);
    
    Xj = Y(:, Sj);
    Wj = Wsym(Sj, Sj);
    
    Mj = Xj*(eye(size(Wj)) - Wj)'*(eye(size(Wj)) - Wj)*Xj';
    
    [V, D] = eigs(Mj);
    U = V(:, 1:2)';
    
    Xr = U*Y;
    
    h = figure();
    scatter(Xr(1, :), Xr(2, :), [], clusters, 'filled');
%     name = sprintf('he_cluster%d_projection.jpg', j);
%     saveas(h, name);
%     
%     name = sprintf('he_cluster%d_projection.txt', j);
%     fileID = fopen(name,'w');
%     fprintf(fileID,'%f, %f\n', Xr);
%     fprintf(fileID,'---------\n');
%     fprintf(fileID,'%f, %f\n', U); 
%     fclose(fileID);
end