import numpy as np
import numpy.matlib

from sklearn.cluster import KMeans
from sklearn.decomposition import PCA

def DataProjection(X, r=0):
	if r == 0:
		return X
	else:
		X = X.T
		X = PCA(n_components=r).fit_transform(X)
		return X.T

def MatrixNormalize(Y):
	n = np.zeros((Y.shape[1], 1))
	Yn = np.zeros((Y.shape[0], Y.shape[1]))
	for i in range(Y.shape[1]):
		n[i] = np.linalg.norm(Y[:, i])
		Yn[:, i] = Y[:, i]/n[i, 0] # May be a problem
	return [Yn, n]

def ErrorLinearSystem(P, Z):
	[R, N] = Z.shape

	if R > N:
		E = np.dot(P[:, N:], Z[N:, :])
		Y = P[:, :N]
		Y0 = Y - E
		C = Z[:N, :]
	else:
		Y = P
		Y0 = P
		C = Z

	[Yn, n] = MatrixNormalize(Y0)
	M = np.matlib.repmat(n.T, Y.shape[0], 1)

	S = Yn - np.dot(Y, C)/M
	return np.sqrt(np.max(np.sum(S**2, axis=0)))

def LambdaMatrix(X, P):
	N = X.shape[1]

	T = np.dot(P.T, X)
	T[:N, :] = T[:N, :] - np.diag(np.diag(T[:N, :]))

	T = abs(T)
	return np.min(np.max(T, axis=0))

def SimilarityGraph(X, affine=False, alpha=800, thr=2e-4, max_iter=200):
	# Number of elements in the dataset
	N = X.shape[1]

	# Setting penalty parameters for the ADMM
	mu1 = alpha/LambdaMatrix(X, X)
	mu2 = alpha

	if not affine:
		# Initialization
		A = np.linalg.inv(mu1*np.dot(X.T, X) + mu2*np.identity(N))
		C1 = np.zeros((N, N))
		lambda2 = np.zeros((N, N))
		err = 10*thr

		i = 0
		while err > thr and i < max_iter:
			# Updating Z
			Z = np.dot(A, mu1*np.dot(X.T, X) + mu2*(C1 - lambda2/mu2))
			Z = Z - np.diag(np.diag(Z))

			# Updating C
			C_ = abs(Z + lambda2/mu2) - 1/mu2*np.ones((N, N))
			C_[C_ < 0] = 0
			C2 = C_ * np.sign(Z + lambda2/mu2)
			C2 = C2 - np.diag(np.diag(C2))

			# Updating Lagrande Multipliers
			lambda2 = lambda2 + mu2*(Z - C2)

			# Computing errors
			err = np.max(abs(Z - C2))

			C1 = C2
			i += 1
	else:
		# Initialization
		A = np.linalg.inv(mu1*np.dot(X.T, X) + mu2*np.identity(N) + mu2*np.ones((N, N)))
		C1 = np.zeros((N, N))
		lambda2 = np.zeros((N, N))
		lambda3 = np.zeros((1, N))
		err1, err2 = 10*thr, 10*thr

		i = 0
		while (err1 > thr or err2 > thr) and i < max_iter:
			# Updating Z
			Z = np.dot(A, mu1*np.dot(X.T, X) + mu2*(C1 - lambda2/mu2) + np.dot(mu2*np.ones((N, 1)), np.ones((1, N)) - lambda3/mu2))
			Z = Z - np.diag(np.diag(Z))

			# Updating C
			C_ = abs(Z + lambda2/mu2) - 1/mu2*np.ones((N, N))
			C_[C_ < 0] = 0
			C2 = C_ * np.sign(Z + lambda2/mu2)
			C2 = C2 - np.diag(np.diag(C2))

			# Updating Lagrande Multipliers
			lambda2 = lambda2 + mu2*(Z - C2)
			lambda3 = lambda3 + mu2*(np.dot(np.ones((1, N)), Z) - np.ones((1, N)))

			# Computing errors
			err1 = np.max(abs(Z - C2))
			err2 = np.max(abs(np.dot(np.ones((1, N)), Z) - np.ones((1, N))))

			C1 = C2
			i += 1

	return C2

# TODO: Version of Similarity Graph with outliers
def SimilarityGraphOutliers(X, affine=False, alpha=800, thr=2e-4, max_iter=200):
	[D, N] = X.shape

	gamma = alpha/np.linalg.norm(X, 1)
	P = np.c_[X, np.identity(D)/gamma] # This could be inverted

	# Setting penalty parameters for the ADMM
	mu1 = alpha/LambdaMatrix(X, P)
	mu2 = alpha

	if not affine:
		# Initialization
		A = np.linalg.inv(mu1*np.dot(P.T, P) + mu2*np.identity(N + D))
		C1 = np.zeros((N + D, N))
		lambda1 = np.zeros((D, N))
		lambda2 = np.zeros((N + D, N))
		err1, err2 = 10*thr, 10*thr

		i = 0
		while (err1 > thr or err2 > thr) and i < max_iter:
			# Updating Z
			Z = np.dot(A, np.dot(mu1*P.T, X + lambda1/mu1) + mu2*(C1 - lambda2/mu2))
			Z[:N, :] = Z[:N, :] - np.diag(np.diag(Z[:N, :]))

			# Updating C
			C_ = abs(Z + lambda2/mu2) - 1/mu2*np.ones((N+D, N))
			C_[C_ < 0] = 0
			C2 = C_ * np.sign(Z + lambda2/mu2)
			C2[:N, :] = C2[:N, :] - np.diag(np.diag(C2[:N, :]))

			# Updating Lagrande Multipliers
			lambda1 = lambda1 + mu1*(X - np.dot(P, Z))
			lambda2 = lambda2 + mu2*(Z - C2)

			# Computing Errors
			err1 = np.max(abs(Z - C2))
			err2 = ErrorLinearSystem(P, Z)

			C1 = C2
			i += 1
	else:
		# Initialization
		delta = np.r_[np.ones((N, 1)), np.zeros((D, 1))]
		A = np.linalg.inv(mu1*np.dot(P.T, P) + mu2*np.identity(N + D) + mu2*np.dot(delta, delta.T))
		C1 = np.zeros((N + D, N))
		lambda1 = np.zeros((D, N))
		lambda2 = np.zeros((N + D, N))
		lambda3 = np.zeros((1, N))
		err1, err2, err3 = 10*thr, 10*thr, 10*thr

		i = 0
		while (err1 > thr or err2 > thr or err3 > thr) and i < max_iter:
			# Updating Z
			Z = np.dot(A, mu1*np.dot(P.T, X + lambda1/mu1) + mu2*(C1 - lambda2/mu2) + mu2*np.dot(delta, np.ones((1, N)) - lambda3/mu2))
			Z[:N, :] = Z[:N, :] - np.diag(np.diag(Z[:N, :]))

			# Updating C
			C_ = abs(Z + lambda2/mu2) - 1/mu2*np.ones((N+D, N))	
			C_[C_ < 0] = 0
			C2 = C_ * np.sign(Z + lambda2/mu2)
			C2[:N, :] = C2[:N, :] - np.diag(np.diag(C2[:N, :]))

			# Updating Lagrande Multipliers
			lambda1 = lambda1 + mu1*(X - np.dot(P, Z))
			lambda2 = lambda2 + mu2*(Z - C2)
			lambda3 = lambda3 + mu2*(np.dot(delta.T, Z) - np.ones((1, N)))

			# Computing Errors
			err1 = np.max(abs(Z - C2))
			err2 = ErrorLinearSystem(P, Z)
			err3 = np.max(abs(np.dot(delta.T, Z) - np.ones((1, N))))

			C1 = C2
			i += 1

	return C2[:N, :]

# Creates a sparse representation of the matrix W removing the smaller components
def SparseRepresentation(W, rho=1):
	if rho >= 1:
		return W

	N = W.shape[1]
	W_ = np.zeros((N, N))

	for i in range(N):
		order = np.argsort(abs(W[:, i]))[::-1]
		sorted_w = abs(W[order, i])

		sum_w = np.sum(sorted_w[:])
		stop, cSum, t = False, 0, 0
		while not stop:
			cSum += sorted_w[t]
			if cSum >= rho*sum_w:
				stop = True
				W_[order[:t], i] = W[order[:t], i]
			t += 1
	return W_

def AdjacencyMatrix(W, k=0):
	N = W.shape[0]

	W = abs(W)

	if k == 0:
		for i in range(N):
			W[:, i] = W[:, i]/(np.max(W[:, i]) + np.finfo(float).eps)
	else:
		for i in range(N):
			order = np.argsort(W[:, i])[::-1]
			for j in range(k):
				W[order[j], i] = W[order[j], i]/(W[order[0], i] + np.finfo(float).eps)

	return W + W.T

def SpectralClustering(W, n):
	N = W.shape[0]

	max_iter = 1000
	n_init = 20

	D = np.diag(1.0/np.sqrt(np.sum(W, axis=0) + np.finfo(float).eps))
	L = np.identity(N) - np.dot(D, np.dot(W, D))

	[_,_,V] = np.linalg.svd(L)
	V = V.T

	K = V[:, N-n:]
	M = np.sum(K**2, axis=1)**0.5
	NS = K/(M[:, None] + np.finfo(float).eps) # TODO: This might cause problems

	groups = KMeans(n_clusters=n, max_iter=max_iter, n_init=n_init).fit_predict(NS)

	return groups

def SSC(X, n, r=0, affine=False, alpha=20, outlier=False, rho=1):
	# Project the data to r dimension
	X = DataProjection(X, r)

	# Creates a similarity graph where elements in the same subspace will have greater weights
	if not outlier:
		W = SimilarityGraph(X, affine, alpha)
	else:
		W = SimilarityGraphOutliers(X, affine, alpha)

	# Remove smallers elements of the matrix to make it sparse
	W = SparseRepresentation(W, rho)

	# Build adjacency matrix from the sparse similarity graph
	W = AdjacencyMatrix(W)

	# Apply Spectral Clustering in the adjacency matrix
	groups = SpectralClustering(W, n)

	return groups