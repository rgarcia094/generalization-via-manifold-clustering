clear all; close all; clc;

% Number of subspaces
n = 10;

% % High-Dimension
% D = 10;
% 
% % Dimensions of each subspace
% d1 = 1; d2 = 1; d3 = 1; d4 = 1; d5 = 1;
% 
% % Number of elements on each subspace
% N1 = 20; N2 = 20; N3 = 20; N4 = 20; N5 = 20;
% 
% % Elements on each subspace
% X1 = randn(D,d1) * randn(d1,N1);
% X2 = randn(D,d2) * randn(d2,N2) + 2.5;
% X3 = randn(D,d3) * randn(d3,N3) + 3.5;
% X4 = randn(D,d4) * randn(d4,N4) - 1.25;
% X5 = randn(D,d5) * randn(d5,N5) - 4;
% X = [X1 X2 X3 X4 X5];
% X = [X; ones(1, N1 + N2 + N3 + N4 + N5)];

% Get Robot Data
RobotData = csvread('data.csv');

% Change angles to radians
RobotData(:, 1) = degtorad(RobotData(:, 1));

% Shift to 0...2pi range
NegPos = find(RobotData(:, 1) < 0);
RobotData(NegPos, 1) = 2*pi + RobotData(NegPos, 1);

% Sort the data
[~, order] = sort(RobotData(:, 1));
RobotData = RobotData(order, :);

% Create a matrix using the 6D data.
X = ones(7, size(RobotData));
X(1:6, :) = RobotData(:, 4:9)';

% Pre-Projection dimension
r = 0;

% Affine constraint
Cst = 1;

% Optimization algorithm
OptM = 'L1Perfect';

% Regularization Parameter
lambda = 0.001;

% Coefficients to pick in the sparse matrix
K = 0;

% SSC Algorithm
Xp = DataProjection(X, r, 'NormalProj');
CMat = SparseCoefRecovery(Xp, Cst, OptM, lambda);
CKSym = BuildAdjacency(CMat, K);
Grps = SpectralClustering(CKSym, n);

% Shows the scatterplot
for j=4:9
    h = figure();
    scatter(RobotData(:, 1), RobotData(:, j), [], Grps, 'filled');
    name = sprintf('dim%d_10clusters.jpg', j-3);
	saveas(h, name);
end;

% Find Smaller Eigenvalues Projections
for j=1:n
    Sj = find(Grps == j);
    
    Xj = Xp(:, Sj);
    Wj = CKSym(Sj, Sj);
    
    Mj = Xj*(eye(size(Wj)) - Wj)'*(eye(size(Wj)) - Wj)*Xj';
    
    [V, D] = eigs(Mj);
    U = V(:, 1:2)'
    
    Xr = U*Xp;
    
    h = figure();
    scatter(Xr(1, :), Xr(2, :), [], Grps, 'filled');
    name = sprintf('se_cluster%d_projection.jpg', j);
    saveas(h, name);
end;

% Find Higher Eigenvalues Projections
for j=1:n
    Sj = find(Grps == j);
    
    Xj = Xp(:, Sj);
    Wj = CKSym(Sj, Sj);
    
    Mj = Xj*(eye(size(Wj)) - Wj)'*(eye(size(Wj)) - Wj)*Xj';
    
    [V, D] = eigs(Mj);
    U = V(:, size(V,2)-1:size(V,2))'
    
    Xr = U*Xp;
    
    h = figure();
    scatter(Xr(1, :), Xr(2, :), [], Grps, 'filled');
    name = sprintf('he_cluster%d_projection.jpg', j);
    saveas(h, name);
end;

G = zeros(n);
knn = 1;
for j=1:n
    Sj = find(Grps == j);
    Sjout = find(Grps ~= j);
    
    Xj = X(1:6, Sj);
    Xjout = X(1:6, Sjout);
    
    for i=1:size(Sj)
        x = Xj(:, i);
        [NPos, NDist] = knnsearch(Xjout', x', 'k', knn);
        G(j, Grps(Sjout(NPos))) = 1;
    end;    
end;
