% Create sinoidal and cossinoidal samples
x1 = -5:.1:5;
y1 = sin(x1);
S = [x1; y1];

x2 = -5:.1:5;
y2 = cos(x2);
C = [x2; y2];

% Affine Space allowing translation
Ca = ones(size(C, 1) + 1, size(C, 2));
Ca(1:2, :) = C;
Sa = ones(size(S, 1) + 1, size(S, 2));
Sa(1:2, :) = S;

% Pass data to high dimension
d = 5;
Ds = randn(d, size(Sa, 1)) * Sa;
Dc = randn(d, size(Ca, 1)) * Ca;
Dl = [Ds Dc]; % Dl = [S C];

% Using PCA
Dpca = DataProjection(Dl, 2, 'PCA'); % Dpca = Dl;

% Plotting PCA result
f1 = figure();
scatter(Dpca(1, :), Dpca(2, :), 'filled');

% Pre-Projection dimension
r = 0;
% Affine constraint
Cst = 1;
% Optimization algorithm
OptM = 'Lasso';
% Regularization Parameter
lambda = 0.001;
% Coefficients to pick in the sparse matrix
K = 0;
% Number of subspaces
n = 2;

% Affine space
Da = ones(size(Dl, 1) + 1, size(Dl, 2));
Da(1:size(Dl, 1), :) = Dl;
% Da = Dl;

% Insert Sin and Cos terms
Dsc = [Da sin(Da) cos(Da)]; % Dsc = [Da Da.^2 Da.^3];

% SSC Algorithm
Dp = DataProjection(Dsc, r, 'NormalProj');
CMat = SparseCoefRecoveryPoli(Dp, Cst, OptM, lambda, size(Da, 2)); % CMat = SparseCoefRecovery(Dp, Cst, OptM, lambda);

% Compact Matrix
CMatK = zeros(size(Da, 2), size(Da, 2));
for i = 0:2
    CMatK = CMatK + CMat(i*size(Da, 2)+1:(i+1)*size(Da, 2), 1:size(Da, 2));
end; % CMatK = CMat;

CKSym = BuildAdjacency(CMatK, K);
Grps = SpectralClustering(CKSym, n);

% Shows the scatterplot
f2 = figure();
scatter(Dpca(1, :), Dpca(2, :), [], Grps, 'filled');
