import sys
import SSC

import numpy as np
import matplotlib.pyplot as plt

from mpl_toolkits.mplot3d import Axes3D

# Reading input file
P = []
x = []
for line in sys.stdin:
    line = line.replace(",", "").replace(";", "")
    p = line.split()
    P.append([float(p[i]) if i > 0 or float(p[0]) >= 0 else 360 + float(p[0]) for i in range(len(p))])

P.sort(key=lambda column: column[:1], reverse=True)
P = np.array(P)

x = np.array(P[:, 0])
P = P[:, 3:P.shape[1]]

fig = plt.figure()

for i in range(P.shape[1]):
	A = np.ones((3, P.shape[0]))
	A[0, :] = x
	A[1, :] = P[:, i]

	labels = SSC.SSC(A, n=10, r=0, affine=True, alpha=20, outlier=False, rho=1)

	ax = fig.add_subplot(231 + i)
	ax.scatter(A[0, :], A[1, :], c=labels, cmap=plt.cm.Spectral)
	plt.axis('tight')
	plt.xticks([]), plt.yticks([])
	plt.title('Attribute: %.3g'%i)

plt.show()