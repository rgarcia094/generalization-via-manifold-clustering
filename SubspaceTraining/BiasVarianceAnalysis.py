""" TODO: 
		- Fixar o tamanho da amostra e o numero de tentativas
		- Plotar todas as regressoes produzidas
		- Perguntar no email o que seria mais correto
"""
import matplotlib.pyplot as plt
import numpy as np
import warnings

from sklearn.gaussian_process import GaussianProcess
from optparse import OptionParser
from matplotlib import cm
from sklearn.metrics.pairwise import euclidean_distances
from sklearn.metrics import mean_squared_error

if __name__ == '__main__':
	warnings.filterwarnings('ignore')

	# Retrieve params from the command line.
	usage = "usage: % prog [options] arg"
	parser = OptionParser(usage)
	parser.add_option("-f", "--folder", action="store", help="files location", type="string")
	(options, args) = parser.parse_args()
	folder = options.folder + '/'

	# Load datasets
	dataset = np.genfromtxt(folder + 'Training_Data.csv', delimiter=',')
	clusters = np.genfromtxt(folder + 'Training_Clusters.csv', delimiter=',').astype(int)

	angles = dataset[:, 0]
	data = dataset[:, 1:]
	n_clusters = np.max(clusters) + 1
	print n_clusters

	steps = [1, 2, 3, 4, 5, 6, 8, 9, 10, 12, 15, 18]
	samples_sizes = (360 / np.array(steps)).astype(int)
	mse = np.zeros(len(steps))

	j = 0
	for step in steps:
		print j
		training_tasks = np.atleast_2d(range(0, 360, step)).T
		clusters_tasks = clusters[training_tasks]

		s_param = np.zeros((360, 6))
		s_error = np.zeros(360)
		for c in range(n_clusters):
			cluster_samples = np.atleast_2d(training_tasks[clusters_tasks == c]).T
			
			f_elem = max(np.min(cluster_samples) - step, 0)
			l_elem = min(np.max(cluster_samples) + step, 359)
			cluster_range = np.array(range(f_elem, l_elem + 1));

			c_param = np.zeros((360, 6))
			c_error = np.zeros(360)
			for p in range(6):
				cluster_param = data[cluster_samples, p]

				gp = GaussianProcess(regr='linear', corr='squared_exponential', nugget=1e-3)
				gp.fit(cluster_samples, cluster_param)

				x_pred = np.atleast_2d(cluster_range).T
				y_pred = gp.predict(x_pred)

				c_param[cluster_range, p] = y_pred[:, 0]

			c_error[cluster_range] = np.linalg.norm(c_param[cluster_range, :] - data[cluster_range, :], axis=1)
			for e in cluster_range:
				if s_error[e] == 0 or c_error[e] < s_error[e]:
					s_error[e] = c_error[e]
					s_param[e, :] = c_param[e, :]

		# print s_param[:, 1]
		mse[j] = mean_squared_error(data, s_param)
		j += 1

	print samples_sizes
	print mse


				# 	bias_s += np.mean(np.abs(t_dataset[index_array, p] - y_pred[:, 0]))/(t*n_params*n_clusters)
				# 	# max_errors[s] = max(np.mean(np.abs(t_dataset[index_array, p] - y_pred[:, 0])), max_errors[s])
				# 	# if min_errors[s] == 0:
				# 	# 	min_errors[s] = np.mean(np.abs(t_dataset[index_array, p] - y_pred[:, 0]))
				# 	# else:
				# 	# 	min_errors[s] = min(np.mean(np.abs(t_dataset[index_array, p] - y_pred[:, 0])), min_errors[s])

		# biases_avrg[s] = bias_s #/n_params

	# tolerance = x_axis * 0 + biases_avrg[samples_max] * 0.1
	# bar_size = biases_avrg[samples_min:] - biases_avrg[samples_max]

	# # print biases_avrg

	# # print x_axis[felem:]
	# # print bar_size > tolerance[0,0]

	# # # Plot the biases
	# # t_samples = n_angles / np.array(range(1, s_interval + 1))

	# fig = plt.figure()
	# # fig.suptitle('Average Error Plot', fontsize=16, fontweight='bold')
	# fig.add_subplot(111)
	# plt.bar(x_axis, bar_size, color=['red' if x > tolerance[0] else 'green' for x in bar_size[:, 0]])
	# plt.plot(x_axis, tolerance)

	# # plt.scatter(x_axis[1:], biases_avrg[1:], s=5, color='black')
	# # plt.errorbar(x_axis[1:], biases_avrg[1:], [biases_avrg[1:] - min_errors[1:], max_errors[1:] - biases_avrg[1:]], linestyle='None')
	# # print [biases_avrg[1:5] - min_errors[1:5], max_errors[1:5] - biases_avrg[1:5]]
	# plt.axis('tight')
	# plt.xlabel('Number of Samples', fontsize=16, fontweight='bold')
	# plt.ylabel('Average Error', fontsize=16, fontweight='bold')

	# plt.show()