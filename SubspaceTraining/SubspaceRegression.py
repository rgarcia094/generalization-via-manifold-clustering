import numpy as np

# from sklearn.gaussian_process import GaussianProcessRegressor
# from sklearn.gaussian_process.kernels import RBF, ConstantKernel as CK

import warnings

from sklearn.model_selection import cross_val_score, train_test_split, cross_val_predict, KFold, GridSearchCV, LeaveOneOut
from sklearn.gaussian_process import GaussianProcess
from sklearn.metrics import mean_squared_error, r2_score, make_scorer
from sklearn.svm import SVR

class SubspaceRegression:

	def __init__(self, dataset, angles, clusters, errors, x_range, step):
		self.dataset = dataset
		self.angles = angles
		self.clusters = clusters
		self.errors = errors
		self.x_range = x_range
		self.step = step

	def apply_regression(self):
		warnings.filterwarnings('ignore')
		# self.step = 0
		# print self.clusters

		# The total number of subspaces
		n_clusters = int(np.max(self.clusters) + 1)
		
		# Matrix where the regression solutions will be saved
		solutions = np.zeros((n_clusters, self.x_range.shape[0], self.dataset.shape[1]))
		mse = np.zeros((n_clusters, self.dataset.shape[1]))
		gp_list = [[GaussianProcess() for j in range(self.dataset.shape[1])] for i in range(n_clusters)]

		# Loop over the dimensions
		for i in range(self.dataset.shape[1]):
			# print "Dimension " + str(i+1)

			# Loop over the different clusters
			for j in range(int(n_clusters)):
				# print "Cluster " + str(j+1)

				# Get all the points in the jth cluster
				X = np.atleast_2d(self.angles[self.clusters == j]).T
				y = self.dataset[self.clusters == j, i]

				# Create a gaussian process using the cluster data points
				if X.shape[0] > 1:
					# svr = SVR(kernel='rbf', C=1e1, gamma=1e-4)
					# svr.fit(X, y)

					n_splits = 4
					gp = GaussianProcess()
					grid = {'regr': ['constant','linear','quadratic'],
								  'corr': ['squared_exponential','cubic'],
								  'nugget': [1e-3, 1e-2] } # 1e-5
					# grid = {'regr': ['constant','linear','quadratic'],
								  # 'corr': ['absolute_exponential','squared_exponential','generalized_exponential','cubic','linear'],
								  # 'theta0': [1e-3, 1e-1, 1e1, 1e3],
								  # 'nugget': [1e-3, 1e-1, 1e1, 1e3] }
					cv = KFold(n_splits=n_splits, shuffle=True)
					scoring = 'neg_mean_squared_error'

					# print i, j
					# print X.shape
					# print y.shape

					g_search = GridSearchCV(estimator=gp, cv=cv, param_grid=grid, error_score=-10, scoring=scoring)
					g_search.fit(X, y)

					# print g_search.best_score_
					# print g_search.best_params_
					mse[j, i] = - g_search.best_score_
					gp_list[j][i] = g_search.best_estimator_

					gp = g_search.best_estimator_

					#####################################################################################

					# gp = GaussianProcess(regr='linear', corr='squared_exponential', nugget=1e-3)
					# gp = GaussianProcess(regr='quadratic', corr='cubic', nugget=1e-3)

					# n_splits = 10
					# kf = KFold(n_splits=n_splits, shuffle=True) # Pode ser necessario usar o Leave-One-Out LeaveOneOut()
					# r2, mse = 0, 0
					# for train, test in kf.split(X):
					# 	X_train, X_test, y_train, y_test = X[train], X[test], y[train], y[test]

					# 	gp.fit(X_train, y_train)

					# 	y_pred = gp.predict(X_test)

					# 	r2 += r2_score(y_test, y_pred)/n_splits
					# 	mse += mean_squared_error(y_test, y_pred)/n_splits
					# print r2, mse



					# score = cross_val_score(gp, X, y, cv=10, scoring=make_scorer(r2_score))
					# print score
					# X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.4)
					# gp.fit(X_train, y_train)

					# y_pred = gp.predict(X_test)

					#####################################################################

					# y_pred = gp.predict(X)
					# mse = mean_squared_error(y, y_pred)
					# print mse

					# gp.fit(X, y);

					# All subspace domain
					min_angle = max(np.min(X[:, 0]) - self.step, 0)
					max_angle = min(np.max(X[:, 0]) + self.step, 359)
					x_range = np.array(range(int(min_angle), int(max_angle) + 1))

					# Predict full subspace domain
					x = np.atleast_2d(x_range).T
					y_pred = gp.predict(x)
					# print np.mean(sigma)

					solutions[j, x_range, i] = y_pred
				else:
					solutions[j, :, i] = y

		return solutions, mse, gp_list


# Original Gaussian
# gp = GaussianProcess(corr='squared_exponential', theta0=1e-2, thetaL=1e-4, thetaU=1e-1, nugget=1e-5)

# # Gaussian with less parameters
# gp = GaussianProcess(regr='linear', corr='squared_exponential', nugget=1e-3)

# Gaussian using error as nugget
# gp = GaussianProcess(regr='linear', corr='squared_exponential', nugget=1e-3*(1/self.errors[self.clusters == j]))

# SVR
# gp = SVR(kernel='rbf', C=1e5, gamma=1e-2)

# Predict the values of all angles using the gaussian process
# x = np.atleast_2d(self.x_range).T
