import numpy as np
import pickle
import time
import sys

from SubspaceClustering import SubspaceClustering
from SubspaceRegression import SubspaceRegression
from RobotSimulator import Simulator

save_folder_name = 'RegressionSolution/'

# Dummy training function
def training_point(angle=0, file=None):
	if file is None:
		folder_name = 'TrainingFiles/'
		file_name = folder_name + str(angle) + '.pkl'
	else:
		file_name = file

	params_file = open(file_name, "r")
	params = pickle.load(params_file)[0]

	return Simulator().start_training(theta=angle, fload=params, render=False)

# Dummy save solution function
def save_solution(angle, params):
	param_file = open(save_folder_name + str(angle) + '.pkl', "w")
	pickle.dump([params, [], 0, 0, 0], param_file)
	param_file.close()

if __name__ == '__main__':
	# Start time counter
	start_time = time.time()

	# Interval between the training points 
	step = 8

	# Number of points in the circle
	n_angles = 360

	# Dimension of each high-dimensional point
	n_params = 6

	# Error threshold to use random seed
	err_limit = 50
	err_threshold = 10

	# Array with the training dataset
	training_data = np.genfromtxt(save_folder_name + 'Training_Data.csv', delimiter=',')[:, 1:]
	training_errors = np.genfromtxt(save_folder_name + 'Training_Error.csv', delimiter=',')

	# Training angles
	angles = np.array(range(0, n_angles, step))
	x_range = np.array(range(n_angles))

	# Apply the subspace clusterization algorithm
	sc = SubspaceClustering(dataset=training_data.T, n_clusters=7, k=10, k_neighbors=True)
	c_labels = sc.apply_clustering()

	# Apply regression for each subspace,
	sr = SubspaceRegression(dataset=training_data, angles=angles, clusters=c_labels, errors=training_errors, x_range=x_range)
	regressions = sr.apply_regression()

	# Choose the best solution for each angle.
	r_solutions = np.zeros((n_angles, n_params))
	r_sol_errors = np.zeros(n_angles)
	r_sol_clusters = np.zeros(n_angles)
	r_sol_trajec = None
	for i in range(regressions.shape[1]):
		# Initialize best solution with the one in the first regression.
		b_param = regressions[0, i, :]
		_, b_error, b_traj = Simulator().start_training(theta=i, fload=b_param, render=False)
		r_sol_clusters[i] = 0

		# Looping along the regressions.
		for j in range(1, regressions.shape[0]):
			c_param = regressions[j, i, :]
			_, c_error, c_traj = Simulator().start_training(theta=i, fload=c_param, render=False)

			# If new regression is better, update.
			if b_error > c_error:
				b_error = c_error
				b_param = c_param
				b_traj = c_traj
				r_sol_clusters[i] = j

		b_traj = np.array(b_traj)[:, :, 0]

		# If i is one of the training data, check if the original solution is better.
		if i % step == 0 and training_errors[i/step] < b_error:
			b_error = training_errors[i/step]
			b_param = training_data[i/step, :]
			r_sol_clusters[i] = c_labels[i/step]

		# Save the best solution to file.
		save_solution(i, b_param)

		# Update the trajectories matrix
		r_solutions[i, :] = b_param
		r_sol_errors[i] = b_error
		if r_sol_trajec is None:
			length = 2*b_traj.shape[1]
			r_sol_trajec = np.zeros(length)
			r_sol_trajec[np.array(range(length)) % 2 == 0] = b_traj[0, :]
			r_sol_trajec[np.array(range(length)) % 2 == 1] = b_traj[1, :]
			r_sol_trajec = r_sol_trajec.reshape(1, r_sol_trajec.shape[0])
		else:
			crt_length = 2*b_traj.shape[1]
			new_trajec = np.zeros(crt_length)
			new_trajec[np.array(range(crt_length)) % 2 == 0] = b_traj[0, :]
			new_trajec[np.array(range(crt_length)) % 2 == 1] = b_traj[1, :]
			new_trajec = new_trajec.reshape(1, new_trajec.shape[0])

			old_length = r_sol_trajec.shape[1]
			if crt_length > old_length:
				last_value = r_sol_trajec[:, -2:]
				new_block = np.tile(last_value, (crt_length - old_length)/2)
				r_sol_trajec = np.c_[r_sol_trajec, new_block]
			elif crt_length < old_length:
				last_value = new_trajec[:, -2:]
				new_block = np.tile(last_value, (old_length - crt_length)/2)
				new_trajec = np.c_[new_trajec, new_block]

			r_sol_trajec = np.r_[r_sol_trajec, new_trajec]

	np.savetxt(save_folder_name + 'Regression_Data.csv', r_solutions, fmt='%.4f', delimiter=',', newline='\n')
	np.savetxt(save_folder_name + 'Regression_TrainingError.csv', r_sol_errors, fmt='%.4f', delimiter=',', newline='\n')
	np.savetxt(save_folder_name + 'Trajectories_Data.csv', r_sol_trajec, fmt='%.4f', delimiter=',', newline='\n')
	np.savetxt(save_folder_name + 'Regression_Clusters.csv', r_sol_clusters, fmt='%.4f', delimiter=',', newline='\n')
	# np.savetxt(save_folder_name + 'Training_Data.csv', original_training, fmt='%.4f', delimiter=',', newline='\n')
	# np.savetxt(save_folder_name + 'Training_Error.csv', training_errors, fmt='%.4f', delimiter=',', newline='\n')
	np.savetxt(save_folder_name + 'Training_Clusters.csv', c_labels, fmt='%.4f', delimiter=',', newline='\n')

	# Shows execution time
	end_time = time.time()
	elapsed_time = end_time - start_time
	print "Final Time:", elapsed_time
