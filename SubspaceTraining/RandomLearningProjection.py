import matplotlib.pyplot as plt
import numpy as np
import pickle

from sklearn.decomposition import PCA
from optparse import OptionParser

def pickle_read(file):
	params_file = open(file, "r")
	return pickle.load(params_file)

if __name__ == '__main__':
	# Retrieve folder from the command line
	usage = "usage: % prog [options] arg"
	parser = OptionParser(usage)
	parser.add_option("-a", "--angle", action="store", help="angle", type="string")
	parser.add_option("-f", "--folder", action="store", help="files location", type="string")
	(options, args) = parser.parse_args()
	folder = options.folder + '/'
	angle = options.angle

	# Read Datasets
	dataset = np.genfromtxt(folder + 'Parameters.csv', delimiter=',')
	errors = np.genfromtxt(folder + 'Errors.csv', delimiter=',')

	# Apply dimensionality reduction
	dataset_2d = PCA(n_components=2).fit_transform(dataset)

	fig = plt.figure()
	fig.suptitle('Performance Error Cost Function (Angle ' + str(angle) + ')', fontsize=16, fontweight='bold')

	# Visualize the 2d dataset.
	fig.add_subplot(234)
	plt.title('PCA Projection', fontsize=12)
	plt.scatter(dataset_2d[:, 0], dataset_2d[:, 1], s=50, c=errors, cmap=plt.cm.cool)
	plt.axis('tight')

	# Boxplot with the error.
	fig.add_subplot(235)
	plt.title('Performance Error BoxPlot')
	plt.boxplot(errors, showmeans=True, whis='range')
	plt.annotate(np.median(errors), xy=(1, np.median(errors)), xytext=(-20, 0), textcoords='offset points', ha='right', va='bottom')
	plt.annotate(np.min(errors), xy=(1, np.min(errors)), xytext=(-20, 0), textcoords='offset points', ha='right', va='bottom')
	plt.annotate(np.max(errors), xy=(1, np.max(errors)), xytext=(-20, 0), textcoords='offset points', ha='right', va='bottom')
	
	n_params = 6
	for i in range(n_params):
		fig.add_subplot(261 + i)
		plt.title('Attribute ' + str(i+1))
		plt.scatter(errors, dataset[:, i], s=10, c=errors, cmap=plt.cm.cool)
		plt.axis('tight')

	# Trajectories Plot
	fig.add_subplot(236)
	plt.title('Trajectories')
	for i in range(20):
		traj_pos = np.array(pickle_read(folder + str(angle) + "_" + str(i) + "_trajectory.pkl"))
		pos_x = traj_pos[0, :, 0]
		pos_y = traj_pos[1, :, 0]
		plt.plot(pos_x, pos_y, color=plt.cm.cool((errors[i] - np.min(errors)) / (np.max(errors) - np.min(errors))))

		# color=cm.cool(255*(i-start)/sizemap)

	plt.show()

