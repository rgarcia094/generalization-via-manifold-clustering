import numpy as np
import matplotlib.pyplot as plt

# Constants
folder = 'RegressionSolution/'
step = 8
n_angles = 360

# Colormap definition
colormap = ['red', 'blue', 'gold', 'green', 'cyan', 'magenta', 'gray', 'orange', 'brown', 'violet', 'chartreuse', 'indianred',
			'skyblue', 'navy', 'black', 'mediumpurple']

def apply_colormap(c):
    return colormap[c % len(colormap)]
vec_apply_colormap = np.vectorize(apply_colormap)

# Create angles array
regression_angles = np.array(range(n_angles))
training_angles = np.array(range(0, n_angles, step))

# Load Files
training_data = np.genfromtxt(folder + 'Training_Data.csv', delimiter=',')[:, 1:]
training_error = np.genfromtxt(folder + 'Training_Error.csv', delimiter=',')
training_clusters = np.genfromtxt(folder + 'Training_Clusters.csv', delimiter=',').astype(int)

regression_data = np.genfromtxt(folder + 'Regression_Data.csv', delimiter=',')
regression_error = np.genfromtxt(folder + 'Regression_TrainingError.csv', delimiter=',')
regression_clusters = np.genfromtxt(folder + 'Regression_Clusters.csv', delimiter=',').astype(int)

trajectories_data = np.genfromtxt(folder + 'Trajectories_Data.csv', delimiter=',')

######### Training Data
fig = plt.figure()
fig.suptitle('Training Data', fontsize=16, fontweight='bold')
for i in range(training_data.shape[1]):
	fig.add_subplot(231 + i)
	plt.scatter(training_angles, training_data[:, i], s=5, color=vec_apply_colormap(training_clusters))
	plt.axis('tight')
	plt.title('Dimension ' + str(i+1), fontsize=12)

######### Training Error
fig = plt.figure()
fig.suptitle('Training Error', fontsize=16, fontweight='bold')
fig.add_subplot(111)
plt.bar(training_angles, training_error, color=vec_apply_colormap(training_clusters))
plt.xlabel('Angle')
plt.ylabel('Error')
plt.xlim([0, 360])
plt.ylim([0, 250])

######### Regression Data
fig = plt.figure()
fig.suptitle('Regression Data', fontsize=16, fontweight='bold')
for i in range(regression_data.shape[1]):
	fig.add_subplot(231 + i)
	plt.scatter(regression_angles, regression_data[:, i], s=1, color=vec_apply_colormap(regression_clusters))
	plt.axis('tight')
	plt.title('Dimension ' + str(i+1), fontsize=12)

######### Regression Error
fig = plt.figure()
fig.suptitle('Regression Error', fontsize=16, fontweight='bold')
fig.add_subplot(111)
plt.bar(regression_angles, regression_error, color=vec_apply_colormap(regression_clusters))
plt.xlabel('Angle')
plt.ylabel('Error')
plt.xlim([0, 360])
plt.ylim([0, 250])

######### Regression Trajectories
fig = plt.figure()
fig.suptitle('Regression Trajectories', fontsize=16, fontweight='bold')
fig.add_subplot(111)
for i in range(trajectories_data.shape[0]):
	x_data = trajectories_data[i, np.array(range(trajectories_data.shape[1])) % 2 == 0]
	y_data = trajectories_data[i, np.array(range(trajectories_data.shape[1])) % 2 == 1]
	plt.plot(x_data, y_data, color=apply_colormap(regression_clusters[i]))
	plt.xlabel('Position X')
	plt.ylabel('Position Y')

######### Regression Trajectories Endpoint
fig = plt.figure()
fig.suptitle('Regression Trajectories Endpoints', fontsize=16, fontweight='bold')
fig.add_subplot(111)
plt.scatter(trajectories_data[:, -2], trajectories_data[:, -1], s=5, color=vec_apply_colormap(regression_clusters))
plt.xlabel('Position X')
plt.ylabel('Position Y')

plt.show()