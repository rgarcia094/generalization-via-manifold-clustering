import numpy as np
import pickle
import time
import sys

from RobotSimulator import Simulator
from optparse import OptionParser

if __name__ == '__main__':
	# Start time counter
	start_time = time.time()

	# Retrieve folder and step from the command line
	usage = "usage: % prog [options] arg"
	parser = OptionParser(usage)
	parser.add_option("-s", "--step", action="store", help="training step", type="string")
	parser.add_option("-f", "--folder", action="store", help="files location", type="string")
	(options, args) = parser.parse_args()
	step = int(options.step)
	folder = options.folder + '/'

	# Number of points in the circle
	n_angles = 360

	# Dimension of each high-dimensional point
	n_params = 6

	# Error threshold to use random seed
	err_limit = 50
	err_threshold = 10

	# Training angles
	angles = np.array(range(0, n_angles, step))
	x_range = np.array(range(n_angles))

	# Array with the training dataset
	training_data = np.zeros((n_angles/step, n_params + 1))
	training_errors = np.zeros(n_angles/step)
	training_data[:, 0] = angles

	# This array will mark with 1 when a random seed was used to initialize the training
	seed_clusters = np.zeros(n_angles/step)
	seed_clusters[0] = 1

	# Apply the training algorithm for one point in each step points
	seed = None
	for i in angles:
		# params, error, _ = training_point(angle=i, file='TrainingFiles/' + str(i) + '.pkl')
		params, error, _ = Simulator().start_training(theta=i, fsave=folder + str(i) + '.pkl', fseed=seed)

		# If the error is too high, apply the training with random seed.
		if error >= err_limit:
			# r_params, r_error, _ = training_point(angle=i, file='TrainingFiles/' + str(i) + '_randseed.pkl')
			r_params, r_error, _ = Simulator().start_training(theta=i, fsave=folder + str(i) + '_randseed.pkl')

			# Gets the random seed solution only if much better than the seeded one.
			if error - r_error >= err_threshold:
				params = r_params
				error = r_error
				seed_clusters[i/step] = 1

		seed = params
		training_data[i/step, 1:] = params
		training_errors[i/step] = error

	np.savetxt(folder + 'Training_Data.csv', training_data, fmt='%.4f', delimiter=',', newline='\n')
	np.savetxt(folder + 'Training_Error.csv', training_errors, fmt='%.4f', delimiter=',', newline='\n')
	np.savetxt(folder + 'Training_NewSeed.csv', seed_clusters, fmt='%.4f', delimiter=',', newline='\n')

	# Shows execution time
	end_time = time.time()
	elapsed_time = end_time - start_time
	np.savetxt(folder + 'Execution_Time.csv', [elapsed_time], fmt='%.4f', delimiter=',', newline='\n')