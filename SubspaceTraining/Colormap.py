import numpy as np

# Colormap definition
colormap = ['red', 'blue', 'gold', 'green', 'cyan', 'magenta', 'gray', 'orange', 'brown', 'violet', 'chartreuse', 'indianred',
			'skyblue', 'navy', 'black', 'mediumpurple']

def apply_colormap(c):
    return colormap[c % len(colormap)]
vec_apply_colormap = np.vectorize(apply_colormap)