import numpy as np
import pickle
import pygame
import math
import cma
import sys

from Domain import Domain
from optparse import OptionParser
from scipy import optimize
from dmp import DMP
from Box2D import *

sys.path.insert(0, "../")

class Simulator():

	def __init__(self):
		self.basis = 3
		self.mode = 'xy'
		self.best_error = float('inf')
		self.best_params = None
		self.best_distance = float('inf')
		self.num_reach_dmps = 0
		self.num_push_dmps = 1
		self.total_dmps = self.num_push_dmps + self.num_reach_dmps
		self.goals_grid = [0]*((self.num_reach_dmps - 1 + self.num_push_dmps - 1)*2)
		self.best_goals = [0]*((self.num_reach_dmps - 1 + self.num_push_dmps - 1)*2)
		self.K = 0.0
		self.D = 0.0
		self.tau = 2.0

		self.width = 1000
		self.height = 1000

		self.FPS = 60
		self.dt = 1.0/self.FPS
		self.origin = (self.width / 2+150, (self.height / 4)*3 - 400)
		self.dmp_dt = 0.1
		self.fpsClock = None

		self.target_x = 0.0
		self.target_y = 0.0
		self.domain = Domain()

	def start_training(self, theta=1, fsave=None, fload=None, fseed=None, render=False, angle='degree', xpos=None, ypos=None, new_basis=None, reach=None, push=None):
		if new_basis is not None:
			self.basis = new_basis
		if reach is not None:
			self.num_reach_dmps = reach
		if push is not None:
			self.num_push_dmps = push
		self.total_dmps = self.num_push_dmps + self.num_reach_dmps

		if theta is not None:
			if angle == 'degree':
				theta = (theta*math.pi)/180.0
			self.target_x = 200*math.cos(theta)
			self.target_y = 200*math.sin(theta)
		else:
			self.target_x = 200.0 if xpos is None else xpos
			self.target_y = 0.0 if ypos is None else ypos

		self.K = 50.0
		self.D = 10.0

		if fload is not None and type(fload) is str:
			param_file = open(fload, "r")
			data = pickle.load(param_file)
			result = data[0]
			self.best_goals = data[1]
			self.num_push_dmps = data[2]
			self.num_reach_dmps = data[3]
			self.total_dmps = self.num_push_dmps + self.num_reach_dmps
			self.basis = data[4]
		elif fload is not None:
			result = fload
		else:
			params = None
			iterations = 2
			if fseed is not None:
				params, epsilons = self.seed_parameters(fseed)
				sigma = 20.0 if self.mode == 'xy' else 1.0
				iterations = 1
				outer_iter = 1
			else:
				sigma = 1000.0 if self.mode == 'xy' else 1.0
				params, epsilons = self.new_parameters(theta)
				outer_iter = 3

			max_vals = [1000.0]*len(self.goals_grid)
			min_vals = [0.0]*len(self.goals_grid)
			num_steps = 2

			last_index = len(self.goals_grid) - 1

			for k in range(outer_iter):
				for i in range(iterations):
					sigma0 = sigma/(i+1)

					if self.mode == 'xy':
						params = params.reshape((params.shape[0],)).tolist()
						result = cma.fmin(objective_function=self.error_func, x0=params, sigma0=sigma0, options={'maxiter': 20})
					else:
						params = params.reshape((params.shape[0],)).tolist()
						result = cma.fmin(objective_function=self.error_func, x0=params, sigma0=sigma0, options={'maxiter':10})

					epsilons[:] = epsilons[:]/10.0
					params = np.asarray(self.best_params)

			result = np.asarray(self.best_params)

			if fsave is not None:
				param_file = open(fsave, "w")
				pickle.dump([result, self.best_goals, self.num_push_dmps, self.num_reach_dmps, self.basis], param_file)
				error_filename = fsave.split(".")[0] + "_error.txt"
				error_file = open(error_filename, "w")
				error_file.write("Error: " + str(self.best_distance))
				error_file.write("\nCost: " + str(self.best_error))
				error_file.close()
				param_file.close()

		pygame.init()
		if render:
			display = pygame.display.set_mode((self.width+200, self.height))
		else:
			display = None

		self.fpsClock = pygame.time.Clock()

		world = self.domain.ResetWorld(self.origin, self.width, self.height, self.target_x, self.target_y)
		init_position = self.get_initial_positions(world)
		dmps_list = self.generate_dmps_from_parameters(result, self.basis, self.K, self.D, world, init_position)

		all_pos = self.positions_from_dmps(dmps_list)

		positions_followed, positions_ccd, thetas, joint_thetas, error = self.domain.RunSimulation(world, all_pos, display, self.height, self.target_x, self.target_y, self.dt, self.fpsClock, self.FPS, render)

		if fload is None and fsave is not None:
			pickle.dump(all_pos, open(fsave.split(".")[0] + "_waypoints.pkl", "w"))
			pickle.dump(positions_ccd, open(fsave.split(".")[0] + "_ccd_waypoints.pkl", "w"))
			pickle.dump(positions_followed, open(fsave.split(".")[0] + "_trajectory.pkl", "w"))
			pickle.dump(thetas, open(fsave.split(".")[0] + "_theta_waypoints.pkl", "w"))
			pickle.dump(joint_thetas, open(fsave.split(".")[0] + "_theta_joints.pkl", "w"))

		return result, error, positions_followed

	def new_parameters(self, theta, n_tries=20):
		if self.mode == "xy":
			# b_params = None
			# b_epsilons = None
			# b_error = None
			# for _ in range(n_tries):
			params = np.zeros(((self.basis*self.total_dmps*2), 1))
			epsilons = np.zeros(((self.basis*self.total_dmps*2)))
			epsilons[:] = 10.0
			params[:self.basis*self.total_dmps*2] = np.random.uniform(-100, 100)
			# _, c_error, c_traj = Simulator().start_training(theta=theta, fload=params, render=False)
			# if b_error == None or b_error > c_error:
			# 	b_params = params
			# 	b_epsilons = epsilons
			# 	b_error = c_error

			# params = b_params
			# epsilons = b_epsilons
		else:
			params = np.zeros(((3*self.basis*self.total_dmps+3), 1))
			epsilons = np.zeros(((3*self.basis*self.total_dmps+3)))

			for i in range(3*self.basis*self.total_dmps):
				params[i] = np.random.uniform(-100, 100)
				epsilons[i] = 10.0

			for i in range(3):
				params[3*self.basis*self.total_dmps + i] = np.random.uniform(-2.0*np.pi, 2.0*np.pi)
				epsilons[3*self.basis*self.total_dmps + i] = 0.01

		return params, epsilons

	def seed_parameters(self, fseed):
		if type(fseed) is str:
			params_file = open(fseed, "r")
			data = pickle.load(params_file)
			params = data[0]
			self.goals_grid = data[1]
			self.num_push_dmps = data[2]
			self.num_reach_dmps = data[3]
			self.total_dmps = self.num_push_dmps + self.num_reach_dmps
			self.basis = data[4]
		else:
			params = fseed

		epsilons = np.zeros((self.basis*self.total_dmps+3))
		epsilons[:] = 1.5

		return params, epsilons

	def error_func(self, x):
		if type(x) is list:
			x = np.array(x).reshape(len(x), 1)

		world = self.domain.ResetWorld(self.origin, self.width, self.height, self.target_x, self.target_y)
		step = 0

		init_position = self.get_initial_positions(world)
		dmps_list = self.generate_dmps_from_parameters(x, self.basis, self.K, self.D, world, init_position)

		all_pos = self.positions_from_dmps(dmps_list)

		trajectory_length = 0
		if self.mode == 'xy':
			initial_distance = math.sqrt((world.arm.end_effector_position()[0] - all_pos[0][-1])**2 + (world.arm.end_effector_position()[1] - all_pos[1][-1])**2)
		else:
			initial_distance = math.sqrt((world.arm.joint1.angle - all_pos[0][-1])**2 + (world.arm.joint2.angle - all_pos[1][-1])**2 + (world.arm.joint3.angle - all_pos[2][-1])**2)

		initial_error = math.sqrt((world.domain_object.target_position[0] - world.arm.end_effector_position()[0])**2 + (world.domain_object.target_position[1] - world.arm.end_effector_position()[1])**2)

		thetas_reached = True
		pd_step = 0
		obstacle_penalty, max_obstacle_penalty = 0.0, 0.0

		arm_contact_cost = 10
		pd_penalty = 0.0
		max_pd_penalty = len(all_pos[0]) * 100.0

		while step < len(all_pos[0]) or thetas_reached == False:
			penalty = 0

			prev_pos = world.arm.end_effector_position()
			prev_thetas = [world.arm.joint1.angle, world.arm.joint2.angle, world.arm.joint3.angle]

			if thetas_reached == True:
				if len(all_pos) == 2:
					theta1, theta2, theta3 = world.arm.inverse_kinematics_ccd(all_pos[0][step], all_pos[1][step])
				else:
					theta1, theta2, theta3 = all_pos[0][step], all_pos[1][step], all_pos[2][step]

				self.domain.SetJointsIteration(theta1, theta2, theta3, world)
				step += 1
				thetas_reached = False
				contact = 0
				pd_step = 0
			else:
				thetas_reached = self.domain.MoveJointsIteration(world.arm.joint1, world.arm.joint2, world.arm.joint3)

			world.arm.set_pivot_positions()
			pd_step += 1
			world.Step(self.dt, 40, 40)
			world.ClearForces()
			if pd_step == 100:
				penalty = 10000
				thetas_reached = True

			pd_penalty += 1.0
			new_pos = world.arm.end_effector_position()
			new_thetas = [world.arm.joint1.angle, world.arm.joint2.angle, world.arm.joint3.angle]

			if self.mode == 'xy':
				trajectory_length += math.sqrt((prev_pos[0] - new_pos[0])**2 + (prev_pos[1] - new_pos[1])**2)
			else:
				trajectory_length += math.sqrt((prev_thetas[0] - new_thetas[0])**2 + (prev_thetas[1] - new_thetas[1])**2 + (prev_thetas[2] - new_thetas[2])**2)

			for edge in world.arm.link3.body.contacts:
				data1 = edge.contact.fixtureA.body.userData
				data2 = edge.contact.fixtureB.body.userData

				if data1 == 'obstacle' or data2 == 'obstacle':
					obstacle_penalty += arm_contact_cost

			max_obstacle_penalty += arm_contact_cost

		end_effector_position = world.arm.end_effector_position()
		error = math.sqrt( (world.domain_object.target_position[0] - end_effector_position[0])**2 + (world.domain_object.target_position[1] - end_effector_position[1])**2)

		cost = (error/initial_error) + (obstacle_penalty/max_obstacle_penalty) + (pd_penalty/max_pd_penalty)

		if error <= self.best_distance:
			self.best_error = cost
			self.best_params = list(x)
			self.best_distance = error
			self.best_goals = list(self.goals_grid)

		return cost

	def get_initial_positions(self, world):
		if self.mode == 'xy':
			init_positions = [world.arm.pivot_position3[0] + world.arm.link3.length, world.arm.pivot_position3[1]]
		else:
			init_positions = [world.arm.joint1.angle, world.arm.joint2.angle, world.arm.joint3.angle]
		return init_positions

	def positions_from_dmps(self, dmp_list):
		all_pos = [[], [], []] if self.mode == 'joints' else [[], []]
		for i, dmp in enumerate(dmp_list):
			xpos, xdot, xddot, times = dmp.run_dmp(self.tau, self.dmp_dt, dmp.start, dmp.goal)

			for j in range(len(xpos)):
				all_pos[(i % len(all_pos))].append(xpos[j][0])

		for i in range(len(all_pos)): #Append last goal position to the corresponding list
			all_pos[i].append(dmp_list[-len(all_pos) + i].goal)

		return all_pos

	def generate_dmps_from_parameters(self, params, num_basis, Kp, Dp, world, init_position):
		dmps_list = []
		num_tracking = len(init_position)

		goals = [params[-3], params[-2], params[-1]] if num_tracking == 3 else [world.domain_object.target_position[0], world.domain_object.target_position[1]]

		for i in range(num_tracking):
			goal = goals[i]
			dmp = DMP(num_basis, Kp, Dp, init_position[i], goal)
			for j in range(num_basis):
				dmp.weights[j] = params[j + (num_basis*i)]
			dmps_list.append(dmp)
		
		return dmps_list
