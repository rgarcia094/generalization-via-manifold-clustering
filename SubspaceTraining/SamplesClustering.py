from __future__ import division, print_function

import matplotlib.pyplot as plt
import matplotlib.style as stl
import numpy as np
import pandas as pd
# import skfuzzy as fuzz
import math
import Colormap as cm
from SubspaceClustering import SubspaceClustering

# from pandas.tools.plotting import scatter_matrix, parallel_coordinates, radviz, lag_plot
from sklearn.decomposition import RandomizedPCA
from sklearn.manifold import MDS, Isomap, LocallyLinearEmbedding, SpectralEmbedding, TSNE
from sklearn.metrics import euclidean_distances
from sklearn.cluster import KMeans, DBSCAN
from optparse import OptionParser

if __name__ == '__main__':
	# Retrieve folder and step from the command line
	usage = "usage: % prog [options] arg"
	parser = OptionParser(usage)
	parser.add_option("-n", "--n_clusters", action="store", help="number of clusters", type="int")
	parser.add_option("-k", "--k", action="store", help="neighborhood limits", type="int")
	parser.add_option("-d", "--d_neighbors", action="store", help="d neighborhood or d distance", type="string")
	parser.add_option("-f", "--folder", action="store", help="files location", type="string")
	(options, args) = parser.parse_args()
	n_clusters = options.n_clusters
	k = options.k
	if options.d_neighbors == 'True':
		k_neighbors = True
	else:
		k_neighbors = False
	folder = options.folder + '/'

	# Load datasets
	dataset = np.genfromtxt(folder + 'Training_Data.csv', delimiter=',')
	training_angles = dataset[:, 0]
	training_data = dataset[:, :]
	training_error = np.genfromtxt(folder + 'Training_Error.csv', delimiter=',')

	# Apply the subspace clusterization algorithm
	# sc = SubspaceClustering(dataset=training_data.T, n_clusters=n_clusters, k=k, k_neighbors=k_neighbors)
	sc = SubspaceClustering(dataset=dataset.T, n_clusters=n_clusters, k=k, k_neighbors=k_neighbors)
	training_clusters = sc.apply_clustering().astype(float)

	labels = training_clusters / np.max(training_clusters)
	colors = plt.cm.Paired(labels)

	######### Training Data
	# stl.use('ggplot')

	# fig = plt.figure(figsize=(20,10))
	# fig.suptitle("Experiment 1 - Obstacles Free", fontsize=16, fontweight='bold')

	# for i in range(training_data.shape[1]):
	# 	fig.add_subplot(231 + i)
	# 	# plt.title('Attribute ' + str(i+1))
		
	# 	plt.scatter(training_angles, training_data[:, i], c=colors)

	# 	if i >= 3:
	# 		plt.xlabel('Angle Task', fontsize=16, fontweight='bold')
	# 	if i % 3 == 0:
	# 		plt.ylabel('Attribute Value', fontsize=16, fontweight='bold')

	# 	plt.axis('tight')
	
	fig = plt.figure()
	fig.add_subplot(111)
	plt.title('Obstacles Environment', fontsize=24, fontweight='bold')
	plt.scatter(training_angles, training_data[:, 2], c=colors)
	plt.xlabel('Angle Task', fontsize=24, fontweight='bold')
	plt.ylabel('Attribute Value', fontsize=24, fontweight='bold')
	plt.axis('tight')

	# # ######### Training Error
	# fig = plt.figure(figsize=(20,10))
	# # fig.suptitle("Experiment 1 - Performance Error", fontsize=16, fontweight='bold')
	# fig.add_subplot(111)
	# # plt.bar(training_angles, training_error, color=cm.vec_apply_colormap(training_clusters))
	# plt.bar(training_angles, training_error, color='black')
	# plt.xlabel('Angle', fontsize=16, fontweight='bold')
	# plt.ylabel('Error', fontsize=16, fontweight='bold')
	# plt.xlim([0, 360])
	# plt.ylim([0, 10])

	np.savetxt(folder + 'Training_Clusters.csv', training_clusters, fmt='%d', delimiter=',', newline='\n')

	plt.show()
