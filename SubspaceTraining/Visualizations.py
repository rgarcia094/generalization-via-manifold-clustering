import matplotlib.pyplot as plt
import Colormap as cm
import numpy as np
import math

from optparse import OptionParser

if __name__ == '__main__':
	# Retrieve folder from the command line
	usage = "usage: % prog [options] arg"
	parser = OptionParser(usage)
	parser.add_option("-r", "--regression_folder", action="store", help="regression files location", type="string")
	(options, args) = parser.parse_args()
	folder = options.regression_folder + '/'

	# Load Files
	regression_angles = np.array(range(360))
	regression_data = np.genfromtxt(folder + 'Regression_Data.csv', delimiter=',')
	regression_error = np.genfromtxt(folder + 'Regression_TrainingError.csv', delimiter=',')
	regression_clusters = np.genfromtxt(folder + 'Regression_Clusters.csv', delimiter=',').astype(int)
	trajectories_data = np.genfromtxt(folder + 'Trajectories_Data.csv', delimiter=',')

	######### Regression Data
	fig = plt.figure()
	fig.suptitle('Regression Data', fontsize=16, fontweight='bold')
	for i in range(regression_data.shape[1]):
		fig.add_subplot(231 + i)
		plt.scatter(regression_angles, regression_data[:, i], s=1, color=cm.vec_apply_colormap(regression_clusters))
		plt.axis('tight')
		plt.title('Dimension ' + str(i+1), fontsize=12)
	# fig.savefig(folder + 'Regression_Data.png')

	######### Regression Error
	fig = plt.figure()
	fig.suptitle('Regression Error', fontsize=16, fontweight='bold')
	fig.add_subplot(111)
	plt.bar(regression_angles, regression_error, color=cm.vec_apply_colormap(regression_clusters))
	plt.xlabel('Angle')
	plt.ylabel('Error')
	plt.xlim([0, 360])
	plt.ylim([0, 250])
	# fig.savefig(folder + 'Regression_Error.png')

	######### Regression Trajectories
	fig = plt.figure()
	fig.suptitle('Regression Trajectories', fontsize=16, fontweight='bold')
	fig.add_subplot(111)
	for i in range(trajectories_data.shape[0]):
		x_data = trajectories_data[i, np.array(range(trajectories_data.shape[1])) % 2 == 0]
		y_data = trajectories_data[i, np.array(range(trajectories_data.shape[1])) % 2 == 1]
		plt.plot(x_data, y_data, color=cm.apply_colormap(regression_clusters[i]))
		plt.xlabel('Position X')
		plt.ylabel('Position Y')
	# fig.savefig(folder + 'Regression_Trajectories.png')

	######### Regression Trajectories Endpoint
	circle_x = np.array([200*math.cos((theta*math.pi)/180.0) for theta in range(360)]) + 1000/2
	circle_y = 1000 - 1000/3 + np.array([200*math.sin((theta*math.pi)/180.0) for theta in range(360)])

	fig = plt.figure()
	# fig.suptitle('Regression Trajectories Endpoints', fontsize=16, fontweight='bold')
	fig.add_subplot(111)
	plt.scatter(trajectories_data[:, -2], trajectories_data[:, -1], s=5, color='lightgreen', zorder=10)
	plt.plot(circle_x, circle_y, color='red', linewidth=5, zorder=5)
	plt.xlabel('Position X', fontsize=16, fontweight='bold')
	plt.ylabel('Position Y', fontsize=16, fontweight='bold')
	# fig.savefig(folder + 'Regression_EndPoints.png')

	plt.show()