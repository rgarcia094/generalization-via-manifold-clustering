import matplotlib.pyplot as plt
import numpy as np

from optparse import OptionParser

if __name__ == '__main__':
	# Gets number of random parameters from command line
	usage = "usage: % prog [options] arg"
	parser = OptionParser(usage)
	parser.add_option("-f", "--folder", action="store", help="result files location", type="string")
	(options, args) = parser.parse_args()
	folder = options.folder + '/'

	# Load datasets
	dataset = np.genfromtxt(folder + 'Training_Data.csv', delimiter=',')
	training_error = np.genfromtxt(folder + 'Training_Error.csv', delimiter=',')
	
	training_angles = dataset[:, 0]
	training_data = dataset[:, 1:]

	######### Training Data
	fig = plt.figure()
	fig.suptitle('Training Data', fontsize=16, fontweight='bold')
	for i in range(training_data.shape[1]):
		fig.add_subplot(231 + i)
		plt.scatter(training_angles, training_data[:, i], s=5, color='red')
		plt.xlim([0, 360])
		plt.ylim([-3000, 3000])
		plt.title('Dimension ' + str(i+1), fontsize=12)

	######### Training Error
	fig = plt.figure()
	fig.suptitle('Training Error', fontsize=16, fontweight='bold')
	fig.add_subplot(111)
	plt.bar(training_angles, training_error, color='red')
	plt.xlabel('Angle')
	plt.ylabel('Error')
	plt.xlim([0, 360])
	plt.ylim([0, 250])

	plt.show()
