import matplotlib.pyplot as plt
import Colormap as cm
import numpy as np

from optparse import OptionParser
if __name__ == '__main__':
	# Retrieve folder from the command line
	usage = "usage: % prog [options] arg"
	parser = OptionParser(usage)
	parser.add_option("-f", "--folder", action="store", help="files location", type="string")
	(options, args) = parser.parse_args()
	folder = options.folder + '/'

	# Load datasets
	training_dataset = np.genfromtxt(folder + 'Training_Data.csv', delimiter=',')
	training_error = np.genfromtxt(folder + 'Training_Error.csv', delimiter=',')
	training_seed = np.genfromtxt(folder + 'Training_NewSeed.csv', delimiter=',')

	angles = training_dataset[:, 0]
	dataset = training_dataset[:, 1:]
	errors = training_error
	clusters = training_seed.astype(int)

	# Tranform seed array in clusters array
	for i in range(1, clusters.shape[0]):
		clusters[i] = clusters[i-1] + clusters[i]
	clusters = clusters - 1

	######### Training Data
	fig = plt.figure()
	fig.suptitle('Training Data', fontsize=16, fontweight='bold')
	for i in range(dataset.shape[1]):
		fig.add_subplot(231 + i)
		plt.scatter(angles, dataset[:, i], s=5, color=cm.vec_apply_colormap(clusters))
		plt.axis('tight')
		plt.title('Dimension ' + str(i+1), fontsize=12)

	######### Training Error
	fig = plt.figure()
	fig.suptitle('Training Error', fontsize=16, fontweight='bold')
	fig.add_subplot(111)
	plt.bar(angles, errors, color=cm.vec_apply_colormap(clusters))
	plt.xlabel('Angle')
	plt.ylabel('Error')
	plt.xlim([0, 360])
	plt.ylim([0, 250])

	np.savetxt(folder + 'Training_Clusters.csv', clusters, fmt='%d', delimiter=',', newline='\n')

	plt.show()