import sys
import pickle
import matplotlib.pyplot as plt
import numpy as np

from matplotlib import cm

if __name__ == '__main__':
	start = 1
	end = 361
	sizemap = end - start
	step = 1

	fig = plt.figure(figsize=(20,10))
	fig.suptitle('Trajectories - Obstacles Losangle', fontsize=16, fontweight='bold')

	for i in range(1, 7):
		folder = 'CompleteTrainings/LosangleObstacles/Test0' + str(i-1) + '/'

		fig.add_subplot(230 + i)
		plt.title("Dataset " + str(i))

		if i == 2:
			start = 0
			end = 360
	
		# for i in range(start, end, step):
		# 	name = folder + str(i) + "_waypoints.pkl"
		# 	param_file = open(name, "r")
		# 	all_pos = np.array(pickle.load(param_file))

		# 	plt.plot(np.asarray(all_pos[0]), np.asarray(all_pos[1]), color=cm.cool(255*(i-start)/sizemap))
		# 	plt.scatter(np.asarray(all_pos[0]), np.asarray(all_pos[1]), color=cm.cool(255*(i-start)/sizemap))

		# 	plt.xlabel('x', fontsize=8)
		# 	plt.ylabel('y', fontsize=8)
		# 	plt.axis('tight')

		# fig.savefig('waypoints_free.png')

		# fig = plt.figure()
		# fig.suptitle('Trajectories - Obstacles Free Training', fontsize=16, fontweight='bold')
		for i in range(start, end, step):
			name = folder + str(i) + "_trajectory.pkl"
			param_file = open(name, "r")
			all_pos = np.array(pickle.load(param_file))

			plt.plot(np.asarray(all_pos[0]), np.asarray(all_pos[1]), color=cm.cool(255*(i-start)/sizemap))

			plt.xlabel('x', fontsize=8)
			plt.ylabel('y', fontsize=8)
			plt.axis('tight')

		fig.savefig('trajectories_obstacles.png')
	
		# plt.show()