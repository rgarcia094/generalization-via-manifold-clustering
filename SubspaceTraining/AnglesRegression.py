import matplotlib.pyplot as plt
import matplotlib.style as stl
import pandas as pd
import numpy as np
import pandas as pd
import pickle

from SubspaceRegression import SubspaceRegression
from optparse import OptionParser

if __name__ == '__main__':
	# stl.use('ggplot')

	# Retrieve folder and step from the command line
	usage = "usage: % prog [options] arg"
	parser = OptionParser(usage)
	parser.add_option("-t", "--training_folder", action="store", help="training files location", type="string")
	parser.add_option("-r", "--regression_folder", action="store", help="regression files location", type="string")
	(options, args) = parser.parse_args()
	training_folder = options.training_folder + '/'
	regression_folder = options.regression_folder + '/'

	# Load datasets
	dataset = np.genfromtxt(training_folder + 'Training_Data.csv', delimiter=',')
	errors = np.genfromtxt(training_folder + 'Training_Error.csv', delimiter=',')
	clusters = np.genfromtxt(training_folder + 'Training_Clusters.csv', delimiter=',')
	# clusters = np.zeros(360)

	print dataset.shape
	print errors.shape
	print clusters.shape

	n_clusters = int(np.max(clusters) + 1)
	print n_clusters

	for s in range (1, 10):
		# sampled_angles = np.array(range(0, 359, 1))
		sampled_angles = np.array(range(0, 359, s))

		angles = dataset[sampled_angles, 0]
		data = dataset[sampled_angles, 1:]
		clusters_n = clusters[sampled_angles]
		errors_n = errors[sampled_angles]
		# angles = dataset[:, 0]
		# data = dataset[:, 1:]

		n_angles = 360
		r_angles = np.array(range(n_angles))
		step = n_angles/angles.shape[0]

		if n_clusters > 1:
			labels = clusters_n.astype(float) / np.max(clusters_n.astype(float))
		else:
			labels = clusters_n
		colors = plt.cm.Paired(labels.astype(float))

		# Show Regressions
		# fig = plt.figure(figsize=(20,10))
		fig = plt.figure()
		# fig.suptitle("Experiment 1: Regression", fontsize=16, fontweight='bold')

		# Apply regression for each subspace,
		t_mse = 0.0
		for _ in range(20):
			sr = SubspaceRegression(dataset=data, angles=angles, clusters=clusters_n, errors=errors_n, x_range=r_angles, step=step)
			regressions, mse, gp_list = sr.apply_regression()

			t_mse += np.mean(mse)/20

		print s, t_mse

	# for i in range(6):
	# 	fig.add_subplot(231 + i)
	for i in range(3, 4):
		fig.add_subplot(111)
		plt.title('MSE = ' + str(mse[0, i]), fontsize=28, fontweight='bold')
		# if i >= 3:
		plt.xlabel('Task Angle', fontsize=24, fontweight='bold')
		# if i % 3 == 0:
		plt.ylabel('Attribute Value', fontsize=24, fontweight='bold')

		# mse_list = np.zeros(n_clusters)

		# mse_list[n_clusters] = np.mean(mse);

	 	for c in range(n_clusters):
			plt.scatter(angles, data[:, i], c=colors, edgecolors='none', alpha=0.5, s=100)
				
			for j in range(regressions.shape[0]):
				min_angle = int(max(np.min(angles[clusters == j]) - step, 0))
				max_angle = int(min(np.max(angles[clusters == j]) + step, 359))

				x_test = np.arange(min_angle, max_angle, 0.1)
				x_test = x_test.reshape((x_test.shape[0], 1))
				y_pred = gp_list[j][i].predict(x_test)
				plt.plot(x_test, y_pred, c="black", linewidth=2)

	# 				label = "MSE: " + str(int(mse[j, i]))
	# 				color = colors[clusters == j, :][0]
					# plt.plot(range(min_angle, max_angle+1), regressions[j, min_angle:max_angle+1, i], color=color, label=label)

	# 			plt.legend()

	# 			# plt.xlim([0, 360])
	# 			# plt.ylim([-3100, 2500])

	# 		fig.savefig(training_folder + 'gp_smce_test0' + str(t) + '_k' + str(n_clusters) + '.png')

	# Save regression matrices
	r_file = open(regression_folder + 'Regression_Data.pkl', "w")
	pickle.dump(regressions, r_file)
	r_file.close()

	plt.show()
	# 		plt.close(fig)
	# 	np.savetxt(training_folder + 'gp_smce_mselist_test0' + str(t) + '.csv', mse_list, fmt='%d', delimiter=',', newline='\n')