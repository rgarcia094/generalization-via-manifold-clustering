import matplotlib.pyplot as plt
import Colormap as cm
import numpy as np

from sklearn.mixture import BayesianGaussianMixture as BGM
from optparse import OptionParser

if __name__ == '__main__':
	# Retrieve folder from the command line
	usage = "usage: % prog [options] arg"
	parser = OptionParser(usage)
	parser.add_option("-n", "--n_components", action="store", help="number of components", type="int")
	parser.add_option("-f", "--folder", action="store", help="files location", type="string")
	(options, args) = parser.parse_args()
	n_components = options.n_components
	folder = options.folder + '/'

	# Load datasets
	dataset = np.genfromtxt(folder + 'Training_Data.csv', delimiter=',')
	training_error = np.genfromtxt(folder + 'Training_Error.csv', delimiter=',')

	training_angles = dataset[:, 0]
	training_data = dataset[:, 1:]

	clusters = np.zeros(training_data.shape)
	for i in range(training_data.shape[1]):
		i_data = np.c_[training_angles, training_data[:, i]]
		bgm = BGM(n_components=n_components, weight_concentration_prior=1e-2)
		bgm.fit(i_data)

		clusters[:, i] = bgm.predict(i_data)
	
	bgm = BGM(n_components=n_components, weight_concentration_prior=1e-2)
	bgm.fit(training_data)

	labels = bgm.predict(training_data)

	######### Training Data
	fig = plt.figure()
	fig.suptitle('Training Data', fontsize=16, fontweight='bold')
	for i in range(training_data.shape[1]):
		fig.add_subplot(231 + i)
		plt.scatter(training_angles, training_data[:, i], s=5, color=cm.vec_apply_colormap(labels.astype(int)))
		plt.axis('tight')
		plt.title('Dimension ' + str(i+1), fontsize=12)

	######### Training Error
	fig = plt.figure()
	fig.suptitle('Training Error', fontsize=16, fontweight='bold')
	fig.add_subplot(111)
	plt.bar(training_angles, training_error, color=cm.vec_apply_colormap(labels))
	plt.xlabel('Angle')
	plt.ylabel('Error')
	plt.xlim([0, 360])
	plt.ylim([0, 250])

	# np.savetxt(folder + 'Training_Clusters.csv', labels, fmt='%d', delimiter=',', newline='\n')

	plt.show()



