import matplotlib.pyplot as plt
import Colormap as cm
import numpy as np
import pickle
import math

from optparse import OptionParser

def pickle_read(file):
	params_file = open(file, "r")
	return pickle.load(params_file)

if __name__ == '__main__':
	# Retrieve folder and step from the command line
	usage = "usage: % prog [options] arg"
	parser = OptionParser(usage)
	parser.add_option("-f", "--folder1", action="store", help="files location", type="string")
	parser.add_option("-g", "--folder2", action="store", help="files location", type="string")
	(options, args) = parser.parse_args()
	folder1 = options.folder1 + '/'
	folder2 = options.folder2 + '/'

	angles = np.array(range(360))

	error1 = np.genfromtxt(folder1 + 'Regression_TrainingError.csv', delimiter=',')
	error2 = np.genfromtxt(folder2 + 'Regression_TrainingError.csv', delimiter=',')

	average_error1 = np.zeros(360) + np.average(error1)
	average_error2 = np.zeros(360) + np.average(error2)

	stdup_error1 = average_error1 + np.std(error1)
	stddw_error1 = average_error1 - np.std(error1)
	stdup_error2 = average_error2 + np.std(error2)
	stddw_error2 = average_error2 - np.std(error2)

	print stdup_error2.shape

	fig = plt.figure()
	# fig.suptitle('Regression Trajectories Endpoints', fontsize=16, fontweight='bold')
	fig.add_subplot(211)
	plt.bar(angles, error1, color='cyan', edgecolor='none')
	plt.plot(angles, average_error1, color='black')
	plt.text(1, average_error1[0] + 0.1, 'Average = ' + str(average_error1[0]), fontsize=20)
	plt.plot(angles, stdup_error1, color='blue')
	plt.text(1, stdup_error1[0] + 0.1, 'Standard Deviation = ' + str(stdup_error1[0] - average_error1[0]), fontsize=20)
	plt.plot(angles, stddw_error1, color='blue')
	plt.xlim([0, 360])
	plt.ylim([0, max(np.max(error1), np.max(error2))])
	plt.xlabel('Task Angle', fontsize=24, fontweight='bold')
	plt.ylabel('Execution Error', fontsize=24, fontweight='bold')

	fig.add_subplot(212)
	plt.bar(angles, error2, color='cyan', edgecolor='none')
	plt.plot(angles, average_error2, color='black')
	plt.text(1, average_error2[0] + 0.1, 'Average = ' + str(average_error2[0]), fontsize=20)
	plt.plot(angles, stdup_error2, color='blue')
	plt.text(1, stdup_error2[0] + 0.1, 'Standard Deviation = ' + str(stdup_error2[0] - average_error2[0]), fontsize=20)
	plt.plot(angles, stddw_error2, color='blue')
	plt.xlim([0, 360])
	plt.ylim([0, max(np.max(error1), np.max(error2))])
	plt.xlabel('Task Angle', fontsize=24, fontweight='bold')
	plt.ylabel('Execution Error', fontsize=24, fontweight='bold')
	# fig.savefig(folder + 'Regression_EndPoints.png')

	plt.show()