import matplotlib.pyplot as plt
import Colormap as cm
import numpy as np
import numpy.linalg as npla
import numbers
import pickle
import math

from optparse import OptionParser
from sklearn.metrics import mean_squared_error

class CrKr:
    """Implementation of the CrKr algorithm.
    Implementation of the Cost Regularized Kernel Regression algorithm,
    proposed in 'Reinforcement Learning to Adjust Robot Movements 
    to New Situations', by Kober.
    :author: Rui Silva
    Example of usage:
    -----------------
    >>> S = np.array([[1, 2], [3, 4]])
    >>> C = np.array([[0.2, 0], [0, 0.5]])
    >>> D = np.array([[10, 11], [9, 8]])
    >>> crkr = CrKr(S, C, D)
    >>> new_state = np.array([[1, 1.5]])
    >>> delta_estimate = crkr.delta_estimate(new_state)
    >>> reward = ...
    >>> crkr.update_matrices(new_state, reward, delta_estimate)
    """

    def __init__(self, S, C, D, ridge_factor=0.5, sigma=1, a=1):
        """Returns an instance of CrKr.
        Parameters:
        ----------
        S : N x m np.array
          Matrix with the states of the training examples.
        C : N x N np.array
          Diagonal matrix with the costs (reward^-1) of each training example.
        D : N x n np.array
          Matrix with the deltas of the training examples.
        ridge_factor : float
          Ridge factor of the kernel regression.
        sigma : float
          Width of the gaussian kernel.
        a : float
          Normalization constant of the gaussian kernel.
        
        Returns:
        --------
        Instance of CrKr
        """
        self._check_S_matrix(S)
        self._check_C_matrix(C, S)
        self._check_D_matrix(D, S)
        self._check_ridge_factor(ridge_factor)
        self._check_sigma(sigma)
        self._check_a(a)
        
        self.S = S
        self.C = C
        self.D = D
        self.ridge_factor = ridge_factor
        self.sigma = sigma
        self.a = a
        self.K = self._compute_K()

    def delta_estimate(self, s):
        """Returns the delta estimate, for a given state.
        Parameters:
        ----------
        s : 1 x m np.array
          Array with the new observed state.
        
        Returns:
        --------
        delta_estimate : 1 x n np.array
          Delta estimate for the given state.
        Raises:
        -------
        ValueError : Exception
          When the given state is not a 2-dimensional np.array, or
          when the number of features (columns) is different than the
          S matrix.
        """
        self._check_state(s, self.S)

        k = self._compute_k(s)
        delta_mean = self._delta_mean(k, self.K)
        delta_variance = self._delta_variance(k, self.K)
        delta_variance_matrix = delta_variance * np.eye(self.D.shape[1])

        dm = np.random.multivariate_normal(delta_mean[0, :], 
                                           delta_variance_matrix)
        
        return np.array([dm])
        
    def update_matrices(self, s, reward, d):
        """Updates the kernel matrices.
        Parameters:
        ----------
        s : 1 x m np.array
          Array with the new observed state.
        reward : float
          Reward given for the execution of the delta estimate.
        d : 1 x n np.array
          Array with the estimated delta.
        Raises:
        -------
        ValueError : Exception
          - When the given state is not a 2-dimensional np.array, or
            when the number of features (columns) is different than the
            S matrix.
          - When the given delta is not a 2-dimenbsional np.array, or
            when the number of features (columns) is different than the
            D matrix.
          - When the reward is 0.
        """
        self._check_state(s, self.S)
        self._check_reward(reward)
        self._check_delta(d, self.D)
        
        self.S = np.vstack((self.S, s))
        self.D = np.vstack((self.D, d))

        C_diagonal = np.hstack((np.diagonal(self.C), 1.0 / reward))
        self.C = np.diag(C_diagonal)

        self.K = self._compute_K()

    def _gaussian_kernel(self, s1, s2):
        """Returns the gaussian kernel between two given arrays.
        Parameters:
        ----------
        s1 : a x b np.array
          The first array.
        s2 : c x b np.array
          The second array.
        Note that it supports the gaussian kernel between 1 state,
        and multiple other states at the same time.
        For that, one of the arrays should have 1 row.
        Returns:
        -------
        gaussian_kernel : 1 x min(a, c) np.array
          The gaussian kernel between the two arrays.
        """
        gk = np.exp(-self.a * np.power(npla.norm(s1 - s2, axis=1), 2) / 
                    (2 * (self.sigma ** 2)))
        return gk

    def _compute_k(self, s):
        """Returns k, the gaussian kernel between s and all training examples.
        Parameters:
        ----------
        s : 1 x m np.array
          Array with the new observed state.
        Returns:
        -------
        k : 1 x n np.array
          Array with the gaussian kernel between s and all training examples.
          Measures the distance between the observed state, and the training
          examples.
        """
        k = self._gaussian_kernel(s, self.S)
        return np.array([k]).T

    def _compute_K(self):
        """Returns K, array that contains the pairwise distance between all 
           training points
        Returns:
        -------
        K : N x N np.array
          Array with the pairwise distance between all training points.
        """
        num_trainings = self.S.shape[0]
        
        K = np.zeros((num_trainings, num_trainings))
        for i in range(0, num_trainings):
            for j in range(i, num_trainings):
                s1 = np.array([self.S[i, :]])
                s2 = np.array([self.S[j, :]])
                K[i, j] = K[j, i] = self._gaussian_kernel(s1, s2)
                
        return K

    def _delta_mean(self, k, K):
        """Returns the delta mean.
        Parameters:
        ----------
        k : 1 x n np.array
          Array with the gaussian kernel between s and all training examples.
          Measures the distance between the observed state, and the training
          examples.
        K : N x N np.array
          Array with the pairwise distance between all training points.
        Returns:
        --------
        delta_mean : 1 x n np.array
          The estimated delta mean.
        """
        dm = np.dot(k.T, 
                    np.dot(npla.inv(K + self.ridge_factor * self.C), self.D))
        return dm

    def _delta_variance(self, k, K):
        """Returns the delta variance.
        Parameters:
        ----------
        k : 1 x n np.array
          Array with the gaussian kernel between s and all training examples.
          Measures the distance between the observed state, and the training
          examples.
        K : N x N np.array
          Array with the pairwise distance between all training points.
        Returns:
        -------
        delta_variance : 1 x 1 np.array
          The estimate delta_variance
        
        """
        dv = (self.a + 
              self.ridge_factor - 
              np.dot(k.T, np.dot(npla.inv(K + self.ridge_factor * self.C), k)))
        return dv

    def _check_S_matrix(self, S):
        """Verifies if the S matrix respects the necessary constraints.
        Parameters:
        ----------
        S : N x m np.array
          S matrix to test.
        Raises:
        ------
        ValueError : Exception
          When the S matrix is not a 2-dimensional np.array.
        """
        if S.ndim != 2:
            raise ValueError('S must be a 2-dimensional matrix')

    def _check_C_matrix(self, C, S):
        """Verifies if the C matrix respects the necessary constraints.
        Parameters:
        ----------
        C : N x N np.array
          C matrix to test.
        Raises:
        -------
        ValueError : Exception
          When the C matrix:
            - Is not a 2-dimensional np.array
            - Does not have the same number of lines as S.
            - Is not a square matrix.
        """
        if C.ndim != 2:
            raise ValueError('C must be a 2-dimensional matrix.')
        if C.shape[0] != S.shape[0]:
            raise ValueError('C must have the same number of lines as S.')
        if C.shape[0] != C.shape[1]:
            raise ValueError('C must be a square matrix.')

    def _check_D_matrix(self, D, S):
        """Verifies if the D matrix respects the necessary constraints.
        Parameters:
        ----------
        D : N x m np.array
          D matrix to test.
        Raises:
        ValueError : Exception
          When the D matrix:
            - Is not a 2-dimensional np.array.
            - Does not have the same number of lines as S.
        """
        if S.ndim != 2:
            raise ValueError('D must be a 2-dimensional matrix.')
        if D.shape[0] != S.shape[0]:
            raise ValueError('D must have the same number of lines as S.')

    def _check_ridge_factor(self, ridge_factor):
        """Verifies if the ridge factor respects the necessary constraints.
        Parameters:
        ----------
        ridge_factor : number
          The ridge factor to test.
        Raises:
        ------
        ValueError : Exception
          When the ridge factor is not a number.
        """
        if not isinstance(ridge_factor, numbers.Number):
            raise ValueError('Ridge Factor must be a numberic value.')
        
    def _check_sigma(self, sigma):
        """Verifies if sigma respects the necessary constraints.
        Parameters:
        ----------
        sigma : number
          The sigma to test.
        Raises:
        ------
        ValueError : Exception
          When sigma is not a number.
        """
        if not isinstance(sigma, numbers.Number):
            raise ValueError('Sigma must be a numberic value.')

    def _check_a(self, a):
        """Verifies if a respects the necessary constraints.
        Parameters:
        ----------
        a : number
          a parameter to test.
        Raises:
        ------
        ValueError : Exception
          When a is not a number.
        """
        if not isinstance(a, numbers.Number):
            raise ValueError('a must be a numberic value.')

    def _check_state(self, s, S):
        """Verifies is the new observed state respects necessary constraints.
        Parameters:
        ----------
        s : np.array
          State to test
        Raises:
        -------
        ValueError : Exception
          When the state:
            - Is not a 2-dimensional np.array
            - Does not have the same number of features (columns) as S
        """
        if s.ndim != 2 or s.shape[1] != S.shape[1]:
            exception_msg = 'State should be a 2-dimensional numpy array ' + \
                            'with the same number of columns as S: ' + \
                            str(S.shape[1])
            raise ValueError(exception_msg)

    def _check_reward(self, reward):
        """Verifies if the observed reward respects the necessary constraints.
        Parameters:
        ----------
        r : float
          Reward to test.
        Raises:
        ValueError : Exception
          When the reward is equal to 0.
        """
        if reward == 0.0:
            raise ValueError('Reward must be different than 0.')

    def _check_delta(self, d, D):
        """Verifies is the delta respects necessary constraints.
        Parameters:
        ----------
        d : np.array
          Delta to test
        Raises:
        -------
        ValueError : Exception
          When the delta:
            - Is not a 2-dimensional np.array
            - Does not have the same number of features (columns) as D
        """
        if d.ndim != 2 or d.shape[1] != D.shape[1]:
            exception_msg = 'Delta should be a 2-dimensional numpy array with ' + \
                            'the same number of columns as D: ' + \
                            str(D.shape[1])
            raise ValueError(exception_msg)

if __name__ == '__main__':
    # Retrieve folder and step from the command line
    usage = "usage: % prog [options] arg"
    parser = OptionParser(usage)
    parser.add_option("-f", "--folder", action="store", help="files location", type="string")
    (options, args) = parser.parse_args()
    folder = str(options.folder) + '/'

    # Load datasets
    dataset = np.genfromtxt(folder + 'Training_Data.csv', delimiter=',')

    steps = [1, 2, 3, 4, 5, 6, 7, 8, 9]
    samples_sizes = (360 / np.array(steps)).astype(int)
    mse = np.zeros(len(steps))

    # j = 0
    # for step in steps:
    #     training_tasks = np.array(range(0, 360, step))

    #     S = np.atleast_2d(dataset[training_tasks, 0]).T
    #     C = np.eye(training_tasks.shape[0]) # / training_tasks.shape[0]
    #     D = np.atleast_2d(dataset[training_tasks, 1:])
    #     ridge_factor = 0.5
    #     sigma = 1
    #     a = 0.001

    #     for _ in range(20):
    #         crkr = CrKr(S, C, D, ridge_factor, sigma, a)

    #         y_pred = np.zeros((360, 6))
    #         for i in range(360):
    #             t = np.atleast_2d([i])
    #             y_pred[i, :] = crkr.delta_estimate(t)

    #         mse[j] += mean_squared_error(dataset[:, 1:], y_pred)/20
    #     j += 1

    # print samples_sizes
    # print mse

    """ Obstacles Free Environment
    [360 180 120  90  72  60  45  40  36  30  24  20]
    [ 2068.04264728  2263.27479946  2469.20353753  2620.56571516  2829.17948709
      3085.0465858   3493.21214996  3977.95840571  4259.70234741  5067.50462612
      5955.78234334  7202.02159539]

        Obstacles Environment
        [360 180 120  90  72  60  45  40  36  30  24  20]
    [ 12591.00146286  13602.91595979  14437.35013624  15175.7925365
      15897.51617974  16903.32169536  17831.70621703  19230.04404063
      20064.33091395  21106.12148499  23289.99372684  27722.51520364]
    """
    ###################################
    """ Obstacles Free Environment
    [360 180 120  90  72  60  45  40  36  30  24  20]
    [ 1534.84129287  1613.91406713  1678.82910266  1723.26757562  1790.92182838
      1880.79442584  2011.83098512  2121.95767309  2233.60702685  2628.97360147
      2807.47293216  3212.39408351]

    [360 180 120  90  72  60  45  40  36  30  24  20]
    [ 2820.61295966  2940.49994635  3023.04449729  3095.42799537  3236.02907108
      3430.46482162  3807.15568544  3886.83049421  3733.72039937  4230.36917715
      5052.59338889  6473.45719894]
    """

    # samples = [360, 180, 120, 90, 72, 60, 45, 40, 36, 30, 24, 20]
    ours_perf = [1216.40315352, 1581.02246446, 1885.00931776, 2258.96346242, 2518.00129295, 3019.32109084, 3159.92448364, 4179.69283149, 3357.41054511]
    crkr_perf = [2068.04264728, 2263.27479946, 2469.20353753, 2620.56571516, 2829.17948709, 3085.0465858, 3154.33247655, 3493.21214996, 3977.95840571]
    regr_perf = [3024.56050049, 3713.04936325, 4175.67390179, 4726.33849656, 5167.96926429, 5462.3390195, 6105.16238964, 6707.32456161, 6176.05535151]

    # ours_perf = [1918.38525768, 2466.84033076, 2928.7284666, 3474.72466934, 3495.40202235, 4353.20544082, 4372.54252948, 5440.33193243, 5444.29593369]
    # crkr_perf = [12591.00146286, 13602.91595979, 14437.35013624, 15175.7925365, 15897.51617974, 16903.32169536, 17789.98304242, 17831.70621703, 19230.04404063]
    # regr_perf = [20087.4418718, 25405.8785567, 29502.2882623, 33094.5837725, 37783.8427369, 38861.8813247, 42268.785806, 45079.2356454, 50157.1379643]

    fig = plt.figure()
    fig.add_subplot(111)
    plt.title('Free Obstacles Environment', fontsize=24, fontweight='bold')
    plt.plot(samples_sizes, ours_perf, c='green', label="Our Method")
    plt.plot(samples_sizes, crkr_perf, c='darkblue', label="CrKr Method", linestyle='--')
    plt.plot(samples_sizes, regr_perf, c='red', label="Single Regression", linestyle=':')
    plt.legend(bbox_to_anchor=(0.56, 1), loc=2, borderaxespad=0., prop={'size': 20})
    plt.xlabel('Sampling Size', fontsize=24, fontweight='bold')
    plt.ylabel('Minimum Square Error', fontsize=24, fontweight='bold')
    plt.ylim([0, 50157.1379643])
    plt.xlim([20, 360])
    # plt.axis('tight')

    plt.show()
    


