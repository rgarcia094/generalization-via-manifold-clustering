import numpy as np
import pickle
import time
import sys

from RobotSimulator import Simulator
from optparse import OptionParser

if __name__ == '__main__':
	# Retrieve folder and step from the command line
	usage = "usage: % prog [options] arg"
	parser = OptionParser(usage)
	parser.add_option("-n", "--n_trainings", action="store", help="Number of trainings", type="int")
	parser.add_option("-a", "--angle", action="store", help="Angle to be trained", type="int")
	parser.add_option("-f", "--folder", action="store", help="files location", type="string")
	(options, args) = parser.parse_args()
	n_trainings = options.n_trainings
	angle = options.angle
	folder = options.folder + '/'

	# Number of parameters
	n_params = 6

	# Create solution matrices
	r_params = np.zeros((n_trainings, n_params))
	r_errors = np.zeros(n_trainings)
	r_trajs = None

	# N_Training iterations
	for i in range(n_trainings):
		# # Creates a random parameter and calculates the training error and trajectories.
		# r_params[i, :] = np.random.uniform(-3000, 3000, size=n_params)

		# # Region with the best local minima for the angle 120
		# r_params[i, :] = [np.random.uniform(-1000, -500), np.random.uniform(-500, 0), np.random.uniform(0, 500),
		# 				  np.random.uniform(-750, -250), np.random.uniform(-500, 0), np.random.uniform(-250, 250)]

		# _, r_errors[i], c_traj = Simulator().start_training(theta=angle, fload=r_params[i, :], render=False)

		# Apply the Training Algorithm with random seed
		r_params[i, :], r_errors[i], c_traj = Simulator().start_training(theta=angle, fsave=folder+str(angle)+'_'+str(i)+'.pkl')

		# Update the trajectories matrix
		c_traj = np.array(c_traj)[:, :, 0]
		if r_trajs is None:
			length = 2*c_traj.shape[1]
			r_trajs = np.zeros(length)
			r_trajs[np.array(range(length)) % 2 == 0] = c_traj[0, :]
			r_trajs[np.array(range(length)) % 2 == 1] = c_traj[1, :]
			r_trajs = r_trajs.reshape(1, r_trajs.shape[0])
		else:
			crt_length = 2*c_traj.shape[1]
			new_trajec = np.zeros(crt_length)
			new_trajec[np.array(range(crt_length)) % 2 == 0] = c_traj[0, :]
			new_trajec[np.array(range(crt_length)) % 2 == 1] = c_traj[1, :]
			new_trajec = new_trajec.reshape(1, new_trajec.shape[0])

			old_length = r_trajs.shape[1]
			if crt_length > old_length:
				last_value = r_trajs[:, -2:]
				new_block = np.tile(last_value, (crt_length - old_length)/2)
				r_trajs = np.c_[r_trajs, new_block]
			elif crt_length < old_length:
				last_value = new_trajec[:, -2:]
				new_block = np.tile(last_value, (old_length - crt_length)/2)
				new_trajec = np.c_[new_trajec, new_block]

			r_trajs = np.r_[r_trajs, new_trajec]

	# Save Matrices
	np.savetxt(folder + 'Parameters.csv', r_params, fmt='%.4f', delimiter=',', newline='\n')
	np.savetxt(folder + 'Errors.csv', r_errors, fmt='%.4f', delimiter=',', newline='\n')
	np.savetxt(folder + 'Trajectories.csv', r_trajs, fmt='%.4f', delimiter=',', newline='\n')
