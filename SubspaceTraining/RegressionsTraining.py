import numpy as np
import pickle

from RobotSimulator import Simulator
from optparse import OptionParser

def pickle_read(file):
	params_file = open(file, "r")
	return pickle.load(params_file)

# Dummy save solution function
def save_solution(angle, params, file):
	param_file = open(file + str(angle) + '.pkl', "w")
	pickle.dump([params, [], 0, 0, 0], param_file)
	param_file.close()

if __name__ == '__main__':
	# Retrieve folder and step from the command line
	usage = "usage: % prog [options] arg"
	parser = OptionParser(usage)
	parser.add_option("-t", "--training_folder", action="store", help="training files location", type="string")
	parser.add_option("-r", "--regression_folder", action="store", help="regression files location", type="string")
	(options, args) = parser.parse_args()
	training_folder = options.training_folder + '/'
	regression_folder = options.regression_folder + '/'

	# Load datasets
	dataset = np.genfromtxt(training_folder + 'Training_Data.csv', delimiter=',')
	t_error = np.genfromtxt(training_folder + 'Training_Error.csv', delimiter=',')
	t_clusters = np.genfromtxt(training_folder + 'Training_Clusters.csv', delimiter=',').astype(int)
	t_angles = dataset[:, 0]
	t_data = dataset[:, 1:]
	regressions = pickle_read(regression_folder + 'Regression_Data.pkl')

	n_angles = regressions.shape[1]
	n_params = regressions.shape[2]
	step = n_angles/t_angles.shape[0]

	# Choose the best solution for each angle.
	r_solutions = np.zeros((n_angles, n_params))
	r_sol_errors = -np.ones(n_angles)
	r_sol_clusters = np.zeros(n_angles)
	r_sol_trajec = None

	# Loop over all possible angles
	for i in range(regressions.shape[1]):
		b_param = None
		b_error = None
		b_traj = None
		for j in range(regressions.shape[0]):
			min_angle = max(np.min(t_angles[t_clusters == j]) - step, 0)
			max_angle = min(np.max(t_angles[t_clusters == j]) + step, 359)

			if i >= min_angle and i <= max_angle:
				c_param = regressions[j, i, :]
				_, c_error, c_traj = Simulator().start_training(theta=i, fload=c_param, render=False)

				if b_error == None or b_error > c_error:
					b_error = c_error
					b_param = c_param
					b_traj = c_traj
					r_sol_clusters[i] = j

		b_traj = np.array(b_traj)[:, :, 0]

		# # If i is one of the training data, check if the original solution is better.
		# if i % step == 0 and t_error[i/step] < b_error:
		# 	b_error = t_error[i/step]
		# 	b_param = t_data[i/step, :]
		# 	r_sol_clusters[i] = t_clusters[i/step]

		# Save the best solution to file.
		# save_solution(i, b_param, regression_folder)

		# Update the trajectories matrix
		r_solutions[i, :] = b_param
		r_sol_errors[i] = b_error
		if r_sol_trajec is None:
			length = 2*b_traj.shape[1]
			r_sol_trajec = np.zeros(length)
			r_sol_trajec[np.array(range(length)) % 2 == 0] = b_traj[0, :]
			r_sol_trajec[np.array(range(length)) % 2 == 1] = b_traj[1, :]
			r_sol_trajec = r_sol_trajec.reshape(1, r_sol_trajec.shape[0])
		else:
			crt_length = 2*b_traj.shape[1]
			new_trajec = np.zeros(crt_length)
			new_trajec[np.array(range(crt_length)) % 2 == 0] = b_traj[0, :]
			new_trajec[np.array(range(crt_length)) % 2 == 1] = b_traj[1, :]
			new_trajec = new_trajec.reshape(1, new_trajec.shape[0])

			old_length = r_sol_trajec.shape[1]
			if crt_length > old_length:
				last_value = r_sol_trajec[:, -2:]
				new_block = np.tile(last_value, (crt_length - old_length)/2)
				r_sol_trajec = np.c_[r_sol_trajec, new_block]
			elif crt_length < old_length:
				last_value = new_trajec[:, -2:]
				new_block = np.tile(last_value, (old_length - crt_length)/2)
				new_trajec = np.c_[new_trajec, new_block]

			r_sol_trajec = np.r_[r_sol_trajec, new_trajec]

	np.savetxt(regression_folder + 'Regression_Data.csv', r_solutions, fmt='%.4f', delimiter=',', newline='\n')
	np.savetxt(regression_folder + 'Regression_TrainingError.csv', r_sol_errors, fmt='%.4f', delimiter=',', newline='\n')
	np.savetxt(regression_folder + 'Trajectories_Data.csv', r_sol_trajec, fmt='%.4f', delimiter=',', newline='\n')
	np.savetxt(regression_folder + 'Regression_Clusters.csv', r_sol_clusters, fmt='%.4f', delimiter=',', newline='\n')


	# # Loop over each subspace regression
	# for j in range(regressions.shape[0]):
	# 	# Calculate the domain of each subspace
	# 	min_angle = max(np.min(t_angles[t_clusters == j]) - step, 0)
	# 	max_angle = min(np.max(t_angles[t_clusters == j]) + step, 359)

	# 	# Iterate over each angle of the domain
	# 	for i in range(min_angle, max_angle+1):
	# 		c_param = regressions[j, i, :]
	# 		_, c_error, c_traj = Simulator().start_training(theta=i, fload=c_param, render=False)

	# 		# If new parameters are better, change it.
	# 		if r_sol_errors[i] == -1 or r_sol_errors[i] > c_error:
	# 			r_solutions[i, :] = c_param
	# 			r_sol_errors[i] = c_error
	# 			r_sol_clusters = j

	# 			# TODO: Update the trajectories matrix

	# for i in range(0, 360, step):
	# 	if t_error[i/step] < r_sol_errors[i]:
	# 		r_solutions[i, :] = t_data[i/step, :]
	# 		r_sol_errors[i] = t_error[i/step]
	# 		r_sol_clusters[i] = t_clusters[i/step]

	# 		# TODO: Update the trajectories matrix