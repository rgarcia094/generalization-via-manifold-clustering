import numpy as np
import pickle

from RobotSimulator import Simulator
from optparse import OptionParser

# Dummy save solution function
def save_solution(angle, params, file):
	param_file = open(file + str(angle) + '.pkl', "w")
	pickle.dump([params, [], 0, 0, 0], param_file)
	param_file.close()

if __name__ == '__main__':
	# Gets number of random parameters from command line
	usage = "usage: % prog [options] arg"
	parser = OptionParser(usage)
	parser.add_option("-f", "--folder", action="store", help="result files location", type="string")
	parser.add_option("-t", "--tests", action="store", help="number of random parameters to tests", type="int")
	parser.add_option("-l", "--low_limit", action="store", help="minimum value of the domain", type="float")
	parser.add_option("-m", "--high_limit", action="store", help="maximum value of the domain", type="float")
	parser.add_option("-s", "--step_angles", action="store", help="step at each angle to test", type="int")
	(options, args) = parser.parse_args()
	folder = options.folder + '/'
	n_tests = options.tests
	low_limit = options.low_limit
	high_limit = options.high_limit
	step = options.step_angles

	n_angles = 360
	n_params = 6

	r_dataset = np.zeros((n_angles/step, n_params + 1))
	r_error = -np.ones(n_angles/step)

	r_dataset[:, 0] = range(0, n_angles, step);

	for j in range(n_tests):
		print "Testing sample", j
		params = (high_limit - low_limit) * np.random.random_sample(n_params) + low_limit
		for i in r_dataset[:, 0]:
			_, error, _ = Simulator().start_training(theta=i, fload=params, render=False)
			if r_error[int(i)/step] < 0.0 or r_error[int(i)/step] > error:
				r_dataset[int(i)/step, 1:] = params
				r_error[int(i)/step] = error

	for i in r_dataset[:, 0]:
		save_solution(i, r_dataset[int(i)/step, 1:], folder)

	np.savetxt(folder + 'Training_Data.csv', r_dataset, fmt='%.4f', delimiter=',', newline='\n')
	np.savetxt(folder + 'Training_Error.csv', r_error, fmt='%.4f', delimiter=',', newline='\n')