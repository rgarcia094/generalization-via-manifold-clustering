import matplotlib.pyplot as plt
import Colormap as cm
import numpy as np
import pickle
import math

from optparse import OptionParser

def pickle_read(file):
	params_file = open(file, "r")
	return pickle.load(params_file)

if __name__ == '__main__':
	# Retrieve folder and step from the command line
	usage = "usage: % prog [options] arg"
	parser = OptionParser(usage)
	parser.add_option("-f", "--folder", action="store", help="files location", type="string")
	(options, args) = parser.parse_args()
	folder = options.folder + '/'

	endpoints = np.zeros((360, 2))

	for i in range(360):
		print i
		trajectory = pickle_read(folder + str(i) + "_trajectory.pkl")
		# print endpoints[i, :].shape
		# print trajectory[0][-1][0]
		endpoints[i, 0] = trajectory[0][-1][0]
		endpoints[i, 1] = trajectory[1][-1][0]
		print endpoints[i, :]

	######### Regression Trajectories Endpoint
	circle_x = np.array([200*math.cos((theta*math.pi)/180.0) for theta in range(360)]) + 1000/2
	circle_y = 1000 - 1000/3 + np.array([200*math.sin((theta*math.pi)/180.0) for theta in range(360)])

	fig = plt.figure()
	# fig.suptitle('Regression Trajectories Endpoints', fontsize=16, fontweight='bold')
	fig.add_subplot(111)
	plt.plot(circle_x, circle_y, color='red', linewidth=5, zorder=5)
	plt.scatter(endpoints[:, 0], endpoints[:, 1], s=5, color='lightgreen', zorder=10)
	plt.xlabel('Position X', fontsize=16, fontweight='bold')
	plt.ylabel('Position Y', fontsize=16, fontweight='bold')
	# fig.savefig(folder + 'Regression_EndPoints.png')

	plt.show()