import pickle
import math
import os
import numpy as np

l = os.listdir("./")
pkls = []

for f in l:
	fi = f.split(".")
	if fi[-1] == "pkl" and len(fi[0]) < 4: #make sure we are getting the files we want
		pkls.append(f)
		print f

pkls.sort()
train = open("train.txt", "w")

for i in pkls:
	val = i.split(".")[0]
	theta = (float(val) * math.pi) / 180.0
	x = 200 * math.cos(theta)
	y = 200 * math.sin( theta)
	print "T: ", theta, " X: ", x, " Y: ", y
	f = open(i, "r")
	params = pickle.load(f)
	output = ""
	total = len(params) if isinstance(params, list) else params.shape[0]
	for j in range(total):
		if isinstance(params[j], list) or isinstance(params[j], np.ndarray):
			t = len(params[j]) if isinstance(params[j], list) else params[j].shape[0]
			for k in range(t):
				if k == t-1:
					output += str(params[j][k])
				else:
					output += str(params[j][k]) + ", "
		# elif j == total-1:
		# 	output += str(params[j])
		# else:
		# 	output += str(params[j]) + ", "
	# train.write(val[0]+"."+val[1]+"; "+output+"\n")
	#train.write(str(x)+", "+str(y)+"; "+output+"\n")
	train.write(str(theta)+", "+str(x)+", "+str(y)+"; "+output+"\n")

