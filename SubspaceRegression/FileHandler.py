'''
Created on 03/09/2016

@author: Rafael
'''
import numpy as np
import csv

def get_robot_data(filename):
    with open(filename) as csvfile:
        spamreader = csv.reader(csvfile, delimiter=",")
        
        i = 0         
        for row in spamreader:
            if i == 0:
                data = np.array(row).astype(np.float)
            elif row != []:
                data = np.vstack((data, np.array(row).astype(np.float)))
            i += 1
        
        # data[:, 0] = np.radians(data[:, 0])
        
        # neg_pos = data[:, 0] < 0
        # data[neg_pos, 0] = 2*np.pi + data[neg_pos, 0]
        
        # order = np.argsort(data[:, 0])
        # data = data[order, :]
        
        return data
                