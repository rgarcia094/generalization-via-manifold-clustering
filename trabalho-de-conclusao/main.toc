\select@language {brazilian}
\select@language {english}
\select@language {brazilian}
\contentsline {chapter}{1~Introdu\IeC {\c c}\IeC {\~a}o}{9}{CHAPTER.1}
\contentsline {chapter}{2~O Problema de Movimenta\IeC {\c c}\IeC {\~a}o de Bra\IeC {\c c}os Rob\IeC {\'o}ticos}{11}{CHAPTER.2}
\contentsline {chapter}{3~Visualiza\IeC {\c c}\IeC {\~a}o e An\IeC {\'a}lise dos Dados de Treinamento}{14}{CHAPTER.3}
\contentsline {section}{\numberline {3.1}Conjuntos de Treinamento de Pol\IeC {\'\i }ticas de Alta-Dimensionalidade}{14}{section.3.1}
\contentsline {section}{\numberline {3.2}Mensurando Performance de Pol\IeC {\'\i }ticas}{18}{section.3.2}
\contentsline {section}{\numberline {3.3}Visualiza\IeC {\c c}\IeC {\~a}o de Trajet\IeC {\'o}rias Codificadas por Pol\IeC {\'\i }ticas}{24}{section.3.3}
\contentsline {section}{\numberline {3.4}Redu\IeC {\c c}\IeC {\~a}o de Dimensionalidade de Pol\IeC {\'\i }ticas}{25}{section.3.4}
\contentsline {subsection}{\numberline {3.4.1}PCA}{27}{subsection.3.4.1}
\contentsline {subsection}{\numberline {3.4.2}ISOMAP}{28}{subsection.3.4.2}
\contentsline {subsection}{\numberline {3.4.3}LLE}{31}{subsection.3.4.3}
\contentsline {subsection}{\numberline {3.4.4}t-SNE}{33}{subsection.3.4.4}
\contentsline {section}{\numberline {3.5}Propriedades dos Manifolds de Solu\IeC {\c c}\IeC {\~o}es do Bra\IeC {\c c}o Rob\IeC {\'o}tico}{34}{section.3.5}
\contentsline {chapter}{4~Um M\IeC {\'e}todo para Generaliza\IeC {\c c}\IeC {\~a}o de Comportamentos}{36}{CHAPTER.4}
\contentsline {section}{\numberline {4.1}Identifica\IeC {\c c}\IeC {\~a}o e Clusteriza\IeC {\c c}\IeC {\~a}o de Manifolds N\IeC {\~a}o-Lineares}{38}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}O M\IeC {\'e}todo SMCE}{39}{subsection.4.1.1}
\contentsline {section}{\numberline {4.2}Regress\IeC {\~a}o em Manifolds N\IeC {\~a}o-Lineares}{44}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}\textit {Overfitting} e Underfitting}{45}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}Regress\IeC {\~a}o do Processo Gaussiano}{46}{subsection.4.2.2}
\contentsline {section}{\numberline {4.3}Amostragem das Pol\IeC {\'\i }ticas usadas no Treinamento da Regress\IeC {\~a}o}{51}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}An\IeC {\'a}lise da Vari\IeC {\^a}ncia da Regress\IeC {\~a}o}{52}{subsection.4.3.1}
\contentsline {subsection}{\numberline {4.3.2}Amostragem das Pol\IeC {\'\i }ticas Treinadas pela Otimiza\IeC {\c c}\IeC {\~a}o da DMP}{54}{subsection.4.3.2}
\contentsline {section}{\numberline {4.4}Novas Configura\IeC {\c c}\IeC {\~o}es de Obst\IeC {\'a}culos}{55}{section.4.4}
\contentsline {chapter}{5~Conclus\IeC {\~a}o}{61}{CHAPTER.5}
\contentsline {chapter}{\xspace {}Refer{\^e}ncias}{63}{schapter.6}
