import sys

pointsList = []
for line in sys.stdin:
	line = line.replace(",", "")
	line = line.replace(";", "")
	attrList = line.split()
	attrList = [float(attrList[i]) for i in range(len(attrList))]
	pointsList.append(attrList)
print pointsList