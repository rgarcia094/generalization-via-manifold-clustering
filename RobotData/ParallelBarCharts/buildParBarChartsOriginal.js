data = data_original;
var mean = [d3.mean(data, function(d) { return d[0]; }), d3.mean(data, function(d) { return d[1]; })];
data.forEach(function (d, i) {
    var dx = d[0] - mean[0], dy = d[1] - mean[1];
    var angle = Math.atan2(dy, dx)*180/Math.PI;
    d.unshift(angle);
});
data.sort(function (a, b) { return a[0] - b[0]; });

var transposed = d3.transpose(data)
var dimensions = transposed.length;
var width = 1300, height = 620;
var barHeight = (height-60)/data.length;
var offset = 30;
var barWidth = width/dimensions - offset;

var svg = d3.select("body").append("svg")
    .attr("width", width)
    .attr("height", height);

svg.selectAll("text")
    .data(transposed)
  .enter().append("text")
    .attr("x", function(_, i) { return i*(barWidth + offset) + barWidth/2; })
    .attr("y", height-15)
    .style("font-family", "sans-serif")
    .style("text-anchor", "middle")
    .style("font-size", "14px")
    .text(function(_, i) { return attrName(i); })
    .on("click", function (_, i) { updateBars(i); });

var widthScale = [];
for (var dim = 0; dim < dimensions; dim++) {
    var maxData = d3.max(data, function(array) { return array[dim]; });
    var minData = d3.min(data, function(array) { return array[dim]; });

    var wScale = d3.scale.linear()
        .domain([minData, maxData])
        .range([0, barWidth]);

    widthScale.push(wScale);
}

var charts = [];
for (var dim = 0; dim < dimensions; dim++) {
    var g = svg.append("g")
        .attr("id", "chart" + dim);
    charts.push(g);
}

for (var dim = 0; dim < dimensions; dim++) {
    charts[dim].selectAll("rect")
        .data(data)
      .enter().append("rect")
        .attr("x", dim*(barWidth + offset))
        .attr("y", function(_, i) { return barHeight*i; })
        .attr("width", function(d) { return widthScale[dim](d[dim]); })
        .attr("height", barHeight)
        .style("fill", "blue");

        var dAxis = d3.svg.axis()
            .scale(widthScale[dim])
            .orient("bottom")
            .ticks(4);

        charts[dim].append("g")
            .attr("class", "axis")
            .attr("transform", "translate(" + (dim*(barWidth + offset)) + " , " + (barHeight*data.length+1) + ")")
            .call(dAxis);
}

var rollScale = d3.scale.quantize()
    .domain([0, height-60])
    .range(d3.range(0, height-60, barHeight));

var dragger = d3.behavior.drag()
    .on("drag", function() { onDraggingBehavior(d3.mouse(this)[1]); })
    .on("dragend", onDraggingEnd);

var hightLight = svg.append("g")
    .style("cursor", "move")
    .call(dragger);

var hightLightBar = hightLight.append("rect")
    .attr("x", 0)
    .attr("y", 0)
    .attr("width", dimensions*(barWidth+offset))
    .attr("height", barHeight)
    .style("fill", "black");

var hightLightLabels = hightLight.selectAll("text")
    .data(transposed)
  .enter().append("text")
    .attr("x", function(_, i) { return (barWidth + offset)*(i+1) - offset/2; })
    .attr("y", 14)
    .style("font-family", "sans-serif")
    .style("text-anchor", "end")
    .style("font-size", "14px")
    .text(function(d) { return Math.round(d[0]*100)/100; })

function updateBars(key) {
    data.sort(function (a, b) { return a[key] - b[key]; });
    transposed = d3.transpose(data);

    for (var dim = 0; dim < dimensions; dim++) {
        charts[dim].selectAll("rect")
            .data(data, function(d) { return d; })
          .transition()
            .duration(1000)
            .attr("y", function(_, i) { return barHeight*i; });
    }

    hightLightBar.transition()
        .duration(1000)
        .attr("y", 0);
    
    hightLightLabels.data(transposed).transition()
        .duration(1000)
        .attr("y", 14)
        .text(function(d) { return Math.round(d[0]*100)/100; });
}

function attrName(index) {
    switch (index) {
        case 0: return "Angle";
        case 1: return "Pos X";
        case 2: return "Pos Y";
        default: return "Attribute " + (index - 2);
    }
}

function onDraggingBehavior(mouseY) { 
    hightLightBar.attr("y", mouseY);

    hightLightLabels
        .attr("y", mouseY+14)
        .text(function(d) { return Math.round(d[Math.round(rollScale(mouseY)/barHeight)]*100)/100; });
}

function onDraggingEnd() {
    hightLightBar.attr("y", function() { return rollScale(parseInt(d3.select(this).attr("y"))); });
}