//////////////////////////////////////////////////////////////////////////////////////////////////////////////
var cd = 6
var newData = [data[0]];
data.splice(0, 1);
while (data.length > 0) {
    var index = 0;
    var value = 0;
    for (var j = 2; j < 8; j++) if (j != cd) value += (data[0][j]-newData[newData.length-1][j])*(data[0][j]-newData[newData.length-1][j]);
    data.forEach(function(d, i) {
        var dist = 0
        for (var j = 2; j < 8; j++) if (j != cd) dist += (d[j]-newData[newData.length-1][j])*(d[j]-newData[newData.length-1][j]);
        dist = Math.sqrt(dist);
        if (dist < value) {
            index = i;
            value = dist;
        }
    });
    newData.push(data[index]);
    data.splice(index, 1);
}
data = newData;
console.log(newData)
console.log(data)

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
var mean = [d3.mean(data, function(d) { return d[0]; }), d3.mean(data, function(d) { return d[1]; })];
data.forEach(function (d, i) {
    var dx = d[0] - mean[0], dy = d[1] - mean[1];
    var angle = Math.atan2(dy, dx)*180/Math.PI;
    d.unshift(angle);
});
//data.sort(function (a, b) { return a[0] - b[0]; });

var transposed = d3.transpose(data)

var dimensions = transposed.length;
var width = 1300, height = 620;
var barHeight = (height-60)/data.length;
var offset = 30;
var barWidth = width/dimensions - offset;

var svg = d3.select("body").append("svg")
    .attr("width", width)
    .attr("height", height);

svg.selectAll("text")
    .data(transposed)
  .enter().append("text")
    .attr("x", function(_, i) { return i*(barWidth + offset) + barWidth/2; })
    .attr("y", height-15)
    .style("font-family", "sans-serif")
    .style("text-anchor", "middle")
    .style("font-size", "14px")
    .text(function(_, i) { return attrName(i); })
    .on("click", function (_, i) { 
        data.sort(function (a, b) { return a[i] - b[i]; });
        updateBars(i);
    });

var widthScale = [];
for (var dim = 0; dim < dimensions; dim++) {
    var maxData = d3.max(data, function(array) { return array[dim]; });
    var minData = d3.min(data, function(array) { return array[dim]; });

    var wScale = d3.scale.linear()
        .domain([minData, maxData])
        .range([0, barWidth]);

    widthScale.push(wScale);
}

var charts = [];
for (var dim = 0; dim < dimensions; dim++) {
    var g = svg.append("g")
        .attr("id", "chart" + dim);
    charts.push(g);
}

for (var dim = 0; dim < dimensions; dim++) {
    charts[dim].selectAll("rect")
        .data(data)
      .enter().append("rect")
        .attr("x", dim*(barWidth + offset))
        .attr("y", function(_, i) { return barHeight*i; })
        .attr("width", function(d) { return widthScale[dim](d[dim]); })
        .attr("height", barHeight)
        .style("fill", "blue");

        var dAxis = d3.svg.axis()
            .scale(widthScale[dim])
            .orient("bottom")
            .ticks(4);

        charts[dim].append("g")
            .attr("class", "axis")
            .attr("transform", "translate(" + (dim*(barWidth + offset)) + " , " + (barHeight*data.length+1) + ")")
            .call(dAxis);
}

function updateBars(key) {
    for (var dim = 0; dim < dimensions; dim++) {
        charts[dim].selectAll("rect")
            .data(data, function (d) { return d; })
          .transition()
            .duration(1000)
            .attr("y", function(_, i) { return barHeight*i; });
    }
}

function attrName(index) {
    switch (index) {
        case 0: return "Angle";
        case 1: return "Pos X";
        case 2: return "Pos Y";
        default: return "Attribute " + (index - 2);
    }
}