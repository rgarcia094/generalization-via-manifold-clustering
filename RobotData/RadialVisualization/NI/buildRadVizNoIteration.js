var mean = [d3.mean(data, function(d) { return d[0]; }), d3.mean(data, function(d) { return d[1]; })];
data.forEach(function (d, i) {
    var dx = d[0] - mean[0], dy = d[1] - mean[1];
    var angle = Math.atan2(dy, dx)*180/Math.PI;
    d.unshift(angle);
});
data.sort(function (a, b) { return a[0] - b[0]; });
for (var i = 0; i < 180; i++) arrayRotate(data);

data = data.map(function(array) { return array.slice(3, 9); });

indexes = [5, 3, 4, 6, 1, 2];
var newData = [];
data.forEach(function(d) {
    var elem = [];
    indexes.forEach(function(e) { elem.push(d[e-1]); });
    newData.push(elem);
});
data = newData;

var colorScale = d3.scale.linear()
    .domain([0, data.length])
    .range(["blue", "red"]);

var transposed = d3.transpose(data)

var dimensions = transposed.length;
var width = 1300, height = 620;
var dimLength = 300;

var dimScales = []
for (var dim = 0; dim < dimensions; dim++) {
    var maxData = d3.max(data, function(array) { return array[dim]; });
    var minData = d3.min(data, function(array) { return array[dim]; });

    var dScale = d3.scale.linear()
        .domain([minData, maxData])
        .range([0, 1]);

    dimScales.push(dScale)
}

var svg = d3.select("body").append("svg")
    .attr("width", width)
    .attr("height", height);

var dimLabels = svg.selectAll("text")
    .data(transposed)
  .enter().append("text")
    .attr("x", function(_, i) { return width/2 + 1.1*dimLength*Math.cos(2*Math.PI/dimensions*i); })
    .attr("y", function(_, i) { return height/2 + 1.1*dimLength*Math.sin(2*Math.PI/dimensions*i); })
    .style("font-family", "sans-serif")
    .style("text-anchor", "middle")
    .style("font-size", "32px")
    .text(function(_, i) { return indexes[i]; });

var dimLines = svg.selectAll("line")
    .data(transposed)
  .enter().append("line")
    .attr("x1", width/2)
    .attr("y1", height/2)
    .attr("x2", function(_, i) { return width/2 + dimLength*Math.cos(2*Math.PI/dimensions*i); })
    .attr("y2", function(_, i) { return height/2 + dimLength*Math.sin(2*Math.PI/dimensions*i); })
    .style("stroke", "gray")
    .style("stroke-width", 5);

var points = svg.selectAll("circle")
    .data(data)
  .enter().append("circle")
    .attr("id", function(_, i) { return "circle" + i; })
    .attr("cx", function(d) { return calculateTranslation(d, "x"); })
    .attr("cy", function(d) { return calculateTranslation(d, "y"); })
    .attr("r", 5)
    .style("fill", function(_, i) { return colorScale(i); })
    .on("mouseover", function(d, i) { showTooltip(d, i, this); })
    .on("mouseout", function() { d3.select("#tooltip").classed("hidden", true); })

function showTooltip(d, i, elem) {
    var xPos = parseFloat(d3.select(elem).attr("cx"));
    var yPos = parseFloat(d3.select(elem).attr("cy"));

    var text = "Attribute " + indexes[0] + ": " + d[0];
    for (var j = 1; j < dimensions; j++) {
        text += " Attribute " + indexes[j] + ": " + d[j];
    }
    text += " Index: " + i;

    d3.select("#tooltip")
      .style("left", xPos + "px")
      .style("top", yPos + "px")
      .select("#value")
      .text(text);

    d3.select("#tooltip").classed("hidden", false);
}

function calculateTranslation(d, v) {
    var len = (v == "x")? width : height;
    var fun = (v == "x")? Math.cos : Math.sin;

    var sum = 0, cv = 0;
    for (var j = 0; j < dimensions; j++) {
        var axis = dimLength*fun(2*Math.PI/dimensions*j);
        cv += axis*dimScales[j](d[j]);
        sum += dimScales[j](d[j]);
    }

    return len/2 + 2*cv/sum;
}

function arrayRotate(arr, reverse){
    if (reverse)
        arr.unshift(arr.pop());
    else
        arr.push(arr.shift());
    return arr;
} 