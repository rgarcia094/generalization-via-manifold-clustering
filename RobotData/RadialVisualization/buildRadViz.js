var mean = [d3.mean(data, function(d) { return d[0]; }), d3.mean(data, function(d) { return d[1]; })];
data.forEach(function (d, i) {
    var dx = d[0] - mean[0], dy = d[1] - mean[1];
    var angle = Math.atan2(dy, dx)*180/Math.PI;
    d.unshift(angle);
});
data.sort(function (a, b) { return a[0] - b[0]; });
for (var i = 0; i < 180; i++) arrayRotate(data);

data = data.map(function(array) { return array.slice(3, 9); });

indexes = d3.range(0, 6); // USAR 6 ENTRE 2 e 3
workingData = data;

var colorScale = d3.scale.linear()
    .domain([0, workingData.length])
    .range(["blue", "red"]);

var transposed = d3.transpose(workingData)

var dimensions = transposed.length;
var width = 1300, height = 620;
var dimLength = 300;

var dimScales = []
createScales();

var svg = d3.select("body").append("svg")
    .attr("width", width)
    .attr("height", height);

var dragger = d3.behavior.drag()
    .on("drag", function() { onDraggingAxes(d3.select(this).datum(), d3.mouse(this), d3.select(this)); })
    .on("dragend", onDraggingEnd);

var dimLabels = svg.selectAll("text")
    .data(d3.range(transposed.length))
  .enter().append("text")
    .attr("x", function(d) { return width/2 + 1.1*dimLength*Math.cos(2*Math.PI/dimensions*d); })
    .attr("y", function(d) { return height/2 + 1.1*dimLength*Math.sin(2*Math.PI/dimensions*d); })
    .style("font-family", "sans-serif")
    .style("text-anchor", "middle")
    .style("font-size", "32px")
    .text(function(d) { return d+1; });

var dimLines = svg.selectAll("line")
    .data(d3.range(transposed.length))
  .enter().append("line")
    .attr("x1", width/2)
    .attr("y1", height/2)
    .attr("x2", function(d) { return width/2 + dimLength*Math.cos(2*Math.PI/dimensions*d); })
    .attr("y2", function(d) { return height/2 + dimLength*Math.sin(2*Math.PI/dimensions*d); })
    .style("stroke", "gray")
    .style("cursor", "move")
    .style("stroke-width", 5)
    .call(dragger);

var points = svg.selectAll("circle")
    .data(workingData)
  .enter().append("circle")
    .attr("id", function(_, i) { return "circle" + i; })
    .attr("cx", function(d) { return calculateTranslation(d, "x"); })
    .attr("cy", function(d) { return calculateTranslation(d, "y"); })
    .attr("r", 5)
    .style("fill", function(_, i) { return colorScale(i); })
    .on("mouseover", function(d, i) { showTooltip(d, i, this); })
    .on("mouseout", function() { d3.select("#tooltip").classed("hidden", true); })

function showTooltip(d, i, elem) {
    var xPos = parseFloat(d3.select(elem).attr("cx"));
    var yPos = parseFloat(d3.select(elem).attr("cy"));

    var text = "Attribute " + (indexes[0]+1) + ": " + d[0];
    for (var j = 1; j < dimensions; j++) {
        text += " Attribute " + (indexes[j]+1) + ": " + d[j];
    }
    text += " Index: " + i;

    d3.select("#tooltip")
      .style("left", xPos + "px")
      .style("top", yPos + "px")
      .select("#value")
      .text(text);

    d3.select("#tooltip").classed("hidden", false);
}

function calculateTranslation(d, v) {
    var len = (v == "x")? width : height;
    var fun = (v == "x")? Math.cos : Math.sin;

    var sum = 0, cv = 0;
    for (var j = 0; j < dimensions; j++) {
        var axis = dimLength*fun(2*Math.PI/dimensions*j);
        cv += axis*dimScales[j](d[j]);
        sum += dimScales[j](d[j]);
    }

    return len/2 + 2*cv/sum;
}

function arrayRotate(arr, reverse){
    if (reverse)
        arr.unshift(arr.pop());
    else
        arr.push(arr.shift());
    return arr;
} 

function onDraggingAxes(datum, mouse, elem) {
    var angle = Math.atan2(mouse[1] - height/2, mouse[0] - width/2);

    elem.attr("x2", width/2 + dimLength*Math.cos(angle))
        .attr("y2", height/2 + dimLength*Math.sin(angle));

    dimLabels
        .attr("x", function(d) { return calcAxisRotation(d, datum, width, Math.cos, angle, d3.select(this).attr("x")); })
        .attr("y", function(d) { return calcAxisRotation(d, datum, height, Math.sin, angle, d3.select(this).attr("y")); })

    function calcAxisRotation(d, datum, size, func, angle, old) {
        if (d == datum) 
            return size/2 + 1.1*dimLength*func(angle);
        else
            return old;
    }
}

function onDraggingEnd() {
    indexes = [];
    dimLines.each(function(d) {
        var angle = Math.atan2(d3.select(this).attr("y2") - height/2, d3.select(this).attr("x2") - width/2);
        indexes.push({elem: d3.select(this), angle: angle });
    });
    indexes.sort(function (a, b) { return a.angle - b.angle; });
    indexes = indexes.map(function(d) { return d.elem.datum(); });
    
    svg.selectAll("line")
        .data(indexes, function(d) { return d; })
      .transition()
        .duration(1000)
        .attr("x2", function(_, i) { return width/2 + dimLength*Math.cos(2*Math.PI/dimensions*i); })
        .attr("y2", function(_, i) { return height/2 + dimLength*Math.sin(2*Math.PI/dimensions*i); })

    svg.selectAll("text")
        .data(indexes, function(d) { return d; })
      .transition()
        .duration(1000)
        .attr("x", function(_, i) { return width/2 + 1.1*dimLength*Math.cos(2*Math.PI/dimensions*i); })
        .attr("y", function(_, i) { return height/2 + 1.1*dimLength*Math.sin(2*Math.PI/dimensions*i); })

    workingData = [];
    data.forEach(function(d) {
        var elem = [];
        indexes.forEach(function(e) { elem.push(d[e]); });
        workingData.push(elem);
    });

    createScales();

    svg.selectAll("circle")
        .data(workingData)
      .transition()
        .duration(1000)
        .attr("cx", function(d) { return calculateTranslation(d, "x"); })
        .attr("cy", function(d) { return calculateTranslation(d, "y"); })
}

function createScales() {
    dimScales = []
    for (var dim = 0; dim < dimensions; dim++) {
        var maxData = d3.max(workingData, function(array) { return array[dim]; });
        var minData = d3.min(workingData, function(array) { return array[dim]; });

        var dScale = d3.scale.linear()
            .domain([minData, maxData])
            .range([0, 1]);

        dimScales.push(dScale)
    }
}