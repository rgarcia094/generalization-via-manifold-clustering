import sys
import numpy as np
import matplotlib.pyplot as plt
from sklearn import manifold

pointsList = []
posList = []
for line in sys.stdin:
    line = line.replace(",", "")
    line = line.replace(";", "")
    attrList = line.split()
    pos = [float(attrList[i]) for i in range(2)]
    attrList = [float(attrList[i]) for i in range(len(attrList)) if i > 1]
    pointsList.append(attrList)
    posList.append(pos)

posList = np.array(posList)
maxP = posList.max()
minP = posList.min()

posMap = np.array([[x, y] for y in range(int(minP), int(maxP+1), 20) for x in range(int(minP), int(maxP+1), 10)])

color = [[(posList[i][0]-minP)/(maxP-minP), 0.0, (posList[i][1]-minP)/(maxP-minP)] for i in range(posList.shape[0])]
colorMap = [[(posMap[i][0]-minP)/(maxP-minP), 0.0, (posMap[i][1]-minP)/(maxP-minP)] for i in range(posMap.shape[0])]

X = np.array(pointsList)

clf = manifold.SpectralEmbedding()
X_lap = clf.fit_transform(X)

fig = plt.figure()
ax = fig.add_subplot(311)
ax.scatter(posMap[:, 0], posMap[:, 1], color=colorMap, cmap=plt.cm.Spectral)
plt.axis('tight')
plt.xticks([]), plt.yticks([])
plt.title('Color Map')

ax = fig.add_subplot(312)
ax.scatter(posList[:, 0], posList[:, 1], color=color, cmap=plt.cm.Spectral)
plt.axis('tight')
plt.xticks([]), plt.yticks([])
plt.title('Original positions')

ax = fig.add_subplot(313)
ax.scatter(X_lap[:, 0], X_lap[:, 1], color=color, cmap=plt.cm.Spectral)
plt.axis('tight')
plt.xticks([]), plt.yticks([])
plt.title('Isomap Projection')
plt.show()