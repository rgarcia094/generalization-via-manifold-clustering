"""
	TO EXECUTE THIS SCRIPT, USE THE FOLLOWING COMMAND LINE:

		python reduction.py ALGORITHM C N < data.txt > result.js

		ALGORITHM = Name of the desired reduction algorithm
		C = Number of components
		N = Number of neighbors
"""
import sys
import argparse as ap
import numpy as np
import matplotlib.pyplot as plt
from sklearn import manifold

# Reading script arguments
parser = ap.ArgumentParser(description="Choose Dimensionality Reduction Algorithm")
parser.add_argument("r_algorithm", metavar="A", choices=["isomap", "lle", "laplacian"], help="Algorithm name", default="isomap")
parser.add_argument("n_components", metavar="C", type=int, help="Number of components", default=2)
parser.add_argument("n_neighbors", metavar="N", type=int, help="Number of neighbors", default=30)
args = parser.parse_args()

# Reading input file
pointsList = []
posList = []
for line in sys.stdin:
    line = line.replace(",", "")
    line = line.replace(";", "")
    attrList = line.split()
    pos = [float(attrList[i]) for i in range(1, 3)]
    attrList = [float(attrList[i]) for i in range(len(attrList)) if i > 2]
    pointsList.append(attrList)
    posList.append(pos)

# Applying position-dependent colors for each point
posList = np.array(posList)
maxP = posList.max()
minP = posList.min()
posMap = np.array([[x, y] for y in range(int(minP), int(maxP+1), 20) for x in range(int(minP), int(maxP+1), 10)])
color = [[(posList[i][0]-minP)/(maxP-minP), 0.0, (posList[i][1]-minP)/(maxP-minP)] for i in range(posList.shape[0])]
colorMap = [[(posMap[i][0]-minP)/(maxP-minP), 0.0, (posMap[i][1]-minP)/(maxP-minP)] for i in range(posMap.shape[0])]

# Preparing data
X = np.array(pointsList)
n_neighbors = args.n_neighbors
n_components = args.n_components

# Apllying the reduction algorithm
if args.r_algorithm == "isomap":
	result = manifold.Isomap(n_neighbors, n_components=n_components).fit_transform(X)
elif args.r_algorithm == "lle":
	clf = manifold.LocallyLinearEmbedding(n_neighbors, n_components=n_components, method="standard")
	result = clf.fit_transform(X)
else:
	clf = manifold.SpectralEmbedding(n_components=n_components, n_neighbors=n_neighbors)
	result = clf.fit_transform(X)

# Printing output
# sys.stdout.write("var data = [")
# for i in range(len(posList)):
# 	if i == 0:
# 		sys.stdout.write("[")
# 	else:
# 		sys.stdout.write(", [")
# 	sys.stdout.write(str(posList[i][0]) + ", " + str(posList[i][1]))
# 	for j in range(len(result[i])):
# 		sys.stdout.write(", " + str(result[i][j]))
# 	sys.stdout.write("]")
# sys.stdout.write("];")


# Plotting
fig = plt.figure()
ax = fig.add_subplot(311)
ax.scatter(posMap[:, 0], posMap[:, 1], color=colorMap, cmap=plt.cm.Spectral)
plt.axis('tight')
plt.xticks([]), plt.yticks([])
plt.title('Color Map')

ax = fig.add_subplot(312)
ax.scatter(posList[:, 0], posList[:, 1], color=color, cmap=plt.cm.Spectral)
plt.axis('tight')
plt.xticks([]), plt.yticks([])
plt.title('Original positions')

#result = np.array(sorted(result, key=lambda r_entry: r_entry[0]))
#print result

ax = fig.add_subplot(313)
ax.scatter(result[:, 0], result[:, 1], color=color[:], cmap=plt.cm.Spectral)
plt.axis('tight')
plt.xticks([]), plt.yticks([])
plt.title('Isomap Projection')
plt.show()