import sys
import argparse as ap
import numpy as np
import matplotlib.pyplot as plt
import gap

from sklearn import cluster, manifold

n_neighbors = 30

pointsList = []
positionList = []
for line in sys.stdin:
    attrList = line.replace(",", "").replace(";", "").split()
    pointsList.append([float(attrList[i]) for i in range(3, len(attrList))])
    positionList.append([float(attrList[i]) for i in range (1, 3)])

C = np.array(positionList)
X = np.array(pointsList)

g = gap.gap(X, ks=range(1,20))
print g

kmeans = cluster.KMeans(n_clusters=2)
kmeans_result = kmeans.fit_predict(X)

plt.figure(figsize=(12, 12))
plt.subplot(211)
plt.scatter(C[:, 0], C[:, 1], c=kmeans_result)
plt.title("K-Means Clustering")

isomap_result = manifold.Isomap(n_neighbors, n_components=2).fit_transform(X)

plt.subplot(212)
plt.scatter(isomap_result[:, 0], isomap_result[:, 1], c=kmeans_result)
plt.title("Isomap Reduction")
plt.show()

"""
Clusters:

k = 3
C1 = [-163.91, 114.726] [199.96, 3.53] = [145, 1]
C2 = [200, 0.062] [-55.192, -192.23] = [0, -106]
C3 = [-58.57, -191.03] [-165.9, 111.96] [-107, 146]
--------------
k = 2
C1 = [199.99, 0.028] - [-199.96, -6.606] = [0, 178]
C2 = [-199.84, -10.07] - [199.97, 3.59] = [-177, 1]

The point [81, -182] (angle -66) is the only one which the cluster isn't continuos (K >= 7)

"""