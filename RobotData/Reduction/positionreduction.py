import sys
import numpy as np
import matplotlib.pyplot as plt
from sklearn import manifold

posList = []
for line in sys.stdin:
	line = line.replace(",", "")
	line = line.replace(";", "")
	attrList = line.split()
	pos = [float(attrList[i]) for i in range(2)]
	posList.append(pos)

posList = np.array(posList)
maxP = posList.max()
minP = posList.min()

posMap = np.array([[x, y] for y in range(int(minP), int(maxP+1), 20) for x in range(int(minP), int(maxP+1), 10)])

color = [[(posList[i][0]-minP)/(maxP-minP), 0.0, (posList[i][1]-minP)/(maxP-minP)] for i in range(posList.shape[0])]

X = np.array(posList)
n_neighbors = 2

X_iso = manifold.Isomap(n_neighbors, n_components=1).fit_transform(X)

fig = plt.figure()
ax = fig.add_subplot(211)
ax.scatter(posList[:, 0], posList[:, 1], color=color, cmap=plt.cm.Spectral)
plt.axis('tight')
plt.xticks([]), plt.yticks([])
plt.title('Color Map')

ax = fig.add_subplot(212)
ax.scatter(X_iso[:], X_iso[:], color=color, cmap=plt.cm.Spectral)
plt.axis('tight')
plt.xticks([]), plt.yticks([])
plt.title('Isomap Projection')
plt.show()