var mean = [d3.mean(data, function(d) { return d[0]; }), d3.mean(data, function(d) { return d[1]; })];
data.forEach(function (d, i) {
    var dx = d[0] - mean[0], dy = d[1] - mean[1];
    var angle = Math.atan2(dy, dx)*180/Math.PI;
    d.unshift(angle);
});

var maxMatrix = d3.max(data, function(array) { return array[0]; });
var minMatrix = d3.min(data, function(array) { return array[0]; });

var blue_to_brown = d3.scale.linear()
	.domain([minMatrix, maxMatrix])
	.range(["steelblue", "brown"])
	.interpolate(d3.interpolateLab);

var mapper = d3.range(0, data.length, 1).map(function(i) {
	return {
		"angle": data[i][0],
		"x": data[i][1],
		"y": data[i][2],
		"attr1": data[i][3],
		"attr2": data[i][4],
		"attr3": data[i][5],
		"attr4": data[i][6],
		"attr5": data[i][7],
		"attr6": data[i][8]
	};
});

var pc2 = d3.parcoords()("#example")
	.data(mapper)
	.color(function(d) { return blue_to_brown(d["x"]); })
	.alpha(0.25)
	.margin({ top: 24, left: 0, bottom: 12, right: 0 })
  .render()
	.brushMode("1D-axes")
	.reorderable();