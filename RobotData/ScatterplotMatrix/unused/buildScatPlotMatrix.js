var mean = [d3.mean(data, function(d) { return d[0]; }), d3.mean(data, function(d) { return d[1]; })];
data.forEach(function (d, i) {
    var dx = d[0] - mean[0], dy = d[1] - mean[1];
    var angle = Math.atan2(dy, dx)*180/Math.PI;
    d.unshift(angle);
});
data.sort(function (a, b) { return a[0] - b[0]; });

var transposed = d3.transpose(data)

var dimensions = transposed.length;
var width = 1250, height = 1250;
var matrixSize = 1250;
var padding = 10;
var scatterplotSize = (matrixSize - padding*(dimensions))/dimensions;

var plotsIndex = d3.merge(d3.range(dimensions).map(function(a) { 
	return d3.range(dimensions).map(function(b) {
		return [a, b];
	});
}));

var svg = d3.select("body").append("svg")
	.attr("height", height)
	.attr("width", width);

var scatterPlots = svg.selectAll("g")
	.data(plotsIndex)
  .enter().append("g");

var borders = scatterPlots.append("rect")
  	.attr("x", function(d) { return (scatterplotSize+padding)*d[0]; })
  	.attr("y", function(d) { return (scatterplotSize+padding)*d[1]; })
  	.attr("width", scatterplotSize)
  	.attr("height", scatterplotSize)
  	.style("fill", "none")
  	.style("stroke-width", 1)
  	.style("stroke", "gray");

/*var circles = scatterPlots.append("circle")
	.attr("cx", function(d) { return (scatterplotSize+padding)*d[0] + scatterplotSize/2; })
  	.attr("cy", function(d) { return (scatterplotSize+padding)*d[1] + scatterplotSize/2; })
  	.attr("r", scatterplotSize/2)
  	.style("fill", "red");*/

var dataTest = [[20, 20, 5], [10, 10, 5]];

var circles = scatterPlots.each(function(d, i) {
	console.log(d3.selection(this));
});


/*selectAll("circle")
	.data(dataTest)
  .enter().append("circle")
	.attr("cx", function(d) { console.log(this); return d[0]; })
  	.attr("cy", function(d) { return d[1]; })
  	.attr("r", scatterplotSize/2)
  	.style("fill", "red");*/