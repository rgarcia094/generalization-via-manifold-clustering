import sys
import math
import numpy as np
import matplotlib.pyplot as plt
from sklearn import manifold

posList = []
for line in sys.stdin:
	line = line.replace(",", "")
	line = line.replace(";", "")
	attrList = line.split()
	attrList = map(float, attrList)
	posList.append(attrList)
posList = np.array(posList)
mean = [posList[:, i].mean() for i in range(2)]

finalArray = []
for i in range(len(posList)):
	dx = posList[i][0] - mean[0]
	dy = posList[i][1] - mean[1]
	angle = math.atan2(dy, dx)*180.0/math.pi
	newArray = np.insert(posList[i], 0, angle)
	finalArray.append(newArray)

print "Angle, PosX, PosY, Attr1, Attr2, Attr3, Attr4, Attr5, Attr6"
for i in range(len(finalArray)):
	print finalArray[i][0], ",", finalArray[i][1], ",", finalArray[i][2], ",", finalArray[i][3], ",", finalArray[i][4], ",", finalArray[i][5], ",", finalArray[i][6], ",", finalArray[i][7], ",", finalArray[i][8]
