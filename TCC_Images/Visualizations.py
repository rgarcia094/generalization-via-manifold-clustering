from __future__ import division, print_function

import matplotlib.pyplot as plt
import matplotlib.style as stl
import numpy as np
import pandas as pd
import skfuzzy as fuzz
import math

from pandas.tools.plotting import scatter_matrix, parallel_coordinates, radviz, lag_plot
from sklearn.decomposition import PCA
from sklearn.manifold import MDS, Isomap, LocallyLinearEmbedding, SpectralEmbedding, TSNE
from sklearn.metrics import euclidean_distances
from sklearn.cluster import KMeans, DBSCAN

# stl.use('ggplot')

# # Errors bars
# for i in range(1, 7):
# 	folder = 'Datasets/ObstaclesLosangle/Test0' + str(i) + '/'
# 	errors = np.genfromtxt(folder + 'errors.csv', delimiter=',')
# 	seeds = np.genfromtxt(folder + 'seeds.csv', delimiter=',')
# 	angles = np.array(range(360))
# 	colors = plt.cm.cool(seeds)

# 	fig = plt.figure()
# 	fig.suptitle("Obstacles Free (Test " + str(i) + ") - Training Error", fontsize=16, fontweight='bold')

# 	fig.add_subplot(111)
# 	plt.bar(angles, errors, color=colors)

# 	plt.xlabel('Angle')
# 	plt.ylabel('Error')
# 	plt.xlim([0, 360])
# 	plt.ylim([0, 30])

# plt.show()


# folder = 'Datasets/ObstaclesFree/Test06/'
# # folder = 'Datasets/ObstaclesLosangle/Test02/'

# # Load Datasets ####################################################################################################

# data = np.genfromtxt(folder + 'dataset.csv', delimiter=',')[:, 1:]
# errors = np.genfromtxt(folder + 'errors.csv', delimiter=',')
# angles = np.array(range(1, 361)).astype(int)
# colors = np.zeros((360, 1))
# # colors = np.r_[np.ones((50, 1)), np.zeros((310, 1))]
# # colors = np.r_[np.zeros((110, 1)), np.ones((20, 1)), np.zeros((230, 1))]

# dataset = np.c_[angles, data, errors]

# # Scatterplot Matrix ###############################################################################################
# stl.use('ggplot')

# for i in range(1, 7):
# 	folder = 'Datasets/ObstaclesFree/Test0' + str(i) + '/'
# 	# folder = 'Datasets/ObstaclesLosangle/Test0' + str(i) + '/'

# 	dataset = np.genfromtxt(folder + 'dataset.csv', delimiter=',')[:, 1:]
# 	errors = np.genfromtxt(folder + 'errors.csv', delimiter=',')
# 	seeds = np.genfromtxt(folder + 'seeds.csv', delimiter=',')
	
# 	angles = np.array(range(360))

# 	data = np.c_[angles, dataset, errors]

# 	df = pd.DataFrame(data, columns=['Angle', 'Attr 1', 'Attr 2', 'Attr 3', 'Attr 4', 'Attr 5', 'Attr 6', 'Error'])
# 	scatter_matrix(df, alpha=0.2, figsize=(10, 10), diagonal='kde', c=seeds, cmap=plt.cm.seismic)

# 	plt.suptitle("Obstacles Free (Test " + str(i) + ")", fontsize=16, fontweight='bold')
# 	# plt.suptitle("Obstacles Losangle (Test " + str(i) + ")", fontsize=16, fontweight='bold')

# plt.show()

# # PCA ##############################################################################################################
# stl.use('ggplot')

# for i in range(1, 7):
# 	# folder = 'Datasets/ObstaclesFree/Test0' + str(i) + '/'
# 	folder = 'Datasets/ObstaclesLosangle/Test0' + str(i) + '/'

# 	dataset = np.genfromtxt(folder + 'dataset.csv', delimiter=',')[:, 1:]
# 	errors = np.genfromtxt(folder + 'errors.csv', delimiter=',')
# 	seeds = np.genfromtxt(folder + 'seeds.csv', delimiter=',')
	
# 	angles = np.array(range(360))

# 	pca = PCA(n_components=2).fit(dataset)
# 	data_low = pca.transform(dataset)

# 	# Plot result
# 	fig = plt.figure()
# 	# fig.suptitle("PCA Projection - Obstacles Free (Test " + str(i) + ")", fontsize=16, fontweight='bold')
# 	fig.suptitle("PCA Projection - Obstacles Losangle (Test " + str(i) + ")", fontsize=16, fontweight='bold')
	
# 	fig.add_subplot(111)
# 	plt.scatter(data_low[:, 0], data_low[:, 1], c=angles, cmap=plt.cm.winter)
# 	plt.colorbar()
	
# 	plt.axis('tight')
# 	plt.xticks([]), plt.yticks([])

# plt.show()

# # MDS ##############################################################################################################
# stl.use('ggplot')

# for i in range(1, 7):
# 	# folder = 'Datasets/ObstaclesFree/Test0' + str(i) + '/'
# 	folder = 'Datasets/ObstaclesLosangle/Test0' + str(i) + '/'

# 	dataset = np.genfromtxt(folder + 'dataset.csv', delimiter=',')[:, 1:]
# 	errors = np.genfromtxt(folder + 'errors.csv', delimiter=',')
# 	seeds = np.genfromtxt(folder + 'seeds.csv', delimiter=',')
	
# 	angles = np.array(range(360))

# 	clf = MDS(n_components=2, n_init=1, max_iter=100, dissimilarity="precomputed")
# 	mds = clf.fit_transform(euclidean_distances(dataset))

# 	# Plot result
# 	fig = plt.figure()
# 	# fig.suptitle("MDS Projection - Obstacles Free (Test " + str(i) + ")", fontsize=16, fontweight='bold')
# 	fig.suptitle("MDS Projection - Obstacles Losangle (Test " + str(i) + ")", fontsize=16, fontweight='bold')
	
# 	fig.add_subplot(111)
# 	plt.scatter(mds[:, 0], mds[:, 1], c=angles, cmap=plt.cm.Spectral)
# 	plt.colorbar()
	
# 	plt.axis('tight')
# 	plt.xticks([]), plt.yticks([])

# plt.show()

# # Isomap ##############################################################################################################
# stl.use('ggplot')

# # for i in range(1, 7):
# i = 6
# # folder = 'Datasets/ObstaclesFree/Test0' + str(i) + '/'
# folder = 'Datasets/ObstaclesLosangle/Test0' + str(i) + '/'

# dataset = np.genfromtxt(folder + 'dataset.csv', delimiter=',')[:, 1:]
# errors = np.genfromtxt(folder + 'errors.csv', delimiter=',')
# seeds = np.genfromtxt(folder + 'seeds.csv', delimiter=',')

# angles = np.array(range(360))

# isomap = Isomap(n_neighbors=25, n_components=2).fit_transform(dataset)

# # Plot result
# fig = plt.figure()
# # fig.suptitle("Isomap Projection - Obstacles Free (Test " + str(i) + ")", fontsize=16, fontweight='bold')
# fig.suptitle("Isomap Projection - Obstacles Losangle (Test " + str(i) + ")", fontsize=16, fontweight='bold')

# fig.add_subplot(111)
# plt.scatter(isomap[:, 0], isomap[:, 1], c=angles, cmap=plt.cm.winter)
# plt.colorbar()

# plt.axis('tight')
# plt.xticks([]), plt.yticks([])

# plt.show()

# # LLE ##############################################################################################################
# stl.use('ggplot')

# for i in range(1, 7):
# 	# folder = 'Datasets/ObstaclesFree/Test0' + str(i) + '/'
# 	folder = 'Datasets/ObstaclesLosangle/Test0' + str(i) + '/'

# 	dataset = np.genfromtxt(folder + 'dataset.csv', delimiter=',')[:, 1:]
# 	errors = np.genfromtxt(folder + 'errors.csv', delimiter=',')
# 	seeds = np.genfromtxt(folder + 'seeds.csv', delimiter=',')
	
# 	angles = np.array(range(360))

# 	lle = LocallyLinearEmbedding(n_neighbors=100, n_components=2, method='standard').fit_transform(dataset)

# 	# Plot result
# 	fig = plt.figure()
# 	# fig.suptitle("LLE Projection - Obstacles Free (Test " + str(i) + ")", fontsize=16, fontweight='bold')
# 	fig.suptitle("LLE Projection - Obstacles Losangle (Test " + str(i) + ")", fontsize=16, fontweight='bold')
	
# 	fig.add_subplot(111)
# 	plt.scatter(lle[:, 0], lle[:, 1], c=angles, cmap=plt.cm.winter)
# 	plt.colorbar()
	
# 	plt.axis('tight')
# 	plt.xticks([]), plt.yticks([])

# plt.show()

# # Laplacian #########################################################################################################
# stl.use('ggplot')

# for i in range(1, 7):
# 	folder = 'Datasets/ObstaclesFree/Test0' + str(i) + '/'
# 	# folder = 'Datasets/ObstaclesLosangle/Test0' + str(i) + '/'

# 	dataset = np.genfromtxt(folder + 'dataset.csv', delimiter=',')[:, 1:]
# 	errors = np.genfromtxt(folder + 'errors.csv', delimiter=',')
# 	seeds = np.genfromtxt(folder + 'seeds.csv', delimiter=',')
	
# 	angles = np.array(range(360))

# 	laplacian = SpectralEmbedding(n_neighbors=20, n_components=2).fit_transform(dataset)

# 	# Plot result
# 	fig = plt.figure()
# 	fig.suptitle("Laplacian Projection - Obstacles Free (Test " + str(i) + ")", fontsize=16, fontweight='bold')
# 	# fig.suptitle("Laplacian Eigenmap Projection - Obstacles Losangle (Test " + str(i) + ")", fontsize=16, fontweight='bold')
	
# 	fig.add_subplot(111)
# 	plt.scatter(laplacian[:, 0], laplacian[:, 1], c=angles, cmap=plt.cm.Spectral)
# 	plt.colorbar()
	
# 	plt.axis('tight')
# 	plt.xticks([]), plt.yticks([])

# plt.show()

# # t-SNE #############################################################################################################
# stl.use('ggplot')

# for i in range(1, 7):
# 	# folder = 'Datasets/ObstaclesFree/Test0' + str(i) + '/'
# 	folder = 'Datasets/ObstaclesLosangle/Test0' + str(i) + '/'

# 	dataset = np.genfromtxt(folder + 'dataset.csv', delimiter=',')[:, 1:]
# 	errors = np.genfromtxt(folder + 'errors.csv', delimiter=',')
# 	seeds = np.genfromtxt(folder + 'seeds.csv', delimiter=',')
	
# 	angles = np.array(range(360))

# 	tsne = TSNE(n_components=2, perplexity=20).fit_transform(dataset)
# 	print (tsne.shape)

# 	# Plot result
# 	fig = plt.figure()
# 	# fig.suptitle("t-SNE Projection - Obstacles Free (Test " + str(i) + ")", fontsize=16, fontweight='bold')
# 	fig.suptitle("t-SNE Eigenmap Projection - Obstacles Losangle (Test " + str(i) + ")", fontsize=16, fontweight='bold')
	
# 	fig.add_subplot(111)
# 	plt.scatter(tsne[:, 0], tsne[:, 1], c=angles, cmap=plt.cm.Spectral)
# 	plt.colorbar()
	
# 	plt.axis('tight')
# 	plt.xticks([]), plt.yticks([])

# plt.show()

# # Dimensions Scatterplot #############################################################################################################
# stl.use('ggplot')

# # Plot result
# fig = plt.figure()
# # fig.suptitle("Dimensions Scatterplots - Obstacles Free", fontsize=16, fontweight='bold')
# fig.suptitle("Dimensions Scatterplots - Obstacles Losangle", fontsize=16, fontweight='bold')

# for i in range(1, 7):
# 	fig.add_subplot(230 + i)
# 	plt.title("Attribute " + str(i))

# 	for j in range(2, 7):
# 		# folder = 'Datasets/ObstaclesFree/Test0' + str(j) + '/'
# 		folder = 'Datasets/ObstaclesLosangle/Test0' + str(j) + '/'
# 		colors = ['b', 'c', 'y', 'm', 'r', 'g']

# 		dataset = np.genfromtxt(folder + 'dataset.csv', delimiter=',')[:, i]
# 		angles = np.array(range(360))

# 		plt.scatter(angles, dataset, c=colors[j-1], label="Dataset " + str(j))

# 	if i % 3 == 1:
# 		plt.ylabel('Attribute Value')
# 	if i > 3:
# 		plt.xlabel('Angle')

# 	plt.axis('tight')
# 	# plt.xlim([0, 360])
# 	# plt.ylim([-3100, 2500])

# plt.legend(bbox_to_anchor=(1.025, 0.85, 1., .102), loc=3, ncol=1, borderaxespad=0.)
# plt.show()

# Error Bar Chart #############################################################################################################
stl.use('ggplot')

# Plot result
fig = plt.figure()
fig.suptitle("Error Chart - Obstacles Free", fontsize=16, fontweight='bold')
# fig.suptitle("Error Chart - Obstacles Losangle", fontsize=16, fontweight='bold')

for i in range(1, 7):
	fig.add_subplot(230 + i)
	plt.title("Dataset " + str(i))

	folder = 'Datasets/ObstaclesFree/Test0' + str(i) + '/'
	# folder = 'Datasets/ObstaclesLosangle/Test0' + str(i) + '/'

	errors = np.genfromtxt(folder + 'errors.csv', delimiter=',')
	seeds = np.genfromtxt(folder + 'seeds.csv', delimiter=',')

	angles = np.array(range(360))
	colors = plt.cm.winter(seeds)

	plt.bar(angles, errors, color=colors)

	if i % 3 == 1:
		plt.ylabel('Error')
	if i > 3:
		plt.xlabel('Angle')

	plt.xlim([0, 360])
	plt.ylim([0, 200])

plt.show()

# # K-Means Elbow Method #######################################################################################################
# stl.use('ggplot')

# fig = plt.figure()
# # fig.suptitle("Elbow Method using K-Means - Obstacles Free Datasets", fontsize=16, fontweight='bold')
# fig.suptitle("Elbow Method using K-Means - Obstacles Losangle Datasets", fontsize=16, fontweight='bold')
# for i in range(1, 7):
# 	# folder = 'Datasets/ObstaclesFree/Test0' + str(i) + '/'
# 	folder = 'Datasets/ObstaclesLosangle/Test0' + str(i) + '/'

# 	dataset = np.genfromtxt(folder + 'dataset.csv', delimiter=',')[:, 1:]
# 	angles = np.array(range(360))

# 	min_k = 2
# 	max_k = 17

# 	sse = np.zeros(max_k)
# 	for k in range(min_k, max_k):
# 		kmeans = KMeans(n_clusters=k).fit(dataset)
# 		labels = kmeans.labels_

# 		for l in range(max(labels) + 1):
# 			c_data = dataset[labels == l, :]
# 			c_mean = np.mean(c_data, axis=0)

# 			c_data = c_data - c_mean
# 			c_data = np.sqrt(np.sum(c_data**2, axis=1))

# 			sse[k] += np.mean(c_data)

# 		sse[k] = sse[k]/k

# 	fig.add_subplot(230 + i)

# 	plt.title("Test Dataset " + str(i), fontsize=8)

# 	plt.scatter(range(min_k, max_k), sse[min_k:])
	
# 	plt.xlabel('Number of Clusters', fontsize=8)
# 	plt.ylabel('Average Sum of Square Errors', fontsize=8)

# plt.axis('tight')

# plt.show()

# # K-Means Clusterization #######################################################################################################
# stl.use('ggplot')

# for t in range(1, 7):
# 		for n_clusters in range(1, 9):
# 			folder = "../TCC_Images/Datasets/ObstaclesLosangle/Test0" + str(t) + "/"

# 			dataset = np.genfromtxt(folder + 'dataset.csv', delimiter=',')[:, 1:]
# 			angles = np.array(range(360))

# 			kmeans = KMeans(n_clusters=n_clusters).fit(dataset)
# 			labels = kmeans.labels_.astype(float)
# 			labels = labels / np.max(labels)
# 			colors = plt.cm.Paired(labels.astype(float))

# 			fig = plt.figure(figsize=(20,10))
# 			fig.suptitle("K-Means - Obstacles Losangle (Test 0" + str(t) + " / " + str(n_clusters) + " Clusters)", fontsize=16, fontweight='bold')

# 			for j in range(6):
# 				fig.add_subplot(231 + j)
# 				plt.title('Attribute ' + str(j+1))

# 				plt.scatter(angles, dataset[:, j], c=colors)

# 				if j >= 3:
# 					plt.xlabel('Angle')
# 				if j % 3 == 0:
# 					plt.ylabel('Attribute Value')

# 				# plt.xlim([0, 360])
# 				# plt.ylim([-3100, 2500])

# 			np.savetxt(folder + 'clusters_kmeans_test0' + str(t) + '_k' + str(n_clusters) + '.csv', kmeans.labels_, fmt='%d', delimiter=',', newline='\n')
# 			fig.savefig(folder + 'KMeans_test0' + str(t) + '_k' + str(n_clusters) + '.png')

# 			# plt.show()
# 			plt.close(fig)

# # DBSCAN Clusterization #######################################################################################################
# stl.use('ggplot')

# for n_test in range(1, 7):
# 	folder = 'Datasets/ObstaclesFree/Test0' + str(n_test) + '/'
# 	# folder = 'Datasets/ObstaclesLosangle/Test0' + str(n_test) + '/'

# 	dataset = np.genfromtxt(folder + 'dataset.csv', delimiter=',')[:, 1:]
# 	angles = np.array(range(360))

# 	max_sample = np.max(dataset)
# 	min_sample = np.min(dataset)

# 	dataset = (dataset - min_sample)/(max_sample - min_sample)

# 	dbscan = DBSCAN(eps=0.1, min_samples=5).fit(dataset)

# 	dataset = dataset*(max_sample - min_sample) + min_sample

# 	labels = dbscan.labels_.astype(float)

# 	labels = labels / np.max(labels)
# 	colors = plt.cm.Paired(labels.astype(float))

# 	fig = plt.figure()
# 	fig.suptitle("DBSCAN - Obstacles Free (Test 0" + str(n_test) + ")", fontsize=16, fontweight='bold')
# 	# fig.suptitle("DBSCAN - Obstacles Losangle (Test 0" + str(n_test) + ")", fontsize=16, fontweight='bold')

# 	for j in range(6):
# 		fig.add_subplot(231 + j)
# 		plt.title('Attribute ' + str(j+1))

# 		plt.scatter(angles, dataset[:, j], c=colors)

# 		if j >= 3:
# 			plt.xlabel('Angle')
# 		if j % 3 == 0:
# 			plt.ylabel('Attribute Value')

# 		plt.xlim([0, 360])
# 		plt.ylim([-3100, 2500])

# 	np.savetxt(folder + 'clusters_dbscan.csv', dbscan.labels_, fmt='%d', delimiter=',', newline='\n')

# plt.show()

# # SSC Reader #######################################################################################################
# stl.use('ggplot')

# for n_test in range(1, 7):
# 	folder = 'Datasets/ObstaclesFree/Test0' + str(n_test) + '/'
# 	# folder = 'Datasets/ObstaclesLosangle/Test0' + str(n_test) + '/'

# 	dataset = np.genfromtxt(folder + 'dataset.csv', delimiter=',')[:, 1:]
# 	labels = np.genfromtxt(folder + 'clusters_ssc.csv', delimiter=',').astype(float)
# 	angles = np.array(range(360))

# 	labels = labels / np.max(labels)
# 	colors = plt.cm.Paired(labels.astype(float))

# 	fig = plt.figure()
# 	fig.suptitle("SSC - Obstacles Free (Test 0" + str(n_test) + " / 10 Clusters)", fontsize=16, fontweight='bold')
# 	# fig.suptitle("SSC - Obstacles Losangle (Test 0" + str(n_test) + " / 10 Clusters)", fontsize=16, fontweight='bold')

# 	for j in range(6):
# 		fig.add_subplot(231 + j)
# 		plt.title('Attribute ' + str(j+1))

# 		plt.scatter(angles, dataset[:, j], c=colors)

# 		if j >= 3:
# 			plt.xlabel('Angle')
# 		if j % 3 == 0:
# 			plt.ylabel('Attribute Value')

# 		plt.xlim([0, 360])
# 		plt.ylim([-3100, 2500])

# plt.show()

# #########################################################################################################
# stl.use('ggplot')

# folders = ['Datasets/ObstaclesLosangle/Test02/',
# 		   'Datasets/ObstaclesLosangle/Test03/',
# 		   'Datasets/ObstaclesLosangle/Test04/',
# 		   'Datasets/ObstaclesLosangle/Test05/',
# 		   'Datasets/ObstaclesLosangle/Test06/',
# 		   'Datasets/ObstaclesFree/Test02/']

# fig = plt.figure()
# fig.suptitle("Original - Performance Error", fontsize=16, fontweight='bold')

# for i in range(6):
# 	fig.add_subplot(231 + i)
# 	if i == 5:
# 		plt.title("Dataset 2 - Obstacles Free")
# 	else:
# 		plt.title("Dataset " + str(i+2) + " - Obstacles Losangle")

# 	folder = folders[i]

# 	errors = np.genfromtxt(folder + 'errors.csv', delimiter=',')
# 	# clusters = np.genfromtxt(folder + 'Regression_Clusters.csv', delimiter=',')

# 	angles = np.array(range(360))
# 	# colors = plt.cm.Paired(clusters)

# 	plt.bar(angles, errors, color='black')

# 	if i % 3 == 0:
# 		plt.ylabel('Error')
# 	if i > 2:
# 		plt.xlabel('Angle')

# 	plt.xlim([0, 360])
# 	plt.ylim([0, 200])

# plt.show()