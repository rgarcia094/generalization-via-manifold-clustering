import FileHandler as fh
import pickle

import numpy as np
import matplotlib.pyplot as plt

from sklearn.gaussian_process import GaussianProcess
from sklearn.svm import SVR
from sklearn.kernel_ridge import KernelRidge
from scipy.stats import zscore

colormap = ['red', 'blue', 'yellow', 'green', 'cyan', 'pink', 'gray', 'orange', 'brown', 'violet']

def apply_colormap(c):
    return colormap[c % len(colormap)]
vec_apply_colormap = np.vectorize(apply_colormap)

# data = fh.get_robot_data("Obstacle_Free/robot_data_4_clusters.csv")
# errors = np.genfromtxt("Obstacle_Free/training_error.csv", delimiter=",")[:, 1]
data = fh.get_robot_data("Obstacle_Normal/obstacle_data_8_clusters.csv")
errors = np.genfromtxt("Obstacle_Normal/training_error.csv", delimiter=",")[:, 1]

clusters = data[:, 0]
angles = np.round(np.degrees(data[:, 1])).astype(int)
robot = data[:, 2:]

n_clusters = np.max(clusters)
x_range = np.arange(min(angles), max(angles)+0.25, 0.25)

fig = plt.figure()

# fig.suptitle('Regression Obstacles Free', fontsize=16, fontweight='bold')
# fig.suptitle('Regression Obstacles Free (using Training Error)', fontsize=16, fontweight='bold')
# fig.suptitle('Regression Obstacles', fontsize=16, fontweight='bold')
fig.suptitle('Regression Obstacles (using Training Error)', fontsize=16, fontweight='bold')

regression_data = []
regression_error = []
for i in range(robot.shape[1]):
    ax = fig.add_subplot(231 + i)
    regression_attr = []
    regression_attr_error = []
    for j in range(int(n_clusters)):
        X = np.atleast_2d(angles[clusters == j+1]).T
        y = robot[clusters == j+1, i]
        
        #gp = GaussianProcess(corr='squared_exponential', theta0=1e-2, thetaL=1e-4, thetaU=1e-1, nugget=10e-10)
        #gp = GaussianProcess(corr='squared_exponential', theta0=1e-2, thetaL=1e-4, thetaU=1e-1, nugget=10e-10*errors[clusters == j+1])

        # for k in range(359):
        #     errors[k] = abs(errors[k] - errors[k+1])/abs(robot[k, i] - robot[k+1, i])
        # nugget = 1e-5*errors

        nugget = 1e-5*np.abs(zscore(errors[clusters == j+1]))
        gp = GaussianProcess(corr='squared_exponential', theta0=1e-2, thetaL=1e-4, thetaU=1e-1, nugget=nugget)
        gp.fit(X, y)

        x = x_range[x_range >= min(angles[clusters == j+1])]
        x = x[x <= max(angles[clusters == j+1])]
        x = np.atleast_2d(x).T
        y_pred, _ = gp.predict(x, eval_MSE=True)

        if regression_attr == []:
            regression_attr = np.c_[np.atleast_2d(x), np.atleast_2d(y_pred).T]
        else:
            regression_attr = np.r_[regression_attr, np.c_[np.atleast_2d(x), np.atleast_2d(y_pred).T]]

        plt.scatter(X, y, s=1, color=apply_colormap(j))
        plt.plot(x, y_pred, color=apply_colormap(j))

        Y_pred = gp.predict(X)
        regression_attr_error = np.append(regression_attr_error, np.abs(y - Y_pred))

    if regression_error == []:
        regression_error = np.atleast_2d(regression_attr_error)
    else:
        regression_error = np.r_[regression_error, np.atleast_2d(regression_attr_error)]

    plt.axis('tight')
    plt.title('Dimension ' + str(i+1), fontsize=12)

    if regression_data == []:
        regression_data = regression_attr
    else:
        regression_data = np.c_[regression_data, np.atleast_2d(regression_attr[:, 1]).T]

# np.savetxt('RegressionSolution.csv', regression_data, fmt='%.4f', delimiter=',', newline='\n')
# np.savetxt('RegressionError.csv', regression_error, fmt='%.4f', delimiter=',', newline='\n')

# for i in range(regression_data.shape[0]):
#     if regression_data[i, 0] == int(regression_data[i, 0]):
#         filename = "RegressionFiles/" + str(int(regression_data[i, 0])) + ".pkl"
#     else:
#         filename = "RegressionFiles/" + str(regression_data[i, 0]) + ".pkl"
#     param_file = open(filename, "w")
#     pickle.dump([regression_data[i, 1:], [], 1, 0, 3], param_file)

###################################################

fig = plt.figure()
# fig.suptitle('Obstacles Free Regression Error', fontsize=16, fontweight='bold')
# fig.suptitle('Obstacles Free Regression Error (using Training Error)', fontsize=16, fontweight='bold')
# fig.suptitle('Obstacles Regression Error', fontsize=16, fontweight='bold')
fig.suptitle('Obstacles Regression Error (using Training Error)', fontsize=16, fontweight='bold')

for i in range(robot.shape[1]):
    fig.add_subplot(231 + i)
    plt.bar(angles, regression_error[i, :], color=vec_apply_colormap((clusters-1).astype(int)), edgecolor='none')
    plt.xlabel('Angle')
    plt.ylabel('Error')
    plt.xlim([0, 360])
    plt.ylim([0, 500])

###################################################

fig = plt.figure()
# fig.suptitle('Obstacles Free Regression Error (%)', fontsize=16, fontweight='bold')
# fig.suptitle('Obstacles Free Regression Error (%) (using Training Error)', fontsize=16, fontweight='bold')
# fig.suptitle('Obstacles Regression Error (%)', fontsize=16, fontweight='bold')
fig.suptitle('Obstacles Regression Error (%) (using Training Error)', fontsize=16, fontweight='bold')

for i in range(robot.shape[1]):
    fig.add_subplot(231 + i)
    dim_range = np.max(robot[:, i]) - np.min(robot[:, i])
    plt.bar(angles, regression_error[i, :]/dim_range, color=vec_apply_colormap((clusters-1).astype(int)), edgecolor='none')
    plt.xlabel('Angle')
    plt.ylabel('Error')
    plt.xlim([0, 360])
    plt.ylim([0, 1])

###################################################

regression_normalized_error = np.sqrt(np.sum(np.power(regression_error, 2), axis=0))
fig = plt.figure()
# fig.suptitle('Obstacles Free Regression Normalized Error', fontsize=16, fontweight='bold')
# fig.suptitle('Obstacles Free Regression Normalized Error (using Training Error)', fontsize=16, fontweight='bold')
# fig.suptitle('Obstacles Regression Normalized Error', fontsize=16, fontweight='bold')
fig.suptitle('Obstacles Regression Normalized Error (using Training Error)', fontsize=16, fontweight='bold')
fig.add_subplot(111)
plt.bar(angles, regression_normalized_error, color=vec_apply_colormap((clusters-1).astype(int)))
plt.xlabel('Angle')
plt.ylabel('Error')
plt.xlim([0, 360])
plt.ylim([0, 500])

plt.show()

# # Regression with full dataset
# fig1 = plt.figure()
# for i in range(robot.shape[1]):
#     X = np.atleast_2d(angles).T
#     y = robot[:, i]
    
#     gp = GaussianProcess(corr='squared_exponential', theta0=1e-2, thetaL=1e-4, thetaU=1e-1, nugget=0.0001)
#     gp.fit(X, y)
    
#     x = np.atleast_2d(np.linspace(0, 2*np.pi, 1000)).T
#     y_pred, sigma2_pred = gp.predict(x, eval_MSE=True)
#     sigma = np.sqrt(sigma2_pred)
    
#     # clf = SVR(kernel='rbf', C=1e5, gamma=0.1)
#     # clf.fit(X, y)     
#     # x = np.atleast_2d(np.linspace(0, 2*np.pi, 1000)).T
#     # y_pred = clf.predict(x)

#     # krr = KernelRidge(alpha=0.001, kernel='rbf', gamma=0.1)
#     # krr.fit(X, y)
#     # x = np.atleast_2d(np.linspace(0, 2*np.pi, 1000)).T
#     # y_pred = krr.predict(x)

        
#     ax = fig1.add_subplot(231 + i)
#     plt.scatter(X, y)
#     plt.plot(x, y_pred)
#     # plt.fill(np.concatenate([x, x[::-1]]), np.concatenate([y_pred - 1.9600 * sigma, (y_pred + 1.9600 * sigma)[::-1]]), alpha=.5, 
#fc='b', ec='None')
#     plt.axis('tight')
#     plt.title('Robot Full Dim ' + str(i+1))