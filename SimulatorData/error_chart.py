import matplotlib.pyplot as plt
import numpy as np

# errors_log = np.genfromtxt("Obstacle_Free/training_error.csv", delimiter=",")
# errors_log = np.genfromtxt("Obstacle_Normal/training_error.csv", delimiter=",")
errors_log = np.genfromtxt("Obstacle_New/training_error.csv", delimiter=",")

# errors_log = np.genfromtxt("Obstacle_Free_Subspace_Regression/training_error.csv", delimiter=",")
# errors_log = np.genfromtxt("Obstacle_Free_Subspace_Regression_Error/training_error.csv", delimiter=",")
# errors_log = np.genfromtxt("Obstacle_Subspace_Regression/training_error.csv", delimiter=",")
# errors_log = np.genfromtxt("Obstacle_Subspace_Regression_Error/training_error.csv", delimiter=",")

angles = errors_log[:, 0]
errors = errors_log[:, 1]

fig = plt.figure()
# fig.suptitle('Training Error (Obstacles Free)', fontsize=16, fontweight='bold')
fig.suptitle('Training Error (Obstacles)', fontsize=16, fontweight='bold')

# fig.suptitle('Training Error (Obstacles Free Regression)', fontsize=16, fontweight='bold')
# fig.suptitle('Training Error (Obstacles Free Regression with Error)', fontsize=16, fontweight='bold')
# fig.suptitle('Training Error (Obstacles Regression)', fontsize=16, fontweight='bold')
# fig.suptitle('Training Error (Obstacles Regression with Error)', fontsize=16, fontweight='bold')

fig.add_subplot(111)
# plt.bar(angles, errors, color='red', edgecolor='none')
plt.bar(angles, errors, color='red')
plt.xlabel('Angle')
plt.ylabel('Error')
plt.xlim([0, 360])
plt.ylim([0, 250])

plt.show()

# # Normal Obstacles
# _, ax_n = plt.subplots()

# errors = np.genfromtxt("Obstacle_Normal/obstacle_error.csv", delimiter=",")
# angles = np.array(range(len(errors)))

# rects = ax_n.bar(angles, errors, color='r')

# ax_n.set_xlabel('Angle')
# ax_n.set_ylabel('Error')
# ax_n.set_title('Training Error Plot (Obstacles Normal)')

# plt.xlim([0, 360])
# plt.ylim([0, 150])

# # Reverse Obstacles
# _, ax_r = plt.subplots()

# errors = np.genfromtxt("Obstacle_Reverse/obstacle_reverse_error.csv", delimiter=",")
# angles = np.array(range(len(errors)))

# rects = ax_r.bar(angles, errors, color='r')

# ax_r.set_xlabel('Angle')
# ax_r.set_ylabel('Error')
# ax_r.set_title('Training Error Plot (Obstacles Reverse)')

# plt.xlim([0, 360])
# plt.ylim([0, 150])

# # Free Obstacles
# _, ax_f = plt.subplots()

# errors = np.genfromtxt("Obstacle_Free/obstacle_free_error.csv", delimiter=",")
# angles = np.array(range(len(errors)))

# rects = ax_f.bar(angles, errors, color='r')

# ax_f.set_xlabel('Angle')
# ax_f.set_ylabel('Error')
# ax_f.set_title('Training Error Plot (Obstacles Free)')

# plt.xlim([0, 360])
# plt.ylim([0, 150])

# # Subspace Regression Obstacles
# _, ax_s = plt.subplots()

# errors = np.genfromtxt("Obstacle_Subspace_Regression/obstacle_subspaceregression_error.csv", delimiter=",")
# angles = np.array(range(len(errors)))

# rects = ax_s.bar(angles, errors, color='r')

# ax_s.set_xlabel('Angle')
# ax_s.set_ylabel('Error')
# ax_s.set_title('Training Error Plot (Obstacles Normal Subspace Regression)')

# plt.xlim([0, 360])
# plt.ylim([0, 150])

# # Subspace Regression with Error Obstacles
# _, ax_e = plt.subplots()

# errors = np.genfromtxt("Obstacle_Subspace_Regression_Error/obstacle_subspace_regression_error.csv", delimiter=",")
# angles = np.array(range(len(errors)))

# rects = ax_e.bar(angles, errors, color='r')

# ax_e.set_xlabel('Angle')
# ax_e.set_ylabel('Error')
# ax_e.set_title('Training Error Plot (Obstacles Normal Subspace Regression Error)')

# plt.xlim([0, 360])
# plt.ylim([0, 150])

# # Subspace Regression with Error Obstacles Free
# _, ax_ef = plt.subplots()

# errors = np.genfromtxt("Obstacle_Free_Subspace_Regression_Error/obstaclefree_subspace_regression_error.csv", delimiter=",")
# angles = np.array(range(len(errors)))

# rects = ax_ef.bar(angles, errors, color='r')

# ax_ef.set_xlabel('Angle')
# ax_ef.set_ylabel('Error')
# ax_ef.set_title('Training Error Plot (Obstacles Free Subspace Regression Error)')

# plt.xlim([0, 360])
# plt.ylim([0, 150])

# # Relative Error of Regression without Obstacles
# _, ax_rf = plt.subplots()

# tra_errors = np.genfromtxt("Obstacle_Free/obstacle_free_error.csv", delimiter=",")
# reg_errors = np.genfromtxt("Obstacle_Free_Subspace_Regression_Error/obstaclefree_subspace_regression_regerror.csv", delimiter=",")
# errors = reg_errors/tra_errors
# angles = np.array(range(len(errors)))

# rects = ax_rf.bar(angles, errors, color='r')

# ax_rf.set_xlabel('Angle')
# ax_rf.set_ylabel('Error')
# ax_rf.set_title('Relative Error Plot (Obstacles Free Subspace Regression Error)')

# plt.xlim([0, 360])
# plt.ylim([0, 150])


# # Relative Error of Regression with Obstacles
# _, ax_ro = plt.subplots()

# tra_errors = np.genfromtxt("Obstacle_Normal/obstacle_error.csv", delimiter=",")
# reg_errors = np.genfromtxt("Obstacle_Subspace_Regression_Error/obstacle_subspace_regression_regerror.csv", delimiter=",")
# errors = reg_errors/tra_errors
# angles = np.array(range(len(errors)))

# rects = ax_ro.bar(angles, errors, color='r')

# ax_ro.set_xlabel('Angle')
# ax_ro.set_ylabel('Error')
# ax_ro.set_title('Relative Error Plot (Obstacles Normal Subspace Regression Error)')

# plt.xlim([0, 360])
# plt.ylim([0, 150])

# # Plotting...
# plt.show()