import matplotlib.pyplot as plt
import numpy as np

colormap = ['red', 'blue', 'yellow', 'green', 'cyan', 'pink', 'gray', 'orange', 'brown', 'violet']

def apply_colormap(c):
    return colormap[c % len(colormap)]
vec_apply_colormap = np.vectorize(apply_colormap)

fig = plt.figure()
fig.suptitle('Training Error of the Random Region - Multiple Tests', fontsize=16, fontweight='bold')
for i in range(1, 11):
	if i < 10:
		filename = "RandRegion_Test/obstacle_105_130_test0" + str(i) + "/error.csv"
	else:
		filename = "RandRegion_Test/obstacle_105_130_test10/error.csv"
	dataset = np.genfromtxt("Obstacle_Normal/obstacle_data_8_clusters.csv", delimiter=",")[104:130]
	err_log = np.genfromtxt(filename, delimiter=",")

	clters = dataset[:, 0]
	angles = err_log[:, 0]
	errors = err_log[:, 1]

	fig.add_subplot(3, 4, i)
	handlers = []
	averages = []
	for i in np.unique(clters.astype(int)):
	 	handlers.append(plt.bar(angles[clters == i], errors[clters == i], color=apply_colormap(i-1)))
	 	averages.append(np.mean(errors[clters == i]))
	plt.legend(handlers, averages, fontsize=11, loc=2)
	plt.xlim([105, 131])
	plt.ylim([0, 250])

plt.show()