import matplotlib.pyplot as plt
import numpy as np

colormap = ['red', 'blue', 'yellow', 'green', 'cyan', 'pink', 'gray', 'orange', 'brown', 'violet']

def apply_colormap(c):
    return colormap[c % len(colormap)]
vec_apply_colormap = np.vectorize(apply_colormap)

dataset = np.genfromtxt("Obstacle_Normal/obstacle_data_8_clusters.csv", delimiter=",")
err_log = np.genfromtxt("Obstacle_Subspace_Regression_Error/Z-Score_Test/RegressionFiles_e-5/error.csv", delimiter=",")

clters = dataset[:, 0]
angles = err_log[:, 0]
errors = err_log[:, 1]

fig = plt.figure()
fig.suptitle('Training Error (Obstacles Regression with Z-Score multiplied by e-5)', fontsize=16, fontweight='bold')

fig.add_subplot(111)
handlers = []
averages = []
for i in range(np.max(clters.astype(int))):
	handlers.append(plt.bar(angles[clters == i+1], errors[clters == i+1], color=apply_colormap(i)))
	averages.append(np.mean(errors[clters == i+1]))
plt.legend(handlers, averages)
plt.xlabel('Angle')
plt.ylabel('Error')
plt.xlim([0, 360])
plt.ylim([0, 250])

plt.show()