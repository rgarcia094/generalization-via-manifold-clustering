import matplotlib.pyplot as plt
import numpy as np

original = np.genfromtxt("Obstacle_Free/training_error.csv", delimiter=",")
# original = np.genfromtxt("Obstacle_Normal/training_error.csv", delimiter=",")

regression = np.genfromtxt("Obstacle_Free_Subspace_Regression/training_error.csv", delimiter=",")
# regression = np.genfromtxt("Obstacle_Free_Subspace_Regression_Error/training_error.csv", delimiter=",")
# regression = np.genfromtxt("Obstacle_Subspace_Regression/training_error.csv", delimiter=",")
# regression = np.genfromtxt("Obstacle_Subspace_Regression_Error/training_error.csv", delimiter=",")

angles = original[:, 0]

original_errors = original[:, 1]
regression_errors = regression[regression[:, 0] == regression[:, 0].astype(int), 1]

fig = plt.figure()
fig.suptitle('Training Error Comparison (Obstacles Free Regression)', fontsize=16, fontweight='bold')
# fig.suptitle('Training Error Comparison (Obstacles Free Regression with Error)', fontsize=16, fontweight='bold')
# fig.suptitle('Training Error Comparison (Obstacles Regression)', fontsize=16, fontweight='bold')
# fig.suptitle('Training Error Comparison (Obstacles Regression with Error)', fontsize=16, fontweight='bold')

fig.add_subplot(111)
regression_plot = plt.bar(angles, regression_errors, color='blue')
original_plot = plt.bar(angles, original_errors, color='red')
plt.xlabel('Angle')
plt.ylabel('Error')
plt.legend((original_plot[0], regression_plot[0]), ('Original', 'Regression'))
plt.xlim([0, 362])
plt.ylim([0, 250])

plt.show()