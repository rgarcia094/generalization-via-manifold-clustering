import matplotlib.pyplot as plt
import numpy as np

colormap = ['red', 'blue', 'yellow', 'green', 'cyan', 'pink', 'gray', 'orange', 'brown', 'violet']

def apply_colormap(c):
    return colormap[c % len(colormap)]
vec_apply_colormap = np.vectorize(apply_colormap)

dataset = np.genfromtxt("Obstacle_Normal/obstacle_data_8_clusters.csv", delimiter=",")
err_log = np.genfromtxt("Obstacle_Normal/training_error.csv", delimiter=",")

clusters = dataset[:, 0]
dataset = dataset[:, 2:]
angles = err_log[:, 0]
errors = err_log[:, 1]

d_errors = np.zeros(359)
d_params = np.zeros((359, 6))
for i in range(359):
	d_errors[i] = abs(errors[i] - errors[i+1])
	for j in range(6):
		d_params[i, j] = abs(dataset[i, j] - dataset[i+1, j])
d_errors = d_errors[d_errors.argsort()]
d_params = d_params[d_errors.argsort(), :]

fig = plt.figure()
# TODO:  fig.suptitle('Training Error (Obstacles Regression with Z-Score multiplied by e-5)', fontsize=16, fontweight='bold')

fig.add_subplot(121)
plt.bar(angles[1:], d_errors, color='red')
plt.xlabel('Angle')
plt.ylabel('Error')
plt.xlim([0, 362])
plt.ylim([0, 250])

fig.add_subplot(122)
plt.bar(angles[1:], d_params[:, 5], color='blue')
plt.xlabel('Angle')
plt.ylabel('Error')
plt.xlim([0, 362])
plt.ylim([0, 250])


# handlers = []
# averages = []
# for i in range(np.max(clters.astype(int))):
# 	handlers.append(plt.bar(angles[clters == i+1], errors[clters == i+1], color=apply_colormap(i)))
# 	averages.append(np.mean(errors[clters == i+1]))
# plt.legend(handlers, averages)

plt.show()