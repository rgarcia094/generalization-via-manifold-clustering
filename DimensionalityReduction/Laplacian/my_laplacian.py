import numpy as np
import matplotlib.pyplot as plt

from scipy import sparse
from sklearn.utils.graph import graph_laplacian
from sklearn import manifold, datasets
from sklearn.neighbors import kneighbors_graph
from sklearn.utils.arpack import eigsh
from mpl_toolkits.mplot3d import Axes3D
Axes3D


def _set_diag(laplacian, value):
    n_nodes = laplacian.shape[0]
    if not sparse.isspmatrix(laplacian):
        laplacian.flat[::n_nodes + 1] = value
    else:
        laplacian = laplacian.tocoo()
        diag_idx = (laplacian.row == laplacian.col)
        laplacian.data[diag_idx] = value
        n_diags = np.unique(laplacian.row - laplacian.col).size
        if n_diags <= 7:
            laplacian = laplacian.todia()
        else:
            laplacian = laplacian.tocsr()
    return laplacian

def _deterministic_vector_sign_flip(u):
    max_abs_rows = np.argmax(np.abs(u), axis=1)
    signs = np.sign(u[range(u.shape[0]), max_abs_rows])
    u *= signs[:, np.newaxis]
    return u

X, color = datasets.samples_generator.make_swiss_roll(n_samples=1500)
n_samples, n_features = X.shape
n_neighbors = 100

# Finds suitable neighbors number
n_neighbors = max(int(X.shape[0] / 10), 1);

# Calculate nearest neighbors weighted graph
affinity_matrix = kneighbors_graph(X, n_neighbors)
affinity_matrix = 0.5*(affinity_matrix + affinity_matrix.T)

n_nodes = affinity_matrix.shape[0]
n_components = 3 # Discard first component later

# Create laplacian matrix and set its diagonal
laplacian, dd = graph_laplacian(affinity_matrix, normed=True, return_diag=True)

laplacian = _set_diag(laplacian, 1)

try:
    # Retrieve eigenvector from the laplacian matrix
	laplacian *= -1
	lambdas, diffusion_map = eigsh(laplacian, k=n_components, sigma=1.0, which='LM', tol=0.0)
	embedding = diffusion_map.T[n_components::-1] * dd
except RuntimeError:
	laplacian *= -1

embedding = _deterministic_vector_sign_flip(embedding)

# Discard constant eigenvalue
X_lle = embedding[1:n_components].T

# Implementing the sklearn laplacian
clf = manifold.SpectralEmbedding()
X_lap = clf.fit_transform(X)

# Plot result
fig = plt.figure()
try:
    ax = fig.add_subplot(311, projection='3d')
    ax.scatter(X[:, 0], X[:, 1], X[:, 2], c=color, cmap=plt.cm.Spectral)
except:
    ax = fig.add_subplot(311)
    ax.scatter(X[:, 0], X[:, 2], c=color, cmap=plt.cm.Spectral)
ax.set_title("Original data")

ax = fig.add_subplot(312)
ax.scatter(X_lap[:, 0], X_lap[:, 1], c=color, cmap=plt.cm.Spectral)
ax.set_title("Sklearn Projection")

ax = fig.add_subplot(313)
ax.scatter(X_lle[:, 0], X_lle[:, 1], c=color, cmap=plt.cm.Spectral)
ax.set_title("My Projection")

plt.axis('tight')
plt.xticks([]), plt.yticks([])
plt.show()
