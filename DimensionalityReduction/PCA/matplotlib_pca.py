import numpy as np

from matplotlib.mlab import PCA as mlabPCA

from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits.mplot3d import proj3d
from matplotlib.patches import FancyArrowPatch

if __name__ == '__main__':
	# Random seed for consistency
	np.random.seed(234234782384239784)

	# Create first class of data values
	mu_vec1 = np.array([0,0,0])
	cov_mat1 = np.array([[1,0,0],[0,1,0],[0,0,1]])
	class1_sample = np.random.multivariate_normal(mu_vec1, cov_mat1, 20).T
	assert class1_sample.shape == (3,20), "The matrix has not the dimensions 3x20"

	# Create second class of data values
	mu_vec2 = np.array([1,1,1])
	cov_mat2 = np.array([[1,0,0],[0,1,0],[0,0,1]])
	class2_sample = np.random.multivariate_normal(mu_vec2, cov_mat2, 20).T
	assert class1_sample.shape == (3,20), "The matrix has not the dimensions 3x20"

	# Plot the random 3-dimensional data
	"""fig = plt.figure(figsize=(8,8))
	ax = fig.add_subplot(111, projection='3d')
	plt.rcParams['legend.fontsize'] = 10   
	ax.plot(class1_sample[0,:], class1_sample[1,:], class1_sample[2,:], 'o', markersize=8, color='blue', alpha=0.5, label='class1')
	ax.plot(class2_sample[0,:], class2_sample[1,:], class2_sample[2,:], '^', markersize=8, alpha=0.5, color='red', label='class2')

	plt.title('Samples for class 1 and class 2')
	ax.legend(loc='upper right')

	plt.show()"""

	# Concatenate both classes of data
	all_samples = np.concatenate((class1_sample, class2_sample), axis=1)
	assert all_samples.shape == (3,40), "The matrix has not the dimensions 3x40"

	# Apply the PCA algorithm. This version scales the variables to unit variance prior to calculating the covariance matrices
	# Scaling make senses when the variables were measured in different types of unit (inches and cm, for example).
	mlab_pca = mlabPCA(all_samples.T)
	print('PC axes in terms of the measurement axes scaled by the standard deviations:\n', mlab_pca.Wt)

	plt.plot(mlab_pca.Y[0:20,0],mlab_pca.Y[0:20,1], 'o', markersize=7, color='blue', alpha=0.5, label='class1')
	plt.plot(mlab_pca.Y[20:40,0], mlab_pca.Y[20:40,1], '^', markersize=7, color='red', alpha=0.5, label='class2')

	plt.xlabel('x_values')
	plt.ylabel('y_values')
	plt.xlim([-4,4])
	plt.ylim([-4,4])
	plt.legend()
	plt.title('Transformed samples with class labels from matplotlib.mlab.PCA()')

	plt.show()

