# Authors: Fabian Pedregosa <fabian.pedregosa@inria.fr>
#          Olivier Grisel <olivier.grisel@ensta.org>
#          Mathieu Blondel <mathieu@mblondel.org>
# License: BSD, (C) INRIA 2011
import numpy as np
import matplotlib.pyplot as plt

from sklearn import datasets, decomposition
from sklearn.decomposition import PCA as sklearnPCA
from mpl_toolkits.mplot3d import Axes3D
Axes3D

X, color = datasets.samples_generator.make_swiss_roll(n_samples=1500)
n_samples, n_features = X.shape
n_neighbors = 30

# Apply the PCA function
# sklearn_pca = sklearnPCA(n_components=2)
# sklearn_transf = sklearn_pca.fit_transform(X)
# sklearn_transf = sklearn_transf*(-1)
X_pca = decomposition.RandomizedPCA(n_components=2).fit_transform(X)

# Plot result
fig = plt.figure()
try:
    # compatibility matplotlib < 1.0
    ax = fig.add_subplot(211, projection='3d')
    ax.scatter(X[:, 0], X[:, 1], X[:, 2], c=color, cmap=plt.cm.Spectral)
except:
    ax = fig.add_subplot(211)
    ax.scatter(X[:, 0], X[:, 2], c=color, cmap=plt.cm.Spectral)

ax.set_title("Original data")
ax = fig.add_subplot(212)
ax.scatter(X_pca[:, 0], X_pca[:, 1], c=color, cmap=plt.cm.Spectral)
plt.axis('tight')
plt.xticks([]), plt.yticks([])
plt.title('Projected data')
plt.show()



#####################################################################################################################################

"""import numpy as np

from sklearn.decomposition import PCA as sklearnPCA
from sklearn import datasets

from pylab import scatter,text, show,cm, figure
from pylab import subplot, imshow, NullLocator

from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits.mplot3d import proj3d
from matplotlib.patches import FancyArrowPatch

if __name__ == '__main__':
	digits = datasets.load_digits(n_class=5)
	X = digits.data
	color = digits.target

	# Apply the PCA function
	sklearn_pca = sklearnPCA(n_components=2)
	sklearn_transf = sklearn_pca.fit_transform(X)
	sklearn_transf = sklearn_transf * (-1)

	# plotting the result
	figure(2)
	scatter(sklearn_transf[:,0], sklearn_transf[:,1], c='k', alpha=0.3, s=10)
	for i in range(sklearn_transf.shape[0]):
		text(sklearn_transf[i,0], sklearn_transf[i,1], str(color[i]), color=cm.Dark2(color[i]/5.), fontdict={'weight':'bold', 'size':11})
	show()"""

#####################################################################################################################################

"""if __name__ == '__main__':
	# Random seed for consistency
	np.random.seed(234234782384239784)

	# Create first class of data values
	mu_vec1 = np.array([0,0,0])
	cov_mat1 = np.array([[1,0,0],[0,1,0],[0,0,1]])
	class1_sample = np.random.multivariate_normal(mu_vec1, cov_mat1, 20).T
	assert class1_sample.shape == (3,20), "The matrix has not the dimensions 3x20"

	# Create second class of data values
	mu_vec2 = np.array([1,1,1])
	cov_mat2 = np.array([[1,0,0],[0,1,0],[0,0,1]])
	class2_sample = np.random.multivariate_normal(mu_vec2, cov_mat2, 20).T
	assert class1_sample.shape == (3,20), "The matrix has not the dimensions 3x20"

	# Plot the random 3-dimensional data
	#fig = plt.figure(figsize=(8,8))
	#ax = fig.add_subplot(111, projection='3d')
	#plt.rcParams['legend.fontsize'] = 10   
	#ax.plot(class1_sample[0,:], class1_sample[1,:], class1_sample[2,:], 'o', markersize=8, color='blue', alpha=0.5, label='class1')
	#ax.plot(class2_sample[0,:], class2_sample[1,:], class2_sample[2,:], '^', markersize=8, alpha=0.5, color='red', label='class2')

	#plt.title('Samples for class 1 and class 2')
	#ax.legend(loc='upper right')

	#plt.show()

	# Concatenate both classes of data
	all_samples = np.concatenate((class1_sample, class2_sample), axis=1)
	assert all_samples.shape == (3,40), "The matrix has not the dimensions 3x40"

	# Apply the PCA function
	sklearn_pca = sklearnPCA(n_components=2)
	sklearn_transf = sklearn_pca.fit_transform(all_samples.T)
	sklearn_transf = sklearn_transf * (-1)

	# Plot the results
	plt.plot(sklearn_transf[0:20,0],sklearn_transf[0:20,1], 'o', markersize=7, color='blue', alpha=0.5, label='class1')
	plt.plot(sklearn_transf[20:40,0], sklearn_transf[20:40,1], '^', markersize=7, color='red', alpha=0.5, label='class2')

	plt.xlabel('x_values')
	plt.ylabel('y_values')
	plt.xlim([-4,4])
	plt.ylim([-4,4])
	plt.legend()
	plt.title('Transformed samples with class labels from matplotlib.mlab.PCA()')

	plt.show()"""
