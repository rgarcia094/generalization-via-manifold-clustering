import numpy as np
import matplotlib.pyplot as plt

from sklearn import datasets, decomposition
from mpl_toolkits.mplot3d import Axes3D

Axes3D

if __name__ == '__main__':
	X, color = datasets.samples_generator.make_s_curve(n_samples=1500)
	# X = np.array(range(50)).reshape((50, 1)).astype(float)

	# t_lin = np.random.randn(1, 3)
	# X = np.dot(X_ld, t_lin)

	# t_lin = np.random.randn(3, 5)
	# X_hd = np.dot(X, t_lin)
	# print X_hd.shape

	# t_inv = np.linalg.pinv(t_lin)
	# print t_inv.shape

	# X_ld = np.dot(X_hd, t_inv)
	# print X_ld.shape

	# print X
	# print X_ld

	n_samples, n_features = X.shape
	n_neighbors = 30

	# Random seed for consistency
	np.random.seed(100)

	# Center the data in 0
	X -= np.mean(X, axis=0)

	# Calculate mean for each dimension
	mean_vector = np.array([np.mean(X[:, i]) for i in range(len(X.T))])
	
	# Computing the Scatter Matrix
	scatter_matrix = np.zeros((len(X.T), len(X.T)))
	for i in range(X.shape[0]):
	    scatter_matrix += (X[i, :].reshape(1, len(X.T)).T - mean_vector).dot((X[i, :].reshape(1, len(X.T)).T - mean_vector).T)

	# Retrieve Eigenvectors and Eigenvalues from the scatter matrix
	eig_val_sc, eig_vec_sc = np.linalg.eig(scatter_matrix)

	# Make a list of (eigenvalue, eigenvector) tuples
	eig_pairs = [(np.abs(eig_val_sc[i]), eig_vec_sc[:,i]) for i in range(len(eig_val_sc))]

	# Sort the (eigenvalue, eigenvector) tuples from high to low
	eig_pairs.sort()
	eig_pairs.reverse()

	# Removing the higher dimensions eigenpairs
	matrix_w = np.hstack((eig_pairs[0][1].reshape(len(X.T), 1), eig_pairs[1][1].reshape(len(X.T), 1)))

	# Applying the reduction matrix in the original data.
	X_pca = X.dot(matrix_w)
	
	# # Executing SKLEARN PCA
	# X_pca_sklearn = decomposition.RandomizedPCA(n_components=2).fit_transform(X)

	# Plot result
	fig = plt.figure()
	ax = fig.add_subplot(311, projection='3d')
	ax.scatter(X[:, 0], X[:, 1], X[:, 2], c=color, cmap=plt.cm.Spectral)

	# ax = fig.add_subplot(312)
	# ax.scatter(X_pca_sklearn[:, 0], X_pca_sklearn[:, 1], c=color, cmap=plt.cm.Spectral)
	# ax.set_title("Sklearn Projection")

	ax = fig.add_subplot(312)
	ax.scatter(X_pca[:, 0], X_pca[:, 1], c=color, cmap=plt.cm.Spectral)

	t_inv = np.linalg.pinv(matrix_w)

	X_hd = np.dot(X_pca, t_inv)

	ax = fig.add_subplot(313, projection='3d')
	ax.scatter(X_hd[:, 0], X_hd[:, 1], X_hd[:, 2], c=color, cmap=plt.cm.Spectral)

	# plt.axis('tight')
	# plt.xticks([]), plt.yticks([])
	# plt.title('My Projection')
	plt.show()

##################################################################################################################################

"""import numpy as np

from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits.mplot3d import proj3d
from matplotlib.patches import FancyArrowPatch

class Arrow3D(FancyArrowPatch):
    def __init__(self, xs, ys, zs, *args, **kwargs):
        FancyArrowPatch.__init__(self, (0,0), (0,0), *args, **kwargs)
        self._verts3d = xs, ys, zs

    def draw(self, renderer):
        xs3d, ys3d, zs3d = self._verts3d
        xs, ys, zs = proj3d.proj_transform(xs3d, ys3d, zs3d, renderer.M)
        self.set_positions((xs[0],ys[0]),(xs[1],ys[1]))
        FancyArrowPatch.draw(self, renderer)

if __name__ == '__main__':
	# Random seed for consistency
	np.random.seed(234234782384239784)

	# Create first class of data values
	mu_vec1 = np.array([0,0,0])
	cov_mat1 = np.array([[1,0,0],[0,1,0],[0,0,1]])
	class1_sample = np.random.multivariate_normal(mu_vec1, cov_mat1, 20).T
	assert class1_sample.shape == (3,20), "The matrix has not the dimensions 3x20"

	# Create second class of data values
	mu_vec2 = np.array([1,1,1])
	cov_mat2 = np.array([[1,0,0],[0,1,0],[0,0,1]])
	class2_sample = np.random.multivariate_normal(mu_vec2, cov_mat2, 20).T
	assert class1_sample.shape == (3,20), "The matrix has not the dimensions 3x20"

	# Plot the random 3-dimensional data
	fig = plt.figure(figsize=(8,8))
	ax = fig.add_subplot(111, projection='3d')
	plt.rcParams['legend.fontsize'] = 10   
	ax.plot(class1_sample[0,:], class1_sample[1,:], class1_sample[2,:], 'o', markersize=8, color='blue', alpha=0.5, label='class1')
	ax.plot(class2_sample[0,:], class2_sample[1,:], class2_sample[2,:], '^', markersize=8, alpha=0.5, color='red', label='class2')

	plt.title('Samples for class 1 and class 2')
	ax.legend(loc='upper right')

	plt.show()

	# Concatenate both classes of data
	all_samples = np.concatenate((class1_sample, class2_sample), axis=1)
	assert all_samples.shape == (3,40), "The matrix has not the dimensions 3x40"

	# Calculate mean for each dimension
	mean_x = np.mean(all_samples[0,:])
	mean_y = np.mean(all_samples[1,:])
	mean_z = np.mean(all_samples[2,:])

	mean_vector = np.array([[mean_x],[mean_y],[mean_z]])

	# Computing the Scatter Matrix
	scatter_matrix = np.zeros((3,3))
	for i in range(all_samples.shape[1]):
	    scatter_matrix += (all_samples[:,i].reshape(3,1) - mean_vector).dot((all_samples[:,i].reshape(3,1) - mean_vector).T)

	# Retrieve Eigenvectors and Eigenvalues from the scatter matrix
	eig_val_sc, eig_vec_sc = np.linalg.eig(scatter_matrix)
	for i in range(len(eig_val_sc)):
		eigv = eig_vec_sc[:,i].reshape(1,3).T
		np.testing.assert_array_almost_equal(scatter_matrix.dot(eigv), eig_val_sc[i]*eigv, decimal=6, err_msg='Error', verbose=True)

	# Plotting the Eigenvectors
	fig = plt.figure(figsize=(7,7))
	ax = fig.add_subplot(111, projection='3d')

	ax.plot(all_samples[0,:], all_samples[1,:], all_samples[2,:], 'o', markersize=8, color='green', alpha=0.2)
	ax.plot([mean_x], [mean_y], [mean_z], 'o', markersize=10, color='red', alpha=0.5)
	for v in eig_vec_sc.T:
	    a = Arrow3D([mean_x, v[0]], [mean_y, v[1]], [mean_z, v[2]], mutation_scale=20, lw=3, arrowstyle="-|>", color="r")
	    ax.add_artist(a)
	ax.set_xlabel('x_values')
	ax.set_ylabel('y_values')
	ax.set_zlabel('z_values')

	plt.title('Eigenvectors')

	plt.show()

	# Make a list of (eigenvalue, eigenvector) tuples
	eig_pairs = [(np.abs(eig_val_sc[i]), eig_vec_sc[:,i]) for i in range(len(eig_val_sc))]

	# Sort the (eigenvalue, eigenvector) tuples from high to low
	eig_pairs.sort()
	eig_pairs.reverse()

	# Removing the eigen pair of the third dimension.
	matrix_w = np.hstack((eig_pairs[0][1].reshape(3,1), eig_pairs[1][1].reshape(3,1)))

	# Applying the reduction matrix in the original data.
	transformed = matrix_w.T.dot(all_samples)
	assert transformed.shape == (2,40), "The matrix is not 2x40 dimensional."

	# Plotting the result
	plt.plot(transformed[0,0:20], transformed[1,0:20], 'o', markersize=7, color='blue', alpha=0.5, label='class1')
	plt.plot(transformed[0,20:40], transformed[1,20:40], '^', markersize=7, color='red', alpha=0.5, label='class2')
	plt.xlim([-4,4])
	plt.ylim([-4,4])
	plt.xlabel('x_values')
	plt.ylabel('y_values')
	plt.legend()
	plt.title('Transformed samples with class labels')

	plt.show()"""
