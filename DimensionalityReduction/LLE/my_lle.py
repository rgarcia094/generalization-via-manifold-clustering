import numpy as np
import matplotlib.pyplot as plt

from sklearn import manifold, datasets
from sklearn.neighbors import NearestNeighbors
from sklearn.manifold.locally_linear import barycenter_kneighbors_graph
from sklearn.utils import check_random_state
from sklearn.utils.arpack import eigsh
from scipy.sparse import eye
from mpl_toolkits.mplot3d import Axes3D
Axes3D

X, color = datasets.samples_generator.make_swiss_roll(n_samples=1500)
n_samples, n_features = X.shape
n_neighbors = 100

# Calculate the k-nearest neighbors of each node.
nbrs_ = NearestNeighbors(n_neighbors=n_neighbors, algorithm="auto")
nbrs_.fit(X)

nbrs = NearestNeighbors(n_neighbors=n_neighbors + 1)
nbrs.fit(nbrs_)
nbrs_ = nbrs._fit_X

# Create Weighted graph with distances
W = barycenter_kneighbors_graph(nbrs, n_neighbors=n_neighbors, reg=1e-3)

# Create sparse matrix
M = eye(*W.shape, format=W.format) - W
M = (M.T * M).tocsr()

random_state = None
random_state = check_random_state(random_state)
v0 = random_state.rand(M.shape[0])
try:
    # Find botton eigenvectors
	eigen_values, eigen_vectors = eigsh(M, 3, sigma=0.0, tol=1e-6, maxiter=100, v0=v0)
except RuntimeError as msg:
	raise ValueError(msg)

# # Discard constant first eigenvector
# X_lle = eigen_vectors[:, 1:]

# clf = manifold.LocallyLinearEmbedding(n_neighbors, n_components=2, method='standard')
# X_sk_lle = clf.fit_transform(X)

# # Plot result
# fig = plt.figure()
# try:
#     ax = fig.add_subplot(311, projection='3d')
#     ax.scatter(X[:, 0], X[:, 1], X[:, 2], c=color, cmap=plt.cm.Spectral)
# except:
#     ax = fig.add_subplot(311)
#     ax.scatter(X[:, 0], X[:, 2], c=color, cmap=plt.cm.Spectral)
# ax.set_title("Original data")

# ax = fig.add_subplot(312)
# ax.scatter(X_sk_lle[:, 0], X_sk_lle[:, 1], c=color, cmap=plt.cm.Spectral)
# ax.set_title("Sklearn Projection")

# ax = fig.add_subplot(313)
# ax.scatter(X_lle[:, 0], X_lle[:, 1], c=color, cmap=plt.cm.Spectral)
# ax.set_title("My Projection")

# plt.axis('tight')
# plt.xticks([]), plt.yticks([])
# plt.show()

###############################################################################################################################

# return null_space(M, n_components, k_skip=1, eigen_solver=eigen_solver, tol=tol, max_iter=max_iter, random_state=random_state)

# W = barycenter_kneighbors_graph(nbrs, n_neighbors=n_neighbors, reg=reg)

# kng = kneighbors_graph(nbrs_, n_neighbors, mode="distance")

# W = np.zeros((len(X), len(X))) # W.shape = (N, k)
# for i in range(len(X)):
# 	Z = np.zeros((len(X), len(X)))
# 	for j in range(len(X)):
# 		if kng[i, j] > 0.0:
# 			Z[j, :] = X[j, :] - X[i, :]
# 	C = Z.dot(Z.T)
#  	W[i, :] = np.linalg.solve(C, np.ones((n_neighbors, 1)))[:, 0] # PROBLEMS MIGHT APPEAR HERE
#  	W[i, :] = W[i, :] / np.sum(W[i, :])
#  	# index_list = [j for j in range(len(X)) if kng[i, j] > 0.0]
#  	# Z = X[index_list[:], :] - X[i, :]
#  	# C = Z.dot(Z.T)
#  	# W[i, :] = np.linalg.solve(C, np.ones((n_neighbors, 1)))[:, 0] # PROBLEMS MIGHT APPEAR HERE
#  	# W[i, :] = W[i, :] / np.sum(W[i, :])

# id_matrix = np.identity(len(X))
# M = (id_matrix - W).T.dot((id_matrix - W))

# # M = (I-W)'*(I-W)
# print M.shape
# #print index

