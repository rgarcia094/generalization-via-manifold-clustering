import numpy as np
import matplotlib.pyplot as plt

from sklearn import manifold, datasets
from sklearn.metrics import euclidean_distances
from mpl_toolkits.mplot3d import Axes3D

Axes3D

X, color = datasets.samples_generator.make_swiss_roll(n_samples=1500)
n_samples, n_features = X.shape
n_neighbors = 30

# Random seed for consistency
seed = np.random.RandomState(seed=3)

# Create array of distances
distances = euclidean_distances(X)

# Creates the square matrix of the distances matrix 
sqr_distances = np.square(distances)

# Creates the identity matrix and ones matrix
identity_matrix = np.identity(len(distances))
ones_matrix = np.ones((len(distances), len(distances)))

# Apply double centering
j_matrix = identity_matrix - 1.0/len(distances) * ones_matrix
b_matrix = -(j_matrix.dot(sqr_distances)).dot(j_matrix)/2.0

# Retrieve Eigenvectors and Eigenvalues from the b matrix
eig_val_sc, eig_vec_sc = np.linalg.eig(b_matrix)

# Sort the (eigenvalue, eigenvector) tuples from high to low
eig_pairs = [(np.abs(eig_val_sc[i]), eig_vec_sc[:,i]) for i in range(len(eig_val_sc))]
eig_pairs.sort(key=lambda pair: pair[0])
eig_pairs.reverse()

# Removing the higher dimensions eigenpairs
matrix_w = np.hstack((eig_pairs[0][1].reshape(len(distances), 1), eig_pairs[1][1].reshape(len(distances), 1)))

# Create the eigenvalues matrix
matrix_v = [[np.sqrt(eig_val_sc[0]), 0], [0, np.sqrt(eig_val_sc[1])]]

# Calculate the resulting positions in 2-D
result_2d = matrix_w.dot(matrix_v);

# Executing SKLEARN MDS
#X1, color1 = datasets.samples_generator.make_swiss_roll(n_samples=1500)
clf = manifold.MDS(n_components=2, n_init=1, max_iter=100, dissimilarity="precomputed")
X_mds = clf.fit_transform(euclidean_distances(X))

# Plot result
fig = plt.figure()
try:
	ax = fig.add_subplot(311, projection='3d')
	ax.scatter(X[:, 0], X[:, 1], X[:, 2], c=color, cmap=plt.cm.Spectral)
except:
	ax = fig.add_subplot(311)
	ax.scatter(X[:, 0], X[:, 2], c=color, cmap=plt.cm.Spectral)
ax.set_title("Original data")

ax = fig.add_subplot(312)
ax.scatter(X_mds[:, 0], X_mds[:, 1], c=color, cmap=plt.cm.Spectral)
ax.set_title("Sklearn Projection")

ax = fig.add_subplot(313)
ax.scatter(result_2d[:, 0], result_2d[:, 1], c=color, cmap=plt.cm.Spectral)
ax.set_title("My Projection")

plt.axis('tight')
plt.xticks([]), plt.yticks([])
plt.show()

#######################################################################################################################

"""import numpy as np

from matplotlib import pyplot as plt
from matplotlib.collections import LineCollection
from mpl_toolkits.mplot3d import Axes3D

from sklearn import manifold
from sklearn.metrics import euclidean_distances
from sklearn.decomposition import PCA

def cmdscale(D):                                                                                      
# Classical multidimensional scaling (MDS)                                                  
                                                                                           
# Parameters                                                                                
# ----------                                                                                
# D : (n, n) array                                                                          
#     Symmetric distance matrix.                                                            
                                                                                           
# Returns                                                                                   
# -------                                                                                   
# Y : (n, p) array                                                                          
#     Configuration matrix. Each column represents a dimension. Only the                    
#     p dimensions corresponding to positive eigenvalues of B are returned.                 
#     Note that each dimension is only determined up to an overall sign,                    
#     corresponding to a reflection.                                                        
                                                                                           
# e : (n,) array                                                                            
#     Eigenvalues of B.                                                                     
    # Number of points                                                                        
    n = len(D)
 
    # Centering matrix                                                                        
    H = np.eye(n) - np.ones((n, n))/n
 
    # YY^T                                                                                    
    B = -H.dot(D**2).dot(H)/2
 
    # Diagonalize                                                                             
    evals, evecs = np.linalg.eigh(B)
 
    # Sort by eigenvalue in descending order                                                  
    idx   = np.argsort(evals)[::-1]
    evals = evals[idx]
    evecs = evecs[:,idx]
 
    # Compute the coordinates using positive-eigenvalued components only                      
    w, = np.where(evals > 0)
    L  = np.diag(np.sqrt(evals[w]))
    V  = evecs[:,w]
    Y  = V.dot(L)
 
    return Y, evals

#use 2-d Gaussian information metric for distances
# see equation 7 from http://arxiv.org/abs/0802.2050 ("FINE" paper)
def getDistance(x,y):
    #going to define a measure here
    #print 'in getSim', x, y
    aa = x[0]-y[0]
    ab = x[1]+y[1]
    bb = x[1]-y[1]
    num = np.sqrt((aa**2+ab**2))+np.sqrt((aa**2+bb**2))
    den = np.sqrt((aa**2+ab**2))-np.sqrt((aa**2+bb**2))
    ret = np.log(num/den)
    return ret

if __name__ == '__main__':
	seed = np.random.RandomState(seed=3)

	# Create the matrix of "dissimilarities" (distances) between points
	distances = np.array([[0, 93, 82, 133], [93, 0, 52, 60], [82, 52, 0, 111], [133, 60, 111, 0]])

	# Creates the square matrix of the distances matrix 
	sqr_distances = np.square(distances)

	# Creates the identity matrix and ones matrix
	identity_matrix = np.identity(len(distances))
	ones_matrix = np.ones((4, 4))

	# Apply double centering
	j_matrix = identity_matrix - 1.0/len(distances) * ones_matrix
	b_matrix = -(j_matrix.dot(sqr_distances)).dot(j_matrix)/2.0

	# Retrieve Eigenvectors and Eigenvalues from the b matrix
	eig_val_sc, eig_vec_sc = np.linalg.eig(b_matrix)

	# Sort the (eigenvalue, eigenvector) tuples from high to low
	eig_pairs = [(np.abs(eig_val_sc[i]), eig_vec_sc[:,i]) for i in range(len(eig_val_sc))]
	eig_pairs.sort()
	eig_pairs.reverse()

	# Removing the eigen pair of the third dimension.
	matrix_w = np.hstack((eig_pairs[0][1].reshape(len(distances), 1), eig_pairs[1][1].reshape(len(distances), 1)))

	# Create the eigenvalues matrix
	matrix_v = [[np.sqrt(eig_val_sc[0]), 0], [0, np.sqrt(eig_val_sc[1])]]

	# Calculate the resulting positions in 2-D
	result_2d = matrix_w.dot(matrix_v);

	#Setup plots
	fig = plt.figure(figsize=(5*3,4.5))

	# choose a different color for each point
	colors = plt.cm.jet(np.linspace(0, 1, len(distances)))

	# plot 2d embedding
	subpl2 = fig.add_subplot(133)
	subpl2.set_autoscaley_on(False)
	subpl2.scatter(result_2d[:, 0], result_2d[:, 1],s=20, c=colors)
	plt.title('my mds')
	plt.axis('tight')

	#######################################################################################################################

	eig_vec_sc, eig_val_sc = cmdscale(distances)

	# Sort the (eigenvalue, eigenvector) tuples from high to low
	eig_pairs = [(np.abs(eig_val_sc[i]), eig_vec_sc[:,i]) for i in range(len(eig_val_sc))]
	eig_pairs.sort()
	eig_pairs.reverse()

	# Removing the eigen pair of the third dimension.
	matrix_w = np.hstack((eig_pairs[0][1].reshape(len(distances), 1), eig_pairs[1][1].reshape(len(distances), 1)))

	# Create the eigenvalues matrix
	matrix_v = [[np.sqrt(eig_val_sc[0]), 0], [0, np.sqrt(eig_val_sc[1])]]

	# Calculate the resulting positions in 2-D
	result_2d = matrix_w.dot(matrix_v);

	# plot 2d embedding
	subpl = fig.add_subplot(132)
	subpl.set_autoscaley_on(False)
	subpl.scatter(result_2d[:, 0], result_2d[:, 1],s=20, c=colors)
	plt.title('my mds 2')
	plt.axis('tight')

	#######################################################################################################################

	#make 2d embedding
	mds2 = manifold.MDS(n_components=2, max_iter=3000, eps=1e-9, random_state=seed, dissimilarity="precomputed", n_jobs=1)
	embed2d = mds2.fit(distances).embedding_

	# plot 2d embedding
	gridsubpl = fig.add_subplot(131)
	gridsubpl.scatter(embed2d[:, 0], embed2d[:, 1], s=20, c=colors)
	gridsubpl.set_xlabel('mean')
	gridsubpl.set_ylabel('standard deviation')
	plt.title('sklearn mds')
	plt.axis('tight')

	plt.show()"""

