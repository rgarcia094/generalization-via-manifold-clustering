# Authors: Fabian Pedregosa <fabian.pedregosa@inria.fr>
#          Olivier Grisel <olivier.grisel@ensta.org>
#          Mathieu Blondel <mathieu@mblondel.org>
# License: BSD, (C) INRIA 2011
from time import time

import numpy as np
import pylab as pl
import matplotlib.pyplot as plt

from matplotlib import offsetbox
from sklearn.utils.fixes import qr_economic
from sklearn import manifold, datasets, decomposition, lda
from sklearn.metrics import euclidean_distances
from mpl_toolkits.mplot3d import Axes3D
Axes3D

X, color = datasets.samples_generator.make_swiss_roll(n_samples=1500)
n_samples, n_features = X.shape
n_neighbors = 30

# Isomap projection of the digits dataset
X_iso = manifold.Isomap(n_neighbors, n_components=2).fit_transform(X)

# Plot result
fig = plt.figure()
try:
    # compatibility matplotlib < 1.0
    ax = fig.add_subplot(211, projection='3d')
    ax.scatter(X[:, 0], X[:, 1], X[:, 2], c=color, cmap=plt.cm.Spectral)
except:
    ax = fig.add_subplot(211)
    ax.scatter(X[:, 0], X[:, 2], c=color, cmap=plt.cm.Spectral)

ax.set_title("Original data")
ax = fig.add_subplot(212)
ax.scatter(X_iso[:, 0], X_iso[:, 1], c=color, cmap=plt.cm.Spectral)
plt.axis('tight')
plt.xticks([]), plt.yticks([])
plt.title('Projected data')
plt.show()