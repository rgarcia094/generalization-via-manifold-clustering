import numpy as np
import matplotlib.pyplot as plt

from mpl_toolkits.mplot3d import Axes3D
from sklearn import manifold, datasets
from sklearn.metrics import euclidean_distances
from sklearn.neighbors import BallTree, NearestNeighbors, kneighbors_graph
from scipy.sparse.csgraph import dijkstra
from sklearn.utils.graph import graph_shortest_path

Axes3D

X, color = datasets.samples_generator.make_swiss_roll(n_samples=1500)
n_samples, n_features = X.shape
n_neighbors = 100

# Random seed for consistency
np.random.seed(234234782384239784)
seed = np.random.RandomState(seed=3)

# Calculate the k-nearest neighbors of each node.
nbrs_ = NearestNeighbors(n_neighbors=n_neighbors, algorithm="auto")
nbrs_.fit(X)
kng = kneighbors_graph(nbrs_, n_neighbors, mode="distance")
# ball_tree = BallTree(X, leaf_size=2)
# distances = euclidean_distances(X)
# for i in range(len(X)):
# 	k_dist, k_ind = ball_tree.query(X[i], k=n_neighbors)
# 	for j in range(len(X)):
# 		if np.any(k_ind[:] == j):
# 			index = np.where(k_ind == j);
# 			distances[i][j] = k_dist[index[0][0]][index[1][0]]
# 		else:
# 			distances[i][j] = 0

# Apply the Dijskstra algorithm
distances = graph_shortest_path(kng, method="auto", directed=False)
# distances, predecessors = dijkstra(distances, return_predecessors=True)

# Creates the square matrix of the distances matrix 
sqr_distances = np.square(distances)

# Creates the identity matrix and ones matrix
identity_matrix = np.identity(len(distances))
ones_matrix = np.ones((len(distances), len(distances)))

# Apply double centering
j_matrix = identity_matrix - 1.0/len(distances) * ones_matrix
b_matrix = -(j_matrix.dot(sqr_distances)).dot(j_matrix)/2.0

# Retrieve Eigenvectors and Eigenvalues from the b matrix
eig_val_sc, eig_vec_sc = np.linalg.eig(b_matrix)

# Sort the (eigenvalue, eigenvector) tuples from high to low
eig_pairs = [(np.abs(eig_val_sc[i]), eig_vec_sc[:,i]) for i in range(len(eig_val_sc))]
eig_pairs.sort(key=lambda pair: pair[0])
eig_pairs.reverse()

# Removing the higher dimensions eigenpairs
matrix_w = np.hstack((eig_pairs[0][1].reshape(len(distances), 1), eig_pairs[1][1].reshape(len(distances), 1)))

# Create the eigenvalues matrix
matrix_v = [[np.sqrt(eig_val_sc[0]), 0], [0, np.sqrt(eig_val_sc[1])]]

# Calculate the resulting positions in 2-D
result_2d = matrix_w.dot(matrix_v) * (-1)

# Calculate the Distances between the projected dataset
low_dim_distances = euclidean_distances(result_2d)



# Isomap projection of the digits dataset
X_iso = manifold.Isomap(n_neighbors, n_components=2).fit_transform(X)

# Plot result
fig = plt.figure()
try:
	ax = fig.add_subplot(311, projection='3d')
	ax.scatter(X[:, 0], X[:, 1], X[:, 2], c=color, cmap=plt.cm.Spectral)
except:
	ax = fig.add_subplot(311)
	ax.scatter(X[:, 0], X[:, 2], c=color, cmap=plt.cm.Spectral)
ax.set_title("Original data")

ax = fig.add_subplot(312)
ax.scatter(X_iso[:, 0], X_iso[:, 1], c=color, cmap=plt.cm.Spectral)
ax.set_title("Sklearn Projection")

ax = fig.add_subplot(313)
ax.scatter(result_2d[:, 0], result_2d[:, 1], c=color, cmap=plt.cm.Spectral)
ax.set_title("My Projection")

plt.axis('tight')
plt.xticks([]), plt.yticks([])
plt.show()

############################################################################################################################

"""import numpy as np

from matplotlib import pyplot as plt
from matplotlib.collections import LineCollection
from mpl_toolkits.mplot3d import Axes3D

from sklearn import manifold
from sklearn.metrics import euclidean_distances
from sklearn.decomposition import PCA
from sklearn.neighbors import BallTree
from scipy.sparse.csgraph import dijkstra

def cmdscale(D):
                                                                                          
    # Classical multidimensional scaling (MDS)                                                  
                                                                                               
    # Parameters                                                                                
    # ----------                                                                                
    # D : (n, n) array                                                                          
    #     Symmetric distance matrix.                                                            
                                                                                               
    # Returns                                                                                   
    # -------                                                                                   
    # Y : (n, p) array                                                                          
    #     Configuration matrix. Each column represents a dimension. Only the                    
    #     p dimensions corresponding to positive eigenvalues of B are returned.                 
    #     Note that each dimension is only determined up to an overall sign,                    
    #     corresponding to a reflection.                                                        
                                                                                               
    # e : (n,) array                                                                            
    #     Eigenvalues of B.                                                                     
                                                                                               
    # Number of points                                                                        
    n = len(D)
 
    # Centering matrix                                                                        
    H = np.eye(n) - np.ones((n, n))/n
 
    # YY^T                                                                                    
    B = -H.dot(D**2).dot(H)/2
 
    # Diagonalize                                                                             
    evals, evecs = np.linalg.eigh(B)
 
    # Sort by eigenvalue in descending order                                                  
    idx   = np.argsort(evals)[::-1]
    evals = evals[idx]
    evecs = evecs[:,idx]
 
    # Compute the coordinates using positive-eigenvalued components only                      
    w, = np.where(evals > 0)
    L  = np.diag(np.sqrt(evals[w]))
    V  = evecs[:,w]
    Y  = V.dot(L)
 
    return Y, evals

if __name__ == '__main__':
	# Random seed for consistency
	np.random.seed(234234782384239784)
	seed = np.random.RandomState(seed=3)

	# Create first class of data values
	mu_vec1 = np.array([0,0,0])
	cov_mat1 = np.array([[1,0,0],[0,1,0],[0,0,1]])
	class1_sample = np.random.multivariate_normal(mu_vec1, cov_mat1, 20).T
	assert class1_sample.shape == (3,20), "The matrix has not the dimensions 3x20"

	# Create second class of data values
	mu_vec2 = np.array([1,1,1])
	cov_mat2 = np.array([[1,0,0],[0,1,0],[0,0,1]])
	class2_sample = np.random.multivariate_normal(mu_vec2, cov_mat2, 20).T
	assert class1_sample.shape == (3,20), "The matrix has not the dimensions 3x20"

	# Concatenate both classes of data
	all_samples = np.concatenate((class1_sample, class2_sample), axis=1)
	assert all_samples.shape == (3,40), "The matrix has not the dimensions 3x40"
	all_samples = all_samples.T

	# Calculate the 4-nearest neighbors of each node.
	ball_tree = BallTree(all_samples, leaf_size=2)
	distances = np.empty([len(all_samples), len(all_samples)])
	for i in range(len(all_samples)):
		k_dist, k_ind = ball_tree.query(all_samples[i], k=10)
		for j in range(len(all_samples)):
			if np.any(k_ind[:] == j):
				index = np.where(k_ind == j);
				distances[i][j] = k_dist[index[0][0]][index[1][0]]
			else:
				distances[i][j] = 0 # np.iinfo(np.int32).max

	# Apply the Dijskstra algorithm
	distances, predecessors = dijkstra(distances, return_predecessors=True)

	# STARTING MDS PART

	# Creates the square matrix of the distances matrix 
	sqr_distances = np.square(distances)

	# Creates the identity matrix and ones matrix
	identity_matrix = np.identity(len(distances))
	ones_matrix = np.ones((len(distances), len(distances)))

	# Apply double centering
	j_matrix = identity_matrix - 1.0/len(distances) * ones_matrix
	b_matrix = -(j_matrix.dot(sqr_distances)).dot(j_matrix)/2.0

	# Retrieve Eigenvectors and Eigenvalues from the b matrix
	eig_val_sc, eig_vec_sc = np.linalg.eig(b_matrix)

	# Sort the (eigenvalue, eigenvector) tuples from high to low
	eig_pairs = [(np.abs(eig_val_sc[i]), eig_vec_sc[:,i]) for i in range(len(eig_val_sc))]
	eig_pairs.sort(key=lambda pair: pair[0])
	eig_pairs.reverse()

	# Removing the eigen pair of the third dimension.
	matrix_w = np.hstack((eig_pairs[0][1].reshape(len(distances), 1), eig_pairs[1][1].reshape(len(distances), 1)))

	# Create the eigenvalues matrix
	matrix_v = [[np.sqrt(eig_val_sc[0]), 0], [0, np.sqrt(eig_val_sc[1])]]

	# Calculate the resulting positions in 2-D
	result_2d = matrix_w.dot(matrix_v);

	#Setup plots
	fig = plt.figure(figsize=(5*3,4.5))

	# choose a different color for each point
	colors = plt.cm.jet(np.linspace(0, 1, len(distances)))

	# plot 2d embedding
	subpl2 = fig.add_subplot(133)
	subpl2.set_autoscaley_on(False)
	subpl2.scatter(result_2d[:, 0], result_2d[:, 1],s=20, c=colors)
	plt.title('my mds')
	plt.axis('tight')

	# for i in range(len(eig_pairs)):
	print b_matrix"""